Release notes for...
- (Version Code) APK Version 14
- Release name: 1.94-R

Released to the alpha release channel on 06/15/2019

Notable User Features:
Performance enhancements for playing a checklist.
Center a revisited skipped item.
Recenter the active item when rotating.