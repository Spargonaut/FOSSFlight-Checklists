I would love for you to contribute. 
This code is far from perfect, so please feel free to provide refactorings, or extensions.

If you'd like to contribute, please...  
... include tests    
... keep your commits small and atomic  
... make sure all the tests pass  
... no merge commits.  Open your PR aginst master.  


#### Testing guidelines
In general, I try to follow the [testing pyramid](https://watirmelon.blog/testing-pyramids/) and avoid the ice cream cone.  

**In order of preference:**  
Unit tests for code that business logic    
Integration (Robolectric) tests for view logic    
Functional tests (espresso) tests for user flows and functionality     