package org.fossflight.efb.data.io

import android.app.Activity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import org.fossflight.efb.PermissionsManager
import org.fossflight.efb.data.model.Aircraft
import org.fossflight.efb.data.persistence.FFRepository

class Exporter (private val exporterListener: ExporterListener,
               private val permissionsManager: PermissionsManager,
               private val repository: FFRepository,
               private val compositeDisposable: CompositeDisposable,
               private val fossFlightFileWriter: FossFlightFileWriter) {
    private var aircraftId: Long = 0

    private val EXPORT_DIRECTORY = "FossFlight/"

    fun startExport(activity: Activity, aircraftId: Long) {
        this.aircraftId = aircraftId
        if (permissionsManager.hasWritePermission(activity)) {
            if (permissionsManager.canWriteToStorage()) {
                continueExport()
            } else {
                exporterListener.displayWriteProblems()
            }
        } else {
            permissionsManager.requestWritePermission(activity)
        }
    }

    fun continueExport() {
        compositeDisposable.add(
                repository.getAircraft(aircraftId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(Consumer<Aircraft> { this.export(it) })
        )
    }

    private fun export(aircraft: Aircraft) {
        val exportDirectory = permissionsManager.getWritableDirectory(EXPORT_DIRECTORY)
        if (!exportDirectory.exists()) {
            exportDirectory.mkdirs()
        }

        val lines = FossFlightAircraftParser().parseAircraft(aircraft)
        val exportFileName = "${exportDirectory.absolutePath}/${aircraft.tailNumber}_ffc.csv"

        try {
            fossFlightFileWriter.write(lines, exportFileName)
        } catch (e: Exception) {
            exporterListener.displayWriteProblems()
        }

        compositeDisposable.dispose()
    }
}
