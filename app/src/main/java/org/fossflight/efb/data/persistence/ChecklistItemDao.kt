package org.fossflight.efb.data.persistence


import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Single
import org.fossflight.efb.data.model.ChecklistItem

@Dao
interface ChecklistItemDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertChecklistItem(item: ChecklistItem): Long

    @Query("SELECT * FROM checklistItem WHERE checklist_id IS :checklistId")
    fun getItemsForChecklist(checklistId: Long): Single<List<ChecklistItem>>

    @Delete
    fun deleteChecklistItem(item: ChecklistItem)

    @Query("SELECT * FROM ChecklistItem WHERE id IS :checklistItemId")
    fun getChecklistItem(checklistItemId: Long): Single<ChecklistItem>
}
