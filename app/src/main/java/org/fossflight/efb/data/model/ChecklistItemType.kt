package org.fossflight.efb.data.model

enum class ChecklistItemType private constructor(private val commonName: String) {
    NORMAL("NORMAL"),
    SECTION_HEADER("SECTION HEADER");

    override fun toString(): String {
        return this.commonName
    }

    companion object {

        fun parseCommonName(nameString: String?): ChecklistItemType {
            for (type in ChecklistItemType.values()) {
                if (type.commonName.equals(nameString, ignoreCase = true)) {
                    return type
                }
            }
            return NORMAL
        }
    }
}
