package org.fossflight.efb.data.io

import org.fossflight.efb.Mockable
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader

@Mockable
class FossFlightFileReader {

    @Throws(IOException::class)
    fun readFile(inputStream: InputStream): List<String> =
            BufferedReader(InputStreamReader(inputStream)).use {
                it.readLines().filter(String::isNotEmpty).toList()
            }
}
