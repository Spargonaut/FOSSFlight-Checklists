package org.fossflight.efb.data.io

import org.fossflight.efb.data.model.Aircraft

interface ChecklistParser {
    fun parse(fileContents: List<String>): Aircraft
}
