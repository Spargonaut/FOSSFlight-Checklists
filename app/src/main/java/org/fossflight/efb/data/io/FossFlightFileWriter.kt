package org.fossflight.efb.data.io

import org.fossflight.efb.Mockable
import java.io.FileWriter
import java.io.IOException

@Mockable
class FossFlightFileWriter {

    @Throws(IOException::class)
    fun write(linesToWrite: List<String>, exportFileName: String) =
            FileWriter(exportFileName).use {
                out -> linesToWrite.forEach { line -> out.write(line) }
            }
}
