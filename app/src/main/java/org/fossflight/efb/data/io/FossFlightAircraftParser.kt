package org.fossflight.efb.data.io

import org.fossflight.efb.data.model.Aircraft
import org.fossflight.efb.data.model.ChecklistItem
import org.fossflight.efb.data.model.ChecklistItemType

const val DELIMITER: String = ","
const val SECTION_HEADER_ACTION = "-----"

class FossFlightAircraftParser {

    fun parseAircraft(aircraft: Aircraft): List<String> {
        val parsedLines = mutableListOf<String>()

        parsedLines.add("${aircraft.tailNumber},TailNumber\n")
        if (aircraft.manufacturer.isNotEmpty()) parsedLines.add("${aircraft.manufacturer},AircraftManufacturer\n")
        if (aircraft.model.isNotEmpty()) parsedLines.add("${aircraft.model},AircraftModel\n")
        if (aircraft.year > 0) parsedLines.add("${aircraft.year},AircraftYear\n")
        parsedLines.add("\n")

        aircraft.checklistCollections.forEach { collection ->
            collection.checklists.forEach { checklist ->
                parsedLines.add("${checklist.name}$DELIMITER${collection.name}\n")
                checklist.items.forEach { item ->
                    val action = if (itemIsSectionHeader(item)) SECTION_HEADER_ACTION else item.action
                    val itemAction = if (action.contains(DELIMITER)) "\"$action\"" else action
                    val itemName = if (item.name.contains(DELIMITER)) "\"${item.name}\"" else item.name
                    parsedLines.add("$itemName$DELIMITER$itemAction\n")
                }
                parsedLines.add("\n")
            }
        }

        return parsedLines
    }

    private fun itemIsSectionHeader(item: ChecklistItem): Boolean {
        return item.type == ChecklistItemType.SECTION_HEADER.toString()
    }
}
