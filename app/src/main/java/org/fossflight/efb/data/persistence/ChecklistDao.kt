package org.fossflight.efb.data.persistence


import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Flowable
import io.reactivex.Single
import org.fossflight.efb.data.model.Checklist

@Dao
interface ChecklistDao {

    @get:Query("SELECT * FROM checklist")
    val checklist: Flowable<List<Checklist>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertChecklist(checklist: Checklist): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertChecklists(checklists: List<Checklist>): List<Long>

    @Query("SELECT * FROM checklist WHERE id IS :checklistId")
    fun getChecklist(checklistId: Long): Flowable<Checklist>

    @Query("SELECT * FROM checklist WHERE checklist_collection_id IS :checklistCollectionId ORDER BY order_number")
    fun getChecklistsForChecklistCollectionId(checklistCollectionId: Long): Single<List<Checklist>>

    @Query("SELECT checklist_collection_id FROM checklist WHERE id IS :checklistId")
    fun getChecklistCollectionId(checklistId: Long): Single<Long>

    @Query("UPDATE Checklist SET checklist_collection_id = :collectionId, name = :name, order_number = :orderNumber WHERE id = :checklistId")
    fun updateChecklist(checklistId: Long, collectionId: Long, name: String, orderNumber: Long): Int

    @Delete
    fun deleteChecklist(checklist: Checklist)

    @Query("SELECT id FROM checklist WHERE checklist_collection_id IS :checklistCollectionId AND order_number IS :orderNumber")
    fun getChecklistIdForAircraftWithOrderNumber(checklistCollectionId: Long, orderNumber: Long): Single<Long>

    @Query("SELECT order_number FROM checklist WHERE checklist_collection_id IS :checklistCollectionId")
    fun getChecklistOrderNumbersFromCollection(checklistCollectionId: Long): Single<List<Long>>
}
