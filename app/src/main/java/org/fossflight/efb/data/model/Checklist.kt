package org.fossflight.efb.data.model

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.ForeignKey.NO_ACTION
import androidx.room.Ignore
import androidx.room.Index
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize
import org.fossflight.efb.BundleKeys

@Entity(foreignKeys =
    arrayOf(
            ForeignKey(
                    entity = ChecklistCollection::class,
                    onDelete = CASCADE,
                    onUpdate = NO_ACTION,
                    parentColumns = arrayOf("id"),
                    childColumns = arrayOf("checklist_collection_id")
            )
    ),
    indices = arrayOf(
            Index("id"),
            Index("checklist_collection_id")
    )
)
@Parcelize
data class Checklist(
        @PrimaryKey(autoGenerate = true) var id: Long = BundleKeys.UNDEFINED,
        @ColumnInfo(name = "checklist_collection_id") var checklistCollectionId: Long = BundleKeys.UNDEFINED,

        var name: String = "",
        @ColumnInfo(name = "order_number") var orderNumber: Long = BundleKeys.UNDEFINED,

        @Ignore var items: List<ChecklistItem> = listOf()
): Parcelable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null) return false
        if (other !is Checklist) return false

        val checklist = other

        if (id != checklist.id) return false
        if (checklistCollectionId != checklist.checklistCollectionId) return false
        if (name != checklist.name) return false
        if (orderNumber != checklist.orderNumber) return false

        if (items.isNotEmpty() && checklist.items.isNotEmpty()) {
            if (items.size != checklist.items.size) return false
            if (items != checklist.items) return false
        } else if (items.isEmpty() && checklist.items.isNotEmpty() ||
                   items.isNotEmpty() && checklist.items.isEmpty()) {
            return false
        }

        return name == checklist.name
    }

    override fun hashCode(): Int {
        var result = (id xor id.ushr(32)).toInt()
        result = 31 * result + name.hashCode()
        result = 31 * result + (checklistCollectionId xor checklistCollectionId.ushr(32)).toInt()
        return result
    }
}
