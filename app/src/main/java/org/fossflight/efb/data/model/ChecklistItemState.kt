package org.fossflight.efb.data.model

enum class ChecklistItemState {
    PRECHECKED,
    CHECKED,
    SKIPPED,
    HIGHLIGHTED
}
