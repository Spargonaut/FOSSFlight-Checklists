package org.fossflight.efb.data.io

import org.fossflight.efb.Mockable
import org.fossflight.efb.Parser.FLTPLANGO_CSV_TOKEN_DELIMITER
import org.fossflight.efb.data.model.Aircraft
import org.fossflight.efb.data.model.Checklist
import org.fossflight.efb.data.model.ChecklistCollection
import org.fossflight.efb.data.model.ChecklistItem

@Mockable
class FltPlanGoChecklistParser : ChecklistParser {

    override fun parse(fileContents: List<String>): Aircraft {

        val tokenizedLine = fileContents.map { tokenize(it) }

        val checklistTypes = tokenizedLine
                .mapIndexed { i, line -> Pair(i, line) }
                .filter { pair -> isType(pair.second) }

        val standardChecklistLines = tokenizedLine.subList(0, checklistTypes[1].first)
        val emergencyChecklistLines = tokenizedLine.subList(checklistTypes[1].first, tokenizedLine.size)

        val standardChecklistNames = collectChecklistNamesForType("Standard Checklist", standardChecklistLines)
        val emergencyChecklistNames = collectChecklistNamesForType("Emergency Checklist", emergencyChecklistLines)

        val standardChecklists = collectChecklists(standardChecklistNames, standardChecklistLines)
        val emergencyChecklists = collectChecklists(emergencyChecklistNames, emergencyChecklistLines)

        val standardChecklistCollection = ChecklistCollection(
                name = "Standard Checklist",
                type = "NORMAL",
                orderNumber = 0,
                checklists = standardChecklists
        )

        val emergencyChecklistCollection = ChecklistCollection(
                name = "Emergency Checklist",
                type = "EMERGENCY",
                orderNumber = 1,
                checklists = emergencyChecklists
        )
        return Aircraft(checklistCollections = listOf(standardChecklistCollection, emergencyChecklistCollection))
    }

    private fun collectChecklists(checklistNames: List<Pair<Int, String>>, lines: List<Array<String>>): List<Checklist> {
        val checklists = mutableListOf<Checklist>()

        val maxIndex = checklistNames.size - 1
        for (index in 0..maxIndex) {
            val startingIndex = checklistNames[index].first + 1
            val endingIndex = if (index + 1 < checklistNames.size) checklistNames[index + 1].first
                              else lines.size

            val name = checklistNames[index].second
            val itemLines = lines.subList(startingIndex, endingIndex)

            val parsedItems = itemLines.map { line -> ChecklistItem(name = line[0], action = line[1]) }

            val checklist = Checklist(
                    name = name,
                    orderNumber = index.toLong(),
                    items = parsedItems
            )
            checklists.add(checklist)
        }

        return checklists
    }

    private fun tokenize(line: String): Array<String> =
        line.substring(1, line.length - 1)
                .split(FLTPLANGO_CSV_TOKEN_DELIMITER.toRegex())
                .dropLastWhile { it.isEmpty() }
                .toTypedArray()

    private fun isType(tokenizedLine: Array<String>): Boolean =
        tokenizedLine.size == 1 && isChecklistType(tokenizedLine[0])

    private fun isChecklistType(line: String) = line == "Standard Checklist" || line == "Emergency Checklist"

    private fun isChecklistName(indexedPair: Pair<Int, Array<String>>) = indexedPair.second.size == 1

    private fun collectChecklistNamesForType(type: String, checklistLines: List<Array<String>>): List<Pair<Int, String>> =
        checklistLines
                .mapIndexed { index, tokenizedLine -> Pair(index, tokenizedLine) }
                .filter { isChecklistName(it) }
                .filter { pair -> pair.second[0] != type }
                .map { pair -> Pair(pair.first, pair.second[0])}
}
