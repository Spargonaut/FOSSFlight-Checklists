package org.fossflight.efb.aircraftList

import org.fossflight.efb.FossFlightEditView
import org.fossflight.efb.data.model.Aircraft

internal interface EditAircraftView : FossFlightEditView {
    fun updateToolbarForAdd()
    fun updateToolbarForUpdate()
    fun updateAircraftInfo(aircraft: Aircraft)
    fun displayTailNumberRequiredViolation()
}
