package org.fossflight.efb.aircraftList

import org.fossflight.efb.data.model.Aircraft

internal interface AircraftListItemListener {
    fun startChecklistCollectionFragment(aircraftId: Long)
    fun deleteAircraft(aircraft: Aircraft)
    fun editAircraft(aircraftId: Long)
    fun exportAircraft(aircraftId: Long)
}
