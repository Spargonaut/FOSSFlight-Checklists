package org.fossflight.efb.aircraftList

import org.fossflight.efb.FossFlightFragmentListener

interface AircraftListFragmentListener : FossFlightFragmentListener {
    fun startChecklistCollectionFragment(aircraftId: Long)
    fun startEditAircraftFragment(aircraftId: Long)
    fun updateToolbarIcon(drawableResourceId: Int)
    fun startAddAircraftDialog(aircraftListFragment: AircraftListFragment)
    fun startExportAircraftDialog(aircraftListFragment: AircraftListFragment, aircraftId: Long, tailNumber: String)
    fun startSelectParserDialogFragment(aircraftListFragment: AircraftListFragment)
}
