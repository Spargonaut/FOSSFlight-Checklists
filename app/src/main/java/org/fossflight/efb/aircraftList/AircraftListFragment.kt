package org.fossflight.efb.aircraftList

import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import io.reactivex.disposables.CompositeDisposable
import org.fossflight.efb.BundleKeys
import org.fossflight.efb.FossFlightApplication
import org.fossflight.efb.FossFlightListFragment
import org.fossflight.efb.PermissionsManager
import org.fossflight.efb.R
import org.fossflight.efb.RequestCodes
import org.fossflight.efb.aircraftList.dialog.ExportAircraftDialogListener
import org.fossflight.efb.aircraftList.dialog.ImportAircraftDialogListener
import org.fossflight.efb.data.io.ChecklistParser
import org.fossflight.efb.data.io.Exporter
import org.fossflight.efb.data.io.ExporterListener
import org.fossflight.efb.data.io.FileImporter
import org.fossflight.efb.data.io.FltPlanGoChecklistParser
import org.fossflight.efb.data.io.FossFlightCSVParser
import org.fossflight.efb.data.io.FossFlightFileReader
import org.fossflight.efb.data.io.FossFlightFileWriter
import org.fossflight.efb.data.model.Aircraft
import org.fossflight.efb.data.persistence.FFRepository
import java.io.FileNotFoundException
import java.io.InputStream
import javax.inject.Inject

class AircraftListFragment :
        FossFlightListFragment(),
        AircraftListView,
        ImportAircraftDialogListener,
        ExportAircraftDialogListener,
        ExporterListener {

    private lateinit var aircraftListFragmentListener: AircraftListFragmentListener
    private lateinit var aircraftListPresenter: AircraftListPresenter
    private lateinit var checklistParser: ChecklistParser
    private lateinit var listViewAdapter: AircraftListViewAdapter

    @Inject
    lateinit var repository: FFRepository

    @Inject
    lateinit var permissionsManager: PermissionsManager

    @Inject
    lateinit var fossFlightFileWriter: FossFlightFileWriter

    private lateinit var exporter: Exporter

    override fun onCreate(arguments: Bundle?) {
        super.onCreate(arguments)
        (requireActivity().application as FossFlightApplication).appComponent.inject(this)
        aircraftListPresenter = AircraftListPresenter(aircraftListView = this, repository = repository)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.aircraft_options, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    fun setParentActivity(listener: AircraftListFragmentListener) {
        this.aircraftListFragmentListener = listener
    }

    override fun applyImages() {
        listViewAdapter = AircraftListViewAdapter(aircraftListPresenter)
        recyclerView.adapter = listViewAdapter
    }

    override fun updateToolbar() {
        setHasOptionsMenu(true)
        aircraftListFragmentListener.updateToolbarIcon(R.drawable.ic_airplane)
        aircraftListFragmentListener.updateToolbarTitle(resources.getString(R.string.app_name))
        aircraftListFragmentListener.updateToolbarSubtitle(resources.getString(R.string.your_aircraft_list))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            R.id.add_aircraft -> aircraftListFragmentListener.startAddAircraftDialog(this)
            R.id.about -> displayAboutInformation()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onStart() {
        super.onStart()
        aircraftListPresenter.presentData()
    }

    override fun onDestroy() {
        super.onDestroy()
        aircraftListPresenter.dispose()
    }

    override fun updateRecyclerView(aircrafts: List<Aircraft>) {
        listViewAdapter.setAircrafts(aircrafts)
        listViewAdapter.notifyDataSetChanged()
    }

    override fun displayImportProblemDialog() =
            aircraftListFragmentListener.displayAlertDialog(R.string.import_problem_message)

    override fun startChecklistCollectionFragment(aircraftId: Long) =
            aircraftListFragmentListener.startChecklistCollectionFragment(aircraftId)

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == RESULT_OK) {
            if (requestCode == RequestCodes.GET_CSV && data != null) {
                val fileUri = data.data ?: return
                try {
                    val inputStream = requireActivity().contentResolver.openInputStream(fileUri)
                    val filename = fileUri.lastPathSegment ?: ""
                    if (inputStream != null) readFile(inputStream, filename)
                } catch (e: FileNotFoundException) {
                    displayImportProblemDialog()
                }
            }
        }
    }

    private fun readFile(inputStream: InputStream, filename: String) {
        val fileImporter = FileImporter(FossFlightFileReader(), checklistParser)
        aircraftListPresenter.importChecklist(filename, inputStream, fileImporter)
    }

    override fun displayExportAircraftDialog(aircraftId: Long, tailNumber: String) =
            aircraftListFragmentListener.startExportAircraftDialog(this, aircraftId, tailNumber)

    private fun displayAboutInformation() {
        val alertDialog = AlertDialog.Builder(context, R.style.alert_dialog)
                .setTitle(R.string.app_info_title)
                .setView(R.layout.app_info_layout)
                .setNeutralButton(R.string.OK, null)
                .create()
        alertDialog.show()
        (alertDialog.findViewById<View>(R.id.visit_website_view) as TextView).movementMethod = LinkMovementMethod.getInstance()
        (alertDialog.findViewById<View>(R.id.checklist_example_view) as TextView).movementMethod = LinkMovementMethod.getInstance()
        (alertDialog.findViewById<View>(R.id.build_info_view) as TextView).text = createBuildInfo()
    }

    private fun createBuildInfo(): String {
        val buildDate = getString(R.string.build_date)
        val commitHash = getString(R.string.commit_hash)
        val apkVersionCode = getString(R.string.apk_version_code)
        return resources.getString(R.string.build_information_template,
                buildDate, commitHash, apkVersionCode)
    }

    override fun addAircraft(which: Int) {
        when (which) {
            0 -> aircraftListFragmentListener.startSelectParserDialogFragment(this)
            2 -> importExampleChecklist()
            else -> aircraftListFragmentListener.startEditAircraftFragment(BundleKeys.UNDEFINED)
        }
    }

    override fun displayDeletedAircraftDialog(tailNumber: String) {
        aircraftListFragmentListener.displayToast(getString(R.string.deleted_tailnumber_template, tailNumber))
    }

    override fun exportAircraft(aircraftId: Long) {
        exporter = Exporter(this, permissionsManager, repository, CompositeDisposable(), fossFlightFileWriter)
        exporter.startExport(requireActivity(), aircraftId)
    }

    fun continueExportingAircraft() = exporter.continueExport()

    override fun displayWriteProblems() = aircraftListFragmentListener.displayAlertDialog(R.string.export_problem_message)

    private fun importExampleChecklist() {
        checklistParser = FossFlightCSVParser()
        val filename = getString(R.string.example_aircraft_tailnumber)
        try {
            val inputStream = resources.openRawResource(R.raw.nexfc1)
            readFile(inputStream, filename)
        } catch (e: Exception) {
            displayImportProblemDialog()
        }

    }

    override fun importChecklistType(which: Int) {
        checklistParser = when (which) {
            0 -> FossFlightCSVParser()
            else -> FltPlanGoChecklistParser()
        }

        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "text/*"
        startActivityForResult(intent, RequestCodes.GET_CSV)
    }

    override fun editAircraft(aircraftId: Long) = aircraftListFragmentListener.startEditAircraftFragment(aircraftId)
}

fun newAircraftListFragment(): AircraftListFragment = AircraftListFragment()
