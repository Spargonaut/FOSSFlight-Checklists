package org.fossflight.efb.aircraftList.dialog

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment

import org.fossflight.efb.BundleKeys
import org.fossflight.efb.R

class SelectParserDialogFragment : AppCompatDialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val title = requireArguments().getString(BundleKeys.TITLE)

        return AlertDialog.Builder(requireActivity(), R.style.alert_dialog)
                .setTitle(title)
                .setIcon(R.drawable.ic_airplane_black)
                .setItems(R.array.import_checklist_type_dialog_items) { _, which ->
                    (targetFragment as ImportAircraftDialogListener).importChecklistType(which)
                    dismiss()
                }
                .setNegativeButton(R.string.cancel) { _, _ -> dismiss() }
                .create()
    }
}

fun newSelectParserDialogFragment(title: String): SelectParserDialogFragment {
    val args = Bundle()
    args.putString(BundleKeys.TITLE, title)

    val frag = SelectParserDialogFragment()
    frag.arguments = args
    return frag
}
