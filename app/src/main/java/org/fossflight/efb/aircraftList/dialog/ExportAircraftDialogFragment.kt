package org.fossflight.efb.aircraftList.dialog

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment

import org.fossflight.efb.BundleKeys
import org.fossflight.efb.R

class ExportAircraftDialogFragment : AppCompatDialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val title = requireArguments().getString(BundleKeys.TITLE)
        val tailNumber = requireArguments().getString(BundleKeys.TAIL_NUMBER)
        val aircraftId = requireArguments().getLong(BundleKeys.AIRCRAFT_ID)

        return AlertDialog.Builder(requireActivity(), R.style.alert_dialog)
                .setTitle("$title $tailNumber")
                .setIcon(R.drawable.ic_airplane_black)
                .setPositiveButton(R.string.export) { _, _ ->
                    (targetFragment as ExportAircraftDialogListener).exportAircraft(aircraftId)
                    dismiss()
                }
                .setNegativeButton(R.string.cancel) { _, _ -> dismiss() }
                .create()
    }
}

fun newExportAircraftDialogFragment(title: String, aircraftId: Long, tailNumber: String): ExportAircraftDialogFragment {
    val args = Bundle()
    args.putLong(BundleKeys.AIRCRAFT_ID, aircraftId)
    args.putString(BundleKeys.TITLE, title)
    args.putString(BundleKeys.TAIL_NUMBER, tailNumber)

    val fragment = ExportAircraftDialogFragment()
    fragment.arguments = args
    return fragment
}
