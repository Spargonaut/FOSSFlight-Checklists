package org.fossflight.efb.aircraftList

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.fossflight.efb.data.io.FileImporter
import org.fossflight.efb.data.model.Aircraft
import org.fossflight.efb.data.persistence.FFRepository
import java.io.InputStream

internal class AircraftListPresenter(
        private val aircraftListView: AircraftListView,
        private val repository: FFRepository,
        private val compositeDisposable: CompositeDisposable = CompositeDisposable()
) : AircraftListItemListener {
    private var aircrafts: MutableList<Aircraft> = mutableListOf()

    fun presentData() {
        compositeDisposable.add(
                repository.allAircraft
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe { this.updateViewWithAircrafts(it) }
        )
    }

    private fun updateViewWithAircrafts(aircrafts: List<Aircraft>) {
        this.aircrafts = aircrafts.toMutableList()
        aircraftListView.updateRecyclerView(aircrafts)

        when {
            this.aircrafts.isEmpty() -> aircraftListView.updateUIForEmptyItemList()
            else -> aircraftListView.updateUIForNonEmptyItemList()
        }
    }

    fun importChecklist(filename: String, inputStream: InputStream, fileImporter: FileImporter) {
        try {
            val importedAircraft = fileImporter.importAircraftFromStream(filename, inputStream)
            compositeDisposable.add(
                    repository.insertAircraft(importedAircraft)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe { presentData() }
            )
        } catch (e: Exception) {
            aircraftListView.displayImportProblemDialog()
        }
    }

    override fun deleteAircraft(aircraft: Aircraft) {
        aircrafts.remove(aircraft)
        aircraftListView.updateRecyclerView(aircrafts)
        if (aircrafts.isEmpty()) aircraftListView.updateUIForEmptyItemList()
        compositeDisposable.add(
                repository.deleteAircraft(aircraft)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe { aircraftListView.displayDeletedAircraftDialog(aircraft.tailNumber) }
        )
    }

    override fun exportAircraft(aircraftId: Long) {
        compositeDisposable.add(
                repository.getAircraftTailNumber(aircraftId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe { tailNumber -> this.displayExportAircraftDialog(aircraftId, tailNumber) }
        )
    }

    override fun startChecklistCollectionFragment(aircraftId: Long) =
            aircraftListView.startChecklistCollectionFragment(aircraftId)

    override fun editAircraft(aircraftId: Long) = aircraftListView.editAircraft(aircraftId)

    private fun displayExportAircraftDialog(aircraftId: Long, tailNumber: String) =
        aircraftListView.displayExportAircraftDialog(aircraftId, tailNumber)

    fun dispose() = compositeDisposable.dispose()

}
