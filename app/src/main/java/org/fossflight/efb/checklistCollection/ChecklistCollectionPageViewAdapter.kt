package org.fossflight.efb.checklistCollection

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import org.fossflight.efb.R
import org.fossflight.efb.data.model.Checklist

internal class ChecklistCollectionPageViewAdapter(
        private val checklistCollectionPageItemListener: ChecklistCollectionPageItemListener
) : RecyclerView.Adapter<ChecklistCollectionPageItemViewHolder>() {

    private var checklists: List<Checklist> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChecklistCollectionPageItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.listable_card, parent, false)
        return ChecklistCollectionPageItemViewHolder(view, checklistCollectionPageItemListener)
    }

    override fun onBindViewHolder(holder: ChecklistCollectionPageItemViewHolder, position: Int) =
        holder.bind(checklists[position])

    override fun getItemCount(): Int = checklists.size

    fun setChecklists(checklists: List<Checklist>) {
        this.checklists = checklists
    }
}
