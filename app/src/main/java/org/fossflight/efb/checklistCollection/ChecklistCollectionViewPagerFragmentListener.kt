package org.fossflight.efb.checklistCollection

import org.fossflight.efb.FossFlightFragmentListener

interface ChecklistCollectionViewPagerFragmentListener : FossFlightFragmentListener {
    fun displaySnackbar(message: String)
    fun startChecklistFragment(checklistId: Long)
    fun startAddChecklistFragment(aircraftId: Long)
    fun startEditChecklistFragment(checklistId: Long)
}
