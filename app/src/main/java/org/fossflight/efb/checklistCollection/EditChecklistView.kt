package org.fossflight.efb.checklistCollection

import org.fossflight.efb.FossFlightEditView

interface EditChecklistView : FossFlightEditView {
    fun updateToolbarInfoForAdd(tailNumber: String)
    fun updateToolbarInfoForUpdate(tailNumber: String)
    fun updateChecklistName(checklistName: String)
    fun updateCollectionNames(types: List<String>)
    fun updateSelectedCollectionIndex(selectionIndex: Int)
    fun displayNameRequiredViolation()
}
