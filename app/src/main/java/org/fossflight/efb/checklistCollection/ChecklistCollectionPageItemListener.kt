package org.fossflight.efb.checklistCollection

import org.fossflight.efb.data.model.Checklist

internal interface ChecklistCollectionPageItemListener {
    fun startChecklistFragment(checklistId: Long)
    fun deleteChecklist(checklist: Checklist)
    fun editChecklist(checklistId: Long)
}
