package org.fossflight.efb.checklistCollection

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import org.fossflight.efb.BundleKeys
import org.fossflight.efb.data.model.Checklist
import org.fossflight.efb.data.model.ChecklistCollection
import org.fossflight.efb.data.persistence.FFRepository

class EditChecklistPresenter internal constructor(
        private val editChecklistView: EditChecklistView,
        private val repository: FFRepository,
        private val compositeDisposable: CompositeDisposable = CompositeDisposable(),
        private val aircraftId: Long = BundleKeys.UNDEFINED,
        private val checklistId: Long = BundleKeys.UNDEFINED
) {

    private lateinit var editAction: EditAction
    private lateinit var allCollections: List<ChecklistCollection>
    private lateinit var checklist: Checklist

    fun presentData() {
        when (checklistId) {
            BundleKeys.UNDEFINED -> {
                editAction = EditAction.ADD
                updateChecklistInfo(Checklist(name = ""))
            }
            else -> {
                editAction = EditAction.UPDATE
                compositeDisposable.add(
                        repository.getChecklist(checklistId)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(this::updateChecklistInfo)
                )
            }
        }
    }

    private fun updateChecklistInfo(checklist: Checklist) {
        this.checklist = checklist
        editChecklistView.updateChecklistName(checklist.name)
        retreiveChecklistCollectionInfo(checklist.id)
    }

    private fun retreiveChecklistCollectionInfo(checklistId: Long) {
        when (checklistId) {
            BundleKeys.UNDEFINED ->
                compositeDisposable.add(
                    repository.getChecklistCollections(aircraftId)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe { collections -> updateCollections(collections) }
            )
            else ->
                compositeDisposable.add(
                    repository.getSiblingChecklistCollections(checklistId)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe { collections -> updateCollections(collections) }
            )
        }
    }

    private fun updateCollections(checklistCollections: List<ChecklistCollection>) {

        this.allCollections =
                if (checklistCollections.isEmpty())
                    listOf(ChecklistCollection(aircraftId = aircraftId, name = BundleKeys.STANDARD_CHECKLIST_COLLECTION_NAME))
                else
                    checklistCollections


        val aircraftId = this.allCollections.get(0).aircraftId
        retreiveAircraftInfoFromAircraftId(aircraftId)

        editChecklistView.updateCollectionNames(this.allCollections.map { it.name })
        updateSelectedCollection()
    }

    private fun retreiveAircraftInfoFromAircraftId(aircraftId: Long) {
        compositeDisposable.add(
                repository.getAircraftTailNumber(aircraftId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe { tailNumber ->
                            when (editAction) {
                                EditAction.ADD -> editChecklistView.updateToolbarInfoForAdd(tailNumber)
                                EditAction.UPDATE -> editChecklistView.updateToolbarInfoForUpdate(tailNumber)
                            }
                        }
        )
    }

    private fun updateSelectedCollection() {
        val index = if (checklistId == BundleKeys.UNDEFINED) 0
                    else allCollections.indexOfFirst { it.id == this.checklist.checklistCollectionId }

        if (index >= 0) editChecklistView.updateSelectedCollectionIndex(index)
    }

    fun saveChecklist(name: String, collectionName: String) {
        when {
            name.isEmpty() -> editChecklistView.displayNameRequiredViolation()
            else -> {
                val checklistCollectionId = allCollections.find { it.name == collectionName }?.id ?: BundleKeys.UNDEFINED
                val checklist = Checklist(
                        id = checklistId,
                        checklistCollectionId = checklistCollectionId,
                        name = name,
                        orderNumber = checklist.orderNumber
                )
                when (checklistCollectionId) {
                    BundleKeys.UNDEFINED -> createTheChecklistCollectionThenSave(checklist)
                    else -> applyChecklistOrderNumberThenSave(checklist)
                }
            }
        }
    }

    private fun createTheChecklistCollectionThenSave(checklist: Checklist) {
        val newChecklistCollection = ChecklistCollection(
                aircraftId = aircraftId,
                name = BundleKeys.STANDARD_CHECKLIST_COLLECTION_NAME,
                orderNumber = 0
        )

        compositeDisposable.add(
            repository.saveChecklistCollection(newChecklistCollection)
                    .subscribeOn(Schedulers.io())
                    .subscribe(
                            Consumer {
                                applyChecklistOrderNumberThenSave(checklist.copy(checklistCollectionId = it))
                            }
                    )
        )
    }

    private fun applyChecklistOrderNumberThenSave(checklist: Checklist) {
        when {
            checklist.id == BundleKeys.UNDEFINED
                    ||
                    checklist.checklistCollectionId != this.checklist.checklistCollectionId
            -> {
                compositeDisposable.add(
                        repository.getChecklistOrderNumbersFromCollection(checklist.checklistCollectionId)
                                .subscribeOn(Schedulers.io())
                                .subscribe { orderNumbers ->
                                    val newOrderNumber = orderNumbers.max()?.plus(1) ?: 0
                                    persistTheChecklist(checklist.copy(orderNumber = newOrderNumber))
                                }
                )
            }
            else -> persistTheChecklist(checklist)
        }
    }

    private fun persistTheChecklist(checklist: Checklist) {
        when (editAction) {
            EditAction.ADD ->
                compositeDisposable.add(
                    repository.saveChecklist(checklist)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe { saveSuccessful(checklist.name) }
                )
            EditAction.UPDATE ->
                compositeDisposable.add(
                        repository.updateChecklist(checklist)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe { saveSuccessful(checklist.name) }
                )


        }
    }

    private fun saveSuccessful(itemName: String) = editChecklistView.saveSuccessful(itemName)

    fun dispose() = compositeDisposable.dispose()
}

enum class EditAction { ADD, UPDATE }