package org.fossflight.efb.checklist

import org.fossflight.efb.data.model.Checklist
import org.fossflight.efb.data.model.ChecklistItem
import org.fossflight.efb.data.model.ChecklistItemState
import org.fossflight.efb.data.model.ChecklistItemType

class ChecklistPlayer {

    var checklist: Checklist = Checklist()
        set(value) {
            field = value
            activeItemIndex = value.items.indexOfFirst { item ->
                item.state === ChecklistItemState.HIGHLIGHTED
            }
        }

    private val inactiveIndex = -1
    var activeItemIndex = inactiveIndex
        private set
    private var previousStateOfItem = ChecklistItemState.PRECHECKED

    fun markItemAsChecked(): Int {
        return markItemAsState(ChecklistItemState.CHECKED)
    }

    fun markItemAsSkipped(): Int {
        return if (isActive())
            markItemAsState(ChecklistItemState.SKIPPED)
        else
            inactiveIndex
    }

    private fun markItemAsState(state: ChecklistItemState): Int {
        setItemToState(state)
        return highlightNextItemToBeChecked()
    }

    private fun setItemToState(itemState: ChecklistItemState) {
        if (activeItemIndex < checklist.items.size && isActive()) {
            checklist.items[activeItemIndex].state = itemState
        }
    }

    private fun highlightNextItemToBeChecked(): Int {
        if (activeItemIndex < checklist.items.size) activeItemIndex++

        while (activeItemIndex < checklist.items.size) {
            val item = checklist.items[activeItemIndex]
            if (item.state === ChecklistItemState.SKIPPED ||
                    item.state === ChecklistItemState.PRECHECKED && item.type == ChecklistItemType.NORMAL.toString()) {
                previousStateOfItem = getCurrentStateOfItemAtIndex(activeItemIndex)
                setItemToState(ChecklistItemState.HIGHLIGHTED)
                break
            }
            activeItemIndex++
        }
        return activeItemIndex
    }

    private fun getCurrentStateOfItemAtIndex(index: Int): ChecklistItemState {
        val items = checklist.items
        return if (index > inactiveIndex && index < items.size)
            items[index].state
        else
            ChecklistItemState.PRECHECKED
    }

    fun highlightLastSkippedItem(): Int {
        if (!hasSkippedItems()) {
            return activeItemIndex
        }

        val indexOfLastSkippedItem = checklist.items
                .subList(0, activeItemIndex)
                .indexOfLast { it.state === ChecklistItemState.SKIPPED }

        if (indexOfLastSkippedItem >= 0) {
            setItemToState(previousStateOfItem)
            previousStateOfItem = getCurrentStateOfItemAtIndex(indexOfLastSkippedItem)
            activeItemIndex = indexOfLastSkippedItem
            setItemToState(ChecklistItemState.HIGHLIGHTED)
            return indexOfLastSkippedItem
        } else {
            return activeItemIndex
        }
    }

    fun highlightFirstSkippedItem(): Int {
        val indexOfFirstSkippedItem = checklist.items.indexOfFirst { it.state == ChecklistItemState.SKIPPED }
        activeItemIndex = indexOfFirstSkippedItem
        checklist.items[activeItemIndex].state = ChecklistItemState.HIGHLIGHTED
        return activeItemIndex
    }

    fun isCompleted(): Boolean {
        return  checklist.items.isNotEmpty() &&
                checklist.items
                        .filter {
                            it.type != ChecklistItemType.SECTION_HEADER.toString()
                        }
                        .all {
                            it.state == ChecklistItemState.CHECKED ||
                            it.state == ChecklistItemState.SKIPPED
                        }
    }

    fun resetChecklist() {
        checklist.items.forEach { item ->
            item.state = ChecklistItemState.PRECHECKED
        }
        activeItemIndex = inactiveIndex
    }

    fun isActive(): Boolean {
        return  activeItemIndex != inactiveIndex &&
                activeItemIndex < checklist.items.size
    }

    fun deleteChecklistItem(item: ChecklistItem) {
         checklist.items = checklist.items - item
    }

    fun hasSkippedItems(): Boolean {
        return checklist.items.find { item ->
            item.state === ChecklistItemState.SKIPPED
        } != null
    }
}