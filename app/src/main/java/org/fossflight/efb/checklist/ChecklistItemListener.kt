package org.fossflight.efb.checklist

import org.fossflight.efb.data.model.ChecklistItem

interface ChecklistItemListener {
    fun isEditable(): Boolean
    fun deleteChecklistItem(checklistItem: ChecklistItem)
    fun editChecklistItem(checklistItemId: Long)
}
