package org.fossflight.efb.checklist

import org.fossflight.efb.FossFlightEditView

interface EditChecklistItemView : FossFlightEditView {
    fun updateToolbarInfoForAdd(subtitle: String)
    fun updateToolbarInfoForUpdate(subtitle: String)
    fun displayProblemToast()
    fun displayNameRequiredViolation()
    fun updateEditableInformation(name: String, action: String, checklistItemTypeOrdinal: Int)
}
