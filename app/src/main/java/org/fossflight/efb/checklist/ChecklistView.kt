package org.fossflight.efb.checklist

import org.fossflight.efb.FossFlightListView
import org.fossflight.efb.data.model.ChecklistItem

interface ChecklistView : FossFlightListView {
    fun updateRecyclerView(checklistItems: List<ChecklistItem>)
    fun activateChecklist()
    fun updateItemListScrolledToPosition(position: Int)

    fun displayChecklistCompleteDialog()
    fun displayChecklistCompleteWithSkippedItemsDialog()

    fun updateToolbarChecklistName(checklistName: String)
    fun updateToolbarTailNumber(tailNumber: String)

    fun displayButtonsForInactiveChecklist()
    fun updateUIForChecklistActiveState(isActive: Boolean, isCompleted: Boolean)
    fun disableGoToPreviouslyCheckedButton()
    fun updateCompletedChecklistButtons(hasSkippedItems: Boolean)
    fun enableGoToPreviouslyCheckedButton()

    fun displayDeletedChecklistItemDialog(name: String)
    fun editChecklistItem(checklistItemId: Long)

    fun finishChecklist()
}
