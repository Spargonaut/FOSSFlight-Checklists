package org.fossflight.efb.checklist

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.fossflight.efb.BundleKeys
import org.fossflight.efb.data.model.ChecklistItem
import org.fossflight.efb.data.model.ChecklistItemType
import org.fossflight.efb.data.persistence.FFRepository

internal class EditChecklistItemPresenter(
        private val editChecklistItemView: EditChecklistItemView,
        private val repository: FFRepository,
        private val compositeDisposable: CompositeDisposable = CompositeDisposable(),
        private val checklistId: Long,
        private val itemId: Long
) {

    private lateinit var editAction: EditAction

    fun presentData() {
        when (itemId) {
            BundleKeys.UNDEFINED -> {
                editAction = EditAction.ADD
                updateForm(ChecklistItem(id = itemId))
            }
            else -> {
                editAction = EditAction.UPDATE
                compositeDisposable.add(
                        repository.getChecklistItem(itemId)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe { checklistItem -> updateForm(checklistItem) }
                )
            }
        }
    }

    private fun updateForm(checklistItem: ChecklistItem) {
        editChecklistItemView.updateEditableInformation(
                name = checklistItem.name,
                action = checklistItem.action,
                checklistItemTypeOrdinal = ChecklistItemType.parseCommonName(checklistItem.type).ordinal
        )
        updateToolbar()
    }

    private fun updateToolbar() {
        when (editAction) {
            EditAction.ADD -> {
                compositeDisposable.add(
                        repository.getChecklist(checklistId)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe { checklist -> editChecklistItemView.updateToolbarInfoForAdd(checklist.name) }
                )
            }
            EditAction.UPDATE -> {
                compositeDisposable.add(
                        repository.getChecklist(checklistId)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe { checklist -> editChecklistItemView.updateToolbarInfoForUpdate(checklist.name) }
                )
            }
        }
    }

    fun saveChecklistItem(name: String, action: String, checklistItemType: String) {
        when (name.trim()) {
            "" -> editChecklistItemView.displayNameRequiredViolation()
            else -> {
                val item = ChecklistItem(
                        id = itemId, checkListId = checklistId,
                        name = name, action = action, type = checklistItemType)

                compositeDisposable.add(
                        repository.saveChecklistItem(item)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe { saveSuccessful(name) }
                )
            }
        }
    }

    private fun saveSuccessful(itemName: String) = editChecklistItemView.saveSuccessful(itemName)

    fun dispose() = compositeDisposable.dispose()
}

enum class EditAction { ADD, UPDATE }