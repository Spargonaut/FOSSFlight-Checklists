package org.fossflight.efb.checklist

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import org.fossflight.efb.FossFlightItemViewHolder
import org.fossflight.efb.R
import org.fossflight.efb.data.model.ChecklistItem
import org.fossflight.efb.data.model.ChecklistItemState.CHECKED
import org.fossflight.efb.data.model.ChecklistItemState.HIGHLIGHTED
import org.fossflight.efb.data.model.ChecklistItemState.PRECHECKED
import org.fossflight.efb.data.model.ChecklistItemState.SKIPPED

class ChecklistItemViewHolder internal constructor(
        view: View,
        checklistItemListener: ChecklistItemListener
) : FossFlightItemViewHolder(view, checklistItemListener) {

    private val view: CardView
    private val contentView: TextView
    private val shortDescriptionView: TextView
    private val statusIcon: ImageView

    init {
        this.view = view as CardView
        this.contentView = view.findViewById(R.id.content)
        this.shortDescriptionView = view.findViewById(R.id.short_description)
        this.statusIcon = view.findViewById(R.id.card_icon)
        this.view.setOnLongClickListener(this)
    }

    override fun bind(checklistItem: ChecklistItem) {
        super.bind(checklistItem)
        contentView.text = checklistItem.name
        shortDescriptionView.text = checklistItem.action

        val cardBackgroundColor = when (checklistItem.state) {
            PRECHECKED -> R.color.prechecked_background
            CHECKED -> R.color.checked_background
            SKIPPED -> R.color.skipped_background
            HIGHLIGHTED -> R.color.highlighted_background
        }

        val iconDrawable = when (checklistItem.state) {
            PRECHECKED -> R.drawable.ic_circle
            CHECKED -> R.drawable.ic_check_circle
            SKIPPED -> R.drawable.ic_warning
            HIGHLIGHTED -> R.drawable.ic_airplane_black
        }

        statusIcon.setImageDrawable(ContextCompat.getDrawable(view.context, iconDrawable))
        view.setCardBackgroundColor(ContextCompat.getColor(view.context, cardBackgroundColor))
    }
}
