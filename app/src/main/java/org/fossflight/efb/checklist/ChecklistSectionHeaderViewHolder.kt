package org.fossflight.efb.checklist

import android.view.View
import android.widget.TextView
import org.fossflight.efb.FossFlightItemViewHolder
import org.fossflight.efb.R
import org.fossflight.efb.data.model.ChecklistItem

class ChecklistSectionHeaderViewHolder internal constructor(
        view: View,
        checklistItemListener: ChecklistItemListener
    ) : FossFlightItemViewHolder(view, checklistItemListener) {

    private val contentView: TextView = view.findViewById(R.id.content)

    init {
        view.setOnLongClickListener(this)
    }

    override fun bind(checklistItem: ChecklistItem) {
        super.bind(checklistItem)
        this.contentView.text = checklistItem.name
    }
}
