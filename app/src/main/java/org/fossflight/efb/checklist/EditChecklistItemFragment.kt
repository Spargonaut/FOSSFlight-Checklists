package org.fossflight.efb.checklist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import org.fossflight.efb.BundleKeys
import org.fossflight.efb.FossFlightApplication
import org.fossflight.efb.FossFlightEditFragment
import org.fossflight.efb.FossFlightFragmentListener
import org.fossflight.efb.R
import org.fossflight.efb.data.persistence.FFRepository
import javax.inject.Inject

class EditChecklistItemFragment : FossFlightEditFragment(), EditChecklistItemView {

    private lateinit var editChecklistItemPresenter: EditChecklistItemPresenter
    private lateinit var fossFlightFragmentListener: FossFlightFragmentListener

    private lateinit var checklistNameEditText: TextInputEditText
    private lateinit var checklistActionEditText: EditText
    private lateinit var checklistNameLayout: TextInputLayout
    private lateinit var spinner: Spinner

    @Inject
    lateinit var repository: FFRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (requireActivity().application as FossFlightApplication).appComponent.inject(this)

        val checklistId = arguments?.getLong(BundleKeys.CHECKLIST_ID) ?: BundleKeys.UNDEFINED
        val checklistItemId = arguments?.getLong(BundleKeys.CHECKLIST_ITEM_ID) ?: BundleKeys.UNDEFINED

        editChecklistItemPresenter = EditChecklistItemPresenter(
                editChecklistItemView = this,
                repository = repository,
                checklistId = checklistId,
                itemId = checklistItemId
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        checklistNameEditText = view.findViewById(R.id.checklist_item_name_edit)
        checklistActionEditText = view.findViewById(R.id.checklist_item_action_edit)
        checklistNameLayout = view.findViewById(R.id.input_layout_name_one)
        spinner = view.findViewById(R.id.checklist_item_type_spinner)
    }

    override fun inflateView(inflater: LayoutInflater, container: ViewGroup): View =
            inflater.inflate(R.layout.edit_checklist_item, container, false)

    override fun onStart() {
        super.onStart()
        editChecklistItemPresenter.presentData()
    }

    override fun onDestroy() {
        super.onDestroy()
        editChecklistItemPresenter.dispose()
    }

    override fun setParentActivity(listener: FossFlightFragmentListener) {
        super.setParentActivity(listener)
        fossFlightFragmentListener = listener
    }

    override fun updateToolbarInfoForAdd(subtitle: String) {
        fossFlightFragmentListener.updateToolbarTitle(getString(R.string.add_checklist_item))
        fossFlightFragmentListener.updateToolbarSubtitle(getString(R.string.checklist_name_template, subtitle))
    }

    override fun updateToolbarInfoForUpdate(subtitle: String) {
        fossFlightFragmentListener.updateToolbarTitle(getString(R.string.edit_checklist_item))
        fossFlightFragmentListener.updateToolbarSubtitle(getString(R.string.checklist_name_template, subtitle))
    }

    override fun save() {
        val name = checklistNameEditText.text.toString()
        val action = checklistActionEditText.text.toString()
        val itemType = spinner.selectedItem.toString()
        editChecklistItemPresenter.saveChecklistItem(name, action, itemType)
    }

    override fun displayNameRequiredViolation() {
        val errorMessage = getString(R.string.name_required_checklist_item_error_message)
        fossFlightFragmentListener.displayToast(errorMessage)
        checklistNameLayout.error = errorMessage
    }

    override fun updateEditableInformation(
            name: String,
            action: String,
            checklistItemTypeOrdinal: Int) {

        checklistNameEditText.setText(name)
        checklistActionEditText.setText(action)

        val checklistItemTypes = listOf("NORMAL", "SECTION HEADER")
        val adapter = ArrayAdapter(
                requireActivity(),
                android.R.layout.simple_spinner_item,
                checklistItemTypes
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter
        spinner.setSelection(checklistItemTypeOrdinal)
    }

    override fun displayProblemToast() =
            fossFlightFragmentListener.displayToast(getString(R.string.problem_getting_checklist_info_message))
}

fun newEditChecklistItemFragment(checklistId: Long, checklistItemId: Long): EditChecklistItemFragment {
    val arguments = Bundle()
    arguments.putLong(BundleKeys.CHECKLIST_ID, checklistId)
    arguments.putLong(BundleKeys.CHECKLIST_ITEM_ID, checklistItemId)

    val fragment = EditChecklistItemFragment()
    fragment.arguments = arguments
    return fragment
}
