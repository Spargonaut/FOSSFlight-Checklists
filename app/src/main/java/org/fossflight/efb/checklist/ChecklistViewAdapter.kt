package org.fossflight.efb.checklist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import org.fossflight.efb.FossFlightItemViewHolder
import org.fossflight.efb.R
import org.fossflight.efb.data.model.ChecklistItem
import org.fossflight.efb.data.model.ChecklistItemType

internal class ChecklistViewAdapter(private val checklistItemListener: ChecklistItemListener) :
        RecyclerView.Adapter<FossFlightItemViewHolder>() {

    private var checklistItems: List<ChecklistItem> = listOf()

    override fun getItemViewType(position: Int): Int {
        if (checklistItems.isNotEmpty()) {
            val typeString = checklistItems[position].type
            return ChecklistItemType.parseCommonName(typeString).ordinal
        }
        return ChecklistItemType.NORMAL.ordinal
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FossFlightItemViewHolder {
        val checklistItemType = ChecklistItemType.values()[viewType]
        return when (checklistItemType) {
            ChecklistItemType.SECTION_HEADER ->  createSectionHeaderaViewHolder(parent)
            ChecklistItemType.NORMAL ->  createItemViewHolder(parent)
        }
    }

    private fun createItemViewHolder(parent: ViewGroup): FossFlightItemViewHolder {
        val view = inflateView(parent, R.layout.checklist_item_card)
        return ChecklistItemViewHolder(view, checklistItemListener)
    }

    private fun createSectionHeaderaViewHolder(parent: ViewGroup): FossFlightItemViewHolder {
        val view = inflateView(parent, R.layout.listable_section_header)
        return ChecklistSectionHeaderViewHolder(view, checklistItemListener)
    }

    private fun inflateView(parent: ViewGroup, layoutToInflate: Int): View =
        LayoutInflater.from(parent.context).inflate(layoutToInflate, parent, false)

    override fun onBindViewHolder(holder: FossFlightItemViewHolder, position: Int) =
        holder.bind(checklistItems[position])

    override fun getItemCount(): Int = checklistItems.size

    fun setChecklistItems(checklistItems: List<ChecklistItem>) {
        this.checklistItems = checklistItems
    }
}
