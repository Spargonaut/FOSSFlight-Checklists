package org.fossflight.efb.checklist

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import org.fossflight.efb.BundleKeys
import org.fossflight.efb.FossFlightApplication
import org.fossflight.efb.FossFlightListFragment
import org.fossflight.efb.R
import org.fossflight.efb.data.model.Checklist
import org.fossflight.efb.data.model.ChecklistItem
import org.fossflight.efb.data.persistence.FFRepository
import javax.inject.Inject

class ChecklistFragment :
        FossFlightListFragment(),
        ChecklistView {

    private lateinit var checklistFragmentListener: ChecklistFragmentListener
    private lateinit var checklistPresenter: ChecklistPresenter
    private lateinit var listViewAdapter: ChecklistViewAdapter

    private lateinit var playButton: ImageView
    private lateinit var skipButton: ImageView
    private lateinit var revisitSkippedButton: ImageView

    private lateinit var resetChecklistMenuItem: MenuItem
    private lateinit var addChecklistItemMenuItem: MenuItem
    private var checklistId: Long = BundleKeys.UNDEFINED

    @Inject
    lateinit var repository: FFRepository

    override fun onCreate(arguments: Bundle?) {
        super.onCreate(arguments)
        (requireActivity().application as FossFlightApplication).appComponent.inject(this)
        activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        checklistId = getArguments()?.getLong(BundleKeys.CHECKLIST_ID) ?: BundleKeys.UNDEFINED
        val checklist: Checklist? = arguments?.getParcelable(BundleKeys.CHECKLIST)
        val checklistPlayer = ChecklistPlayer()
        if (checklist != null ) {
            checklistPlayer.checklist = checklist
            checklistId = checklist.id
        }

        checklistPresenter = ChecklistPresenter(
                checklistView = this,
                repository = repository,
                checklistId = checklistId,
                checklistPlayer = checklistPlayer
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        playButton = view.findViewById(R.id.play_button)
        playButton.setOnClickListener { checklistPresenter.markItemAsChecked() }

        skipButton = view.findViewById(R.id.skip_button)
        skipButton.setOnClickListener { checklistPresenter.markItemAsSkipped() }

        revisitSkippedButton = view.findViewById(R.id.revisit_previously_skipped_button)
        revisitSkippedButton.setOnClickListener { checklistPresenter.highlightLastSkippedItem() }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.checklist_options, menu)
        super.onCreateOptionsMenu(menu, inflater)
        resetChecklistMenuItem = menu.findItem(R.id.reset_checklist)
        addChecklistItemMenuItem = menu.findItem(R.id.add_checklist_item)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        checklistPresenter.adjustOptionsMenu()
    }

    override fun updateUIForChecklistActiveState(isActive: Boolean, isCompleted: Boolean) {
        resetChecklistMenuItem.isEnabled = isActive || isCompleted
        addChecklistItemMenuItem.isEnabled = !isActive && !isCompleted
    }

    override fun inflateView(inflater: LayoutInflater, container: ViewGroup): View {
        val view = inflater.inflate(R.layout.checklist_fragment, container, false)
        return view
    }

    override fun applyImages() {
        listViewAdapter = ChecklistViewAdapter(checklistPresenter)
        recyclerView.adapter = listViewAdapter
    }

    override fun updateToolbar() = setHasOptionsMenu(true)

    fun setParentActivity(listener: ChecklistFragmentListener) {
            checklistFragmentListener = listener
    }

    override fun onResume() {
        super.onResume()
        checklistPresenter.presentData()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(BundleKeys.CHECKLIST, checklistPresenter.checklist)
    }

    override fun onDestroy() {
        super.onDestroy()
        checklistPresenter.dispose()
    }

    override fun updateRecyclerView(checklistItems: List<ChecklistItem>) {
        listViewAdapter.setChecklistItems(checklistItems)
        listViewAdapter.notifyDataSetChanged()
    }

    override fun updateToolbarChecklistName(checklistName: String) =
            checklistFragmentListener.updateToolbarTitle(checklistName)

    override fun updateToolbarTailNumber(tailNumber: String) =
            checklistFragmentListener.updateToolbarSubtitle(tailNumber)

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.reset_checklist -> resetChecklist()
            R.id.add_checklist_item -> checklistPresenter.editChecklistItem(BundleKeys.UNDEFINED)
            android.R.id.home -> {
                checklistFragmentListener.finishFragment()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun updateItemListScrolledToPosition(position: Int) {
        listViewAdapter.notifyDataSetChanged()
        val smoothScroller = CenterSmoothScroller(recyclerView.context)
        smoothScroller.targetPosition = position
        recyclerView.layoutManager?.startSmoothScroll(smoothScroller)
    }

    override fun activateChecklist() {
        playButton.setImageResource(R.drawable.ic_done)
        playButton.setOnClickListener { checklistPresenter.markItemAsChecked() }
        skipButton.visibility = View.VISIBLE
        revisitSkippedButton.visibility = View.VISIBLE
    }

    override fun displayButtonsForInactiveChecklist() {
        playButton.setImageResource(R.drawable.ic_play_arrow)
        playButton.setOnClickListener { checklistPresenter.markItemAsChecked() }
        revisitSkippedButton.visibility = View.INVISIBLE
        skipButton.visibility = View.INVISIBLE
    }

    override fun displayChecklistCompleteDialog() {
        displayCompletedDialog(
                messageId = R.string.end_of_checklist_message,
                positiveAction = { _, _ -> checklistPresenter.startNextChecklist() }
        )
    }

    override fun displayChecklistCompleteWithSkippedItemsDialog() {
        displayCompletedDialog(
                messageId = R.string.end_of_checklist_with_skipped_items_message,
                positiveAction = { _, _ -> checklistPresenter.highlightFirstSkippedItem() }
        )
    }

    override fun updateCompletedChecklistButtons(hasSkippedItems: Boolean) {
        val (revisitSkippedItemsButtonVisibility, playButtonColorId) =
                if (hasSkippedItems)
                    Pair(View.VISIBLE, android.R.color.white)
                else
                    Pair(View.INVISIBLE, R.color.green)

        skipButton.visibility = View.INVISIBLE
        revisitSkippedButton.visibility = revisitSkippedItemsButtonVisibility
        playButton.setImageResource(R.drawable.ic_done_all)
        DrawableCompat.setTint(playButton.drawable, ContextCompat.getColor(requireContext(), playButtonColorId))
        playButton.setOnClickListener { checklistPresenter.startNextChecklist() }
    }

    private fun displayCompletedDialog(
            messageId: Int,
            positiveAction: (DialogInterface, Int) -> Unit
    ) {
        AlertDialog.Builder(activity, R.style.alert_dialog)
                .setMessage(messageId)
                .setPositiveButton(R.string.yes, positiveAction)
                .setNeutralButton(R.string.no) { _, _ ->
                    updateCompletedChecklistButtons(checklistPresenter.checklistPlayer.hasSkippedItems())
                }
                .setOnCancelListener {
                    updateCompletedChecklistButtons(checklistPresenter.checklistPlayer.hasSkippedItems())
                }
                .show()
    }

    private fun resetChecklist() {
        checklistPresenter.resetChecklistItems()
        displayButtonsForInactiveChecklist()
    }

    override fun updateUIForEmptyItemList() {
        super.updateUIForEmptyItemList()
        emptyListTextView.text = getString(R.string.empty_checklist_message)
        DrawableCompat.setTint(playButton.drawable, ContextCompat.getColor(requireContext(), R.color.lightGrey))
        playButton.isClickable = false
    }

    override fun displayDeletedChecklistItemDialog(name: String) {
        val message = getString(R.string.deleted_checklist_item_template, name)
        checklistFragmentListener.displaySnackbar(message)
    }

    override fun disableGoToPreviouslyCheckedButton() =
        DrawableCompat.setTint(revisitSkippedButton.drawable, ContextCompat.getColor(requireContext(), R.color.lightGrey))

    override fun enableGoToPreviouslyCheckedButton() =
        DrawableCompat.setTint(revisitSkippedButton.drawable, ContextCompat.getColor(requireContext(), R.color.skippedYellow))

    override fun editChecklistItem(checklistItemId: Long) =
        checklistFragmentListener.startEditChecklistItemFragment(checklistId, checklistItemId)

    override fun finishChecklist() = checklistFragmentListener.finishFragment()

}

fun newChecklistFragment(checklistId: Long): ChecklistFragment {
    val arguments = Bundle()
    arguments.putLong(BundleKeys.CHECKLIST_ID, checklistId)

    val checklistFragment = ChecklistFragment()
    checklistFragment.arguments = arguments
    return checklistFragment
}
