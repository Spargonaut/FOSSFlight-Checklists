package org.fossflight.efb

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import org.fossflight.efb.data.io.FossFlightFileWriter
import org.fossflight.efb.data.persistence.FFDatabase
import org.fossflight.efb.data.persistence.FFRepository
import javax.inject.Singleton

@Module
open class AppModule internal constructor(private val context: Context) {

    @Provides
    @Singleton
    open fun provideRepository(): FFRepository {
        val ffDatabase = Room.databaseBuilder(context, FFDatabase::class.java, "fossflight-db")
                .addMigrations(FFDatabase.Migrations.MIGRATION_1_2)
                .addMigrations(FFDatabase.Migrations.MIGRATION_2_3)
                .addMigrations(FFDatabase.Migrations.MIGRATION_3_4)
                .addMigrations(FFDatabase.Migrations.MIGRATION_4_5)
                .build()
        return FFRepository(ffDatabase)
    }

    @Provides
    @Singleton
    open fun providePermissionsManager(): PermissionsManager {
        return PermissionsManager()
    }

    @Provides
    @Singleton
    open fun provideFossFlightFileWriter(): FossFlightFileWriter {
        return FossFlightFileWriter()
    }
}
