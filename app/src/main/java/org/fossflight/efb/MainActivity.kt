package org.fossflight.efb

import android.app.AlertDialog
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import org.fossflight.efb.aircraftList.AircraftListFragment
import org.fossflight.efb.aircraftList.AircraftListFragmentListener
import org.fossflight.efb.aircraftList.EditAircraftFragment
import org.fossflight.efb.aircraftList.dialog.newExportAircraftDialogFragment
import org.fossflight.efb.aircraftList.dialog.newImportAircraftDialogFragment
import org.fossflight.efb.aircraftList.dialog.newSelectParserDialogFragment
import org.fossflight.efb.aircraftList.newAircraftListFragment
import org.fossflight.efb.aircraftList.newEditAircraftFragment
import org.fossflight.efb.checklist.ChecklistFragment
import org.fossflight.efb.checklist.ChecklistFragmentListener
import org.fossflight.efb.checklist.EditChecklistItemFragment
import org.fossflight.efb.checklist.newChecklistFragment
import org.fossflight.efb.checklist.newEditChecklistItemFragment
import org.fossflight.efb.checklistCollection.ChecklistCollectionPageFragment
import org.fossflight.efb.checklistCollection.ChecklistCollectionViewPagerFragment
import org.fossflight.efb.checklistCollection.ChecklistCollectionViewPagerFragmentListener
import org.fossflight.efb.checklistCollection.EditChecklistFragment
import org.fossflight.efb.checklistCollection.newAddChecklistFragment
import org.fossflight.efb.checklistCollection.newChecklistCollectionViewPagerFragment
import org.fossflight.efb.checklistCollection.newEditChecklistFragment
import org.fossflight.efb.data.io.ExporterFragmentListener

class MainActivity :
        AppCompatActivity(),
        FossFlightFragmentListener,
        ExporterFragmentListener,
        AircraftListFragmentListener,
        ChecklistCollectionViewPagerFragmentListener,
        ChecklistFragmentListener
{

    private lateinit var aircraftListFragment: AircraftListFragment

    private lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        supportFragmentManager.addOnBackStackChangedListener {
            when (supportFragmentManager.backStackEntryCount) { 0 -> finish() }
        }

        if (supportFragmentManager.backStackEntryCount == 0) startAircraftListFragment()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            RequestCodes.WRITE_EXTERNAL_STORAGE -> handleExport(grantResults)
        }
    }

    override fun onAttachFragment(fragment: Fragment) {
        super.onAttachFragment(fragment)
        if (fragment is AircraftListFragment) fragment.setParentActivity(this)
        else if (fragment is ChecklistCollectionViewPagerFragment) fragment.setParentActivity(this)
        else if (fragment is ChecklistFragment) fragment.setParentActivity(this)
        else if (fragment is ChecklistCollectionPageFragment) fragment.setParentActivity(this)
        else if (fragment is EditAircraftFragment) fragment.setParentActivity(this)
        else if (fragment is EditChecklistFragment) fragment.setParentActivity(this)
        else if (fragment is EditChecklistItemFragment) fragment.setParentActivity(this)
    }

    private fun handleExport(grantResults: IntArray) {
        if (grantResults.isNotEmpty()) {
            if (grantResults[0] >= 0) {
                aircraftListFragment.continueExportingAircraft()
            } else {
                displayNeedForWritePermissions()
            }
        }
    }

    private fun startAircraftListFragment() {
        aircraftListFragment = newAircraftListFragment()
        startFragment(
                fragment = aircraftListFragment,
                toolbarIconId = R.drawable.ic_airplane)
    }

    private fun startFragment(fragment: Fragment, toolbarIconId: Int = R.drawable.ic_arrow_back) {
        toolbar.setNavigationIcon(toolbarIconId)
        val fragmentClassname = fragment.javaClass.simpleName
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, fragment, fragmentClassname)
                .addToBackStack(fragmentClassname)
                .commit()
    }

    override fun finishFragment() {
        supportFragmentManager.popBackStackImmediate()
    }

    override fun displayAlertDialog(messageId: Int) {
        AlertDialog.Builder(this, R.style.alert_dialog)
                .setMessage(messageId)
                .setNeutralButton(R.string.OK, null)
                .show()
    }

    override fun startEditAircraftFragment(aircraftId: Long) =
            startFragment(
                    fragment = newEditAircraftFragment(aircraftId),
                    toolbarIconId = R.drawable.ic_close
            )

    override fun startChecklistCollectionFragment(aircraftId: Long) =
            startFragment(newChecklistCollectionViewPagerFragment(aircraftId))

    override fun startAddChecklistFragment(aircraftId: Long) =
            startFragment(
                    fragment = newAddChecklistFragment(aircraftId),
                    toolbarIconId = R.drawable.ic_close
            )

    override fun startEditChecklistFragment(checklistId: Long) =
            startFragment(
                    fragment = newEditChecklistFragment(checklistId),
                    toolbarIconId = R.drawable.ic_close
            )

    override fun startChecklistFragment(checklistId: Long) = startFragment(newChecklistFragment(checklistId))

    override fun startEditChecklistItemFragment(checklistId: Long, checklistItemId: Long) =
            startFragment(
                    fragment = newEditChecklistItemFragment(checklistId, checklistItemId),
                    toolbarIconId = R.drawable.ic_close
            )

    override fun startAddAircraftDialog(aircraftListFragment: AircraftListFragment) =
            displayDialogFragment(
                    dialogFragment = newImportAircraftDialogFragment(getString(R.string.add_aircraft)),
                    targetFragment = aircraftListFragment
            )

    override fun startExportAircraftDialog(
            aircraftListFragment: AircraftListFragment,
            aircraftId: Long, tailNumber: String
    ) = displayDialogFragment(
            dialogFragment = newExportAircraftDialogFragment(getString(R.string.export_aircraft), aircraftId, tailNumber),
            targetFragment = aircraftListFragment
    )

    override fun startSelectParserDialogFragment(aircraftListFragment: AircraftListFragment) =
            displayDialogFragment(
                    dialogFragment = newSelectParserDialogFragment(getString(R.string.type_to_import)),
                    targetFragment = aircraftListFragment
            )

    private fun displayDialogFragment(dialogFragment: AppCompatDialogFragment, targetFragment: Fragment) {
        dialogFragment.setTargetFragment(targetFragment, -1)
        dialogFragment.show(supportFragmentManager, dialogFragment.javaClass.simpleName)
    }

    override fun displayNeedForWritePermissions() = displayAlertDialog(R.string.need_write_permissions_message)

    override fun displaySnackbar(message: String) =
            Snackbar.make(findViewById(R.id.main_coordinator_layout), message, Snackbar.LENGTH_LONG)
                    .show()

    override fun displayToast(message: String) = Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

    override fun updateToolbarIcon(drawableResourceId: Int) = toolbar.setNavigationIcon(drawableResourceId)

    override fun updateToolbarTitle(title: String) = toolbar.setTitle(title)

    override fun updateToolbarSubtitle(subtitle: String) = toolbar.setSubtitle(subtitle)
}
