package org.fossflight.efb;


import org.junit.rules.ExternalResource;

import java.util.concurrent.Callable;

import androidx.annotation.NonNull;
import io.reactivex.Scheduler;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.functions.Function;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;

public class ForceTrampolineSchedulers extends ExternalResource {
    @Override
    protected void before() throws Throwable {
        super.before();
        RxJavaPlugins.reset();
        RxAndroidPlugins.reset();
        RxJavaPlugins.setIoSchedulerHandler(getSchedulerTrampoline());
        RxJavaPlugins.setInitNewThreadSchedulerHandler(getCallableTrampoline());
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(getCallableTrampoline());
    }

    @NonNull
    private Function<Scheduler, Scheduler> getSchedulerTrampoline() {
        return scheduler -> Schedulers.trampoline();
    }

    @NonNull
    private Function<Callable<Scheduler>, Scheduler> getCallableTrampoline() {
        return scheduler -> Schedulers.trampoline();
    }

    @Override
    protected void after() {
        super.after();
        RxJavaPlugins.reset();
        RxAndroidPlugins.reset();
    }
}
