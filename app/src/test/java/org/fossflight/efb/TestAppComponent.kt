package org.fossflight.efb

import dagger.Component
import org.fossflight.efb.checklistCollection.EditChecklistFragmentTest
import javax.inject.Singleton

@Component(modules = arrayOf(TestAppModule::class))
@Singleton
interface TestAppComponent : AppComponent {
    fun inject(editChecklistFragmentTest: EditChecklistFragmentTest)
}
