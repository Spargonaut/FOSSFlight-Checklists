package org.fossflight.efb.checklist

import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.test.core.app.ActivityScenario
import androidx.test.ext.junit.runners.AndroidJUnit4
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Flowable
import org.fossflight.efb.MainActivity
import org.fossflight.efb.R
import org.fossflight.efb.data.model.Aircraft
import org.fossflight.efb.data.model.ChecklistItemState
import org.fossflight.efb.data.persistence.FFRepository
import org.fossflight.efb.data.testChecklistItem
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.containsString
import org.hamcrest.CoreMatchers.endsWith
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Shadows.shadowOf
import org.robolectric.shadows.ShadowPopupMenu
import javax.inject.Inject


@RunWith(AndroidJUnit4::class)
class ChecklistItemViewHolderTest {
    private val mockChecklistItemListener = mockk<ChecklistItemListener>()

    @Inject
    private val mockRepository = mockk<FFRepository>()

    @Before
    fun `set up mocks and activity`() {
        every { mockRepository.allAircraft } returns Flowable.just<List<Aircraft>>(listOf())
        every { mockChecklistItemListener.deleteChecklistItem(any()) } returns Unit
        every { mockChecklistItemListener.editChecklistItem(any()) } returns Unit
    }

    @Test
    fun `should Get the Text for the ContentView as the String`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->
                val view = createRootCardView(mainActivity)

                val checklistItem = testChecklistItem()
                val checklistItemViewHolder = ChecklistItemViewHolder(view, mockChecklistItemListener)
                checklistItemViewHolder.bind(checklistItem)
                assertThat(checklistItemViewHolder.toString(), containsString("ChecklistItemViewHolder"))
                assertThat(checklistItemViewHolder.toString(), endsWith("'${checklistItem.name}'"))

            }
        }
    }

    @Test
    fun `should Bind the ChecklistItem to the View`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->
                val view = createRootCardView(mainActivity)

                val checklistItem = testChecklistItem(
                        name = "Item One",
                        action = "action item one"
                )

                val checklistItemViewHolder = ChecklistItemViewHolder(view, mockChecklistItemListener)
                checklistItemViewHolder.bind(checklistItem)

                val actualName = (view.findViewById<View>(R.id.content) as TextView).text.toString()
                val actualAction = (view.findViewById<View>(R.id.short_description) as TextView).text.toString()

                assertThat(actualName, `is`("Item One"))
                assertThat(actualAction, `is`("action item one"))
            }
        }
    }

    @Test
    fun `should Display the Prechecked Styles when the Card is Prechecked`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->
                val view = createRootCardView(mainActivity)

                val checklistItem = testChecklistItem()

                val checklistItemViewHolder = ChecklistItemViewHolder(view, mockChecklistItemListener)
                checklistItem.state = ChecklistItemState.PRECHECKED
                checklistItemViewHolder.bind(checklistItem)

                val precheckedBackgroundColor = ContextCompat.getColor(view.context, R.color.prechecked_background)
                val actualColor = (view as CardView).cardBackgroundColor.defaultColor
                assertThat(actualColor, `is`(precheckedBackgroundColor))

                val actualCardIcon = view.findViewById<View>(R.id.card_icon) as ImageView
                val actualDrawableId = shadowOf(actualCardIcon.getDrawable()).getCreatedFromResId()
                assertThat(actualDrawableId, `is`(R.drawable.ic_circle))
            }
        }
    }

    @Test
    fun `should Display the Checked Styles when the Card is Checked`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->
                val view = createRootCardView(mainActivity)

                val checklistItem = testChecklistItem(
                        state = ChecklistItemState.CHECKED
                )

                val checklistItemViewHolder = ChecklistItemViewHolder(view, mockChecklistItemListener)
                checklistItemViewHolder.bind(checklistItem)

                val checkedBackgroundColor = ContextCompat.getColor(view.context, R.color.checked_background)
                val actualColor = (view as CardView).cardBackgroundColor.defaultColor
                assertThat(actualColor, `is`(checkedBackgroundColor))

                val actualCardIcon = view.findViewById<View>(R.id.card_icon) as ImageView
                val actualDrawableId = shadowOf(actualCardIcon.getDrawable()).getCreatedFromResId()
                assertThat(actualDrawableId, `is`(R.drawable.ic_check_circle))
            }
        }
    }

    @Test
    fun `should Display the Skipped Styles when the Card is Skipped`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->
                val view = createRootCardView(mainActivity)

                val checklistItem = testChecklistItem(
                        state = ChecklistItemState.SKIPPED
                )

                val checklistItemViewHolder = ChecklistItemViewHolder(view, mockChecklistItemListener)
                checklistItemViewHolder.bind(checklistItem)

                val skippedBackgroundColor = ContextCompat.getColor(view.context, R.color.skipped_background)
                val actualColor = (view as CardView).cardBackgroundColor.defaultColor
                assertThat(actualColor, `is`(skippedBackgroundColor))

                val actualCardIcon = view.findViewById<View>(R.id.card_icon) as ImageView
                val actualDrawableId = shadowOf(actualCardIcon.getDrawable()).getCreatedFromResId()
                assertThat(actualDrawableId, `is`(R.drawable.ic_warning))
            }
        }
    }

    @Test
    fun `should Display the Highlighted Styles when the Card is Highlighted`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->
                val view = createRootCardView(mainActivity)

                val checklistItem = testChecklistItem(
                        state = ChecklistItemState.HIGHLIGHTED
                )

                val checklistItemViewHolder = ChecklistItemViewHolder(view, mockChecklistItemListener)
                checklistItemViewHolder.bind(checklistItem)

                val highlightedBackgroundColor = ContextCompat.getColor(view.context, R.color.highlighted_background)
                val actualColor = (view as CardView).cardBackgroundColor.defaultColor
                assertThat(actualColor, `is`(highlightedBackgroundColor))

                val actualCardIcon = view.findViewById<View>(R.id.card_icon) as ImageView
                val actualDrawableId = shadowOf(actualCardIcon.getDrawable()).getCreatedFromResId()
                assertThat(actualDrawableId, `is`(R.drawable.ic_airplane_black))
            }
        }
    }

    @Test
    fun `should Display the Delete Option on the Popup Menu for the Item`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->
                val view = createRootCardView(mainActivity)

                val checklistItem = testChecklistItem()

                every { mockChecklistItemListener.isEditable() } returns true

                val checklistItemViewHolder = ChecklistItemViewHolder(view, mockChecklistItemListener)
                checklistItemViewHolder.bind(checklistItem)

                view.performLongClick()

                val latestPopupMenu = ShadowPopupMenu.getLatestPopupMenu()

                val menu = latestPopupMenu.menu
                val deleteItem = menu.getItem(1)

                assertThat(deleteItem.title.toString(), `is`("Delete"))
            }
        }
    }

    @Test
    fun `should Display the Edit Option on the Popup Menu for the Item`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->
                val view = createRootCardView(mainActivity)

                val checklistItem = testChecklistItem()

                every { mockChecklistItemListener.isEditable() } returns true

                val checklistItemViewHolder = ChecklistItemViewHolder(view, mockChecklistItemListener)
                checklistItemViewHolder.bind(checklistItem)

                view.performLongClick()

                val latestPopupMenu = ShadowPopupMenu.getLatestPopupMenu()

                val menu = latestPopupMenu.menu
                val deleteItem = menu.getItem(0)

                assertThat(deleteItem.title.toString(), `is`("Edit"))
            }
        }
    }

    @Test
    fun `should Delete an Item from the Popup Context Menu`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->
                val view = createRootCardView(mainActivity)

                val checklistItem = testChecklistItem()

                every { mockChecklistItemListener.isEditable() } returns true

                val checklistItemViewHolder = ChecklistItemViewHolder(view, mockChecklistItemListener)
                checklistItemViewHolder.bind(checklistItem)

                view.performLongClick()

                val latestPopupMenu = ShadowPopupMenu.getLatestPopupMenu()
                val shadowLatestPopupMenu = shadowOf(latestPopupMenu)

                val onMenuItemClickListener = shadowLatestPopupMenu.onMenuItemClickListener

                val menuItemId = R.id.delete_item
                val mockMenuItem = mockk<MenuItem>()
                every { mockMenuItem.itemId } returns menuItemId

                onMenuItemClickListener.onMenuItemClick(mockMenuItem)

                verify { mockChecklistItemListener.deleteChecklistItem(checklistItem) }
            }
        }
    }

    @Test
    fun `should Ignore a Long Click when a Checklist is Not Editable`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->
                val view = createRootCardView(mainActivity)

                val checklistItem = testChecklistItem()

                every { mockChecklistItemListener.isEditable() } returns false

                val checklistItemViewHolder = ChecklistItemViewHolder(view, mockChecklistItemListener)
                checklistItemViewHolder.bind(checklistItem)

                view.performLongClick()

                val latestPopupMenu = ShadowPopupMenu.getLatestPopupMenu()

                assertNull(latestPopupMenu)
            }
        }
    }

    @Test
    fun `should Display the Card with the Focused Styles`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->
                val view = createRootCardView(mainActivity)

                val checklistItem = testChecklistItem(
                        state = ChecklistItemState.HIGHLIGHTED
                )
                val checklistItemViewHolder = ChecklistItemViewHolder(view, mockChecklistItemListener)
                checklistItemViewHolder.bind(checklistItem)

                val highlightedBackground = ContextCompat.getColor(view.context, R.color.highlighted_background)
                val actualColor = (view as CardView).cardBackgroundColor.defaultColor
                assertThat(actualColor, `is`(highlightedBackground))
            }
        }
    }

    @Test
    fun `should Edit an Item From the Popup Menu`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->
                val view = createRootCardView(mainActivity)

                val checklistItem = testChecklistItem()

                every { mockChecklistItemListener.isEditable() } returns true

                val checklistItemViewHolder = ChecklistItemViewHolder(view, mockChecklistItemListener)
                checklistItemViewHolder.bind(checklistItem)

                view.performLongClick()

                val latestPopupMenu = ShadowPopupMenu.getLatestPopupMenu()
                val shadowLatestPopupMenu = shadowOf(latestPopupMenu)

                val onMenuItemClickListener = shadowLatestPopupMenu.onMenuItemClickListener

                val menuItemId = R.id.edit_item
                val mockMenuItem = mockk<MenuItem>()
                every { mockMenuItem.itemId } returns menuItemId

                onMenuItemClickListener.onMenuItemClick(mockMenuItem)

                verify { mockChecklistItemListener.editChecklistItem(checklistItem.id) }
            }
        }
    }

    private fun createRootCardView(mainActivity: MainActivity): View {
        val root = CardView(mainActivity)
        val layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        )
        layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
        root.layoutParams = layoutParams
        return LayoutInflater.from(mainActivity).inflate(R.layout.checklist_item_card, root)
    }
}