package org.fossflight.efb.checklist

import android.content.DialogInterface
import android.os.Bundle
import android.view.MenuItem
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.view.menu.MenuBuilder
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Flowable
import io.reactivex.Single
import org.fossflight.efb.BundleKeys
import org.fossflight.efb.ForceTrampolineSchedulers
import org.fossflight.efb.FossFlightApplication
import org.fossflight.efb.FossFlightTestApplication
import org.fossflight.efb.R
import org.fossflight.efb.TestAppModule
import org.fossflight.efb.data.model.Aircraft
import org.fossflight.efb.data.model.Checklist
import org.fossflight.efb.data.model.ChecklistItemState
import org.fossflight.efb.data.persistence.FFRepository
import org.fossflight.efb.data.randomId
import org.fossflight.efb.data.randomName
import org.fossflight.efb.data.randomOrderNumber
import org.fossflight.efb.data.randomTailNumber
import org.fossflight.efb.data.testChecklist
import org.fossflight.efb.data.testChecklistItem
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.ExpectedException
import org.junit.runner.RunWith
import org.robolectric.Shadows.shadowOf
import org.robolectric.annotation.Config
import org.robolectric.fakes.RoboMenu
import org.robolectric.shadows.ShadowAlertDialog

@Config(application = FossFlightTestApplication::class)
@RunWith(AndroidJUnit4::class)
class ChecklistFragmentTest {

    private val emptyChecklistId = randomId()
    private val nonEmptyChecklistId = randomId()
    private val checklistName = randomName()
    private val tailNumber = randomTailNumber()
    private val orderNumber = randomOrderNumber()

    private val nonEmptyChecklist = testChecklist(
            id = nonEmptyChecklistId,
            name = checklistName,
            orderNumber = orderNumber,
            items = listOf(
                    testChecklistItem(state = ChecklistItemState.PRECHECKED),
                    testChecklistItem(state = ChecklistItemState.PRECHECKED)
            )
    )

    private val idOfNextChecklist = randomId()
    val nextChecklist  = testChecklist(
            id = idOfNextChecklist,
            orderNumber = orderNumber + 1,
            items = listOf(
                    testChecklistItem(state = ChecklistItemState.CHECKED),
                    testChecklistItem(state = ChecklistItemState.CHECKED)
            )
    )



    @get:Rule
    var expectedException = ExpectedException.none()

    @get:Rule
    var forceTrampolineSchedulers = ForceTrampolineSchedulers()

    private val mockRepository = mockk<FFRepository>()

    private val mockChecklistFragmentListener = mockk<ChecklistFragmentListener>()

    val instantiate: () -> ChecklistFragment = {
        val fragment = ChecklistFragment()
        fragment.setParentActivity(mockChecklistFragmentListener)
        fragment
    }

    @Before
    fun `set up`() {
        val application = ApplicationProvider.getApplicationContext<FossFlightTestApplication>()
        val testAppModule = TestAppModule(
                context = application.applicationContext,
                repository = mockRepository
        )

        application.setAppModule(testAppModule)

        val emptyChecklist = testChecklist(
                id = emptyChecklistId,
                name = checklistName,
                items = listOf()
        )

        every { mockRepository.allAircraft } returns Flowable.just<List<Aircraft>>(listOf())

        every { mockRepository.getChecklist(nonEmptyChecklistId) } returns Single.just(nonEmptyChecklist)
        every { mockRepository.getAircraftTailNumberForChecklist(nonEmptyChecklistId) } returns Single.just(tailNumber)

        every { mockRepository.getChecklist(emptyChecklistId) } returns Single.just(emptyChecklist)
        every { mockRepository.getAircraftTailNumberForChecklist(emptyChecklistId) } returns Single.just(tailNumber)

        every { mockChecklistFragmentListener.updateToolbarTitle(any()) } returns Unit
        every { mockChecklistFragmentListener.updateToolbarSubtitle(any()) } returns Unit
        every { mockChecklistFragmentListener.displaySnackbar(any()) } returns Unit
        every { mockChecklistFragmentListener.finishFragment() } returns Unit
        every { mockChecklistFragmentListener.startEditChecklistItemFragment(any(), any()) } returns Unit

        every { mockRepository.getChecklist(idOfNextChecklist) } returns Single.just(nextChecklist)
        every { mockRepository.getAircraftTailNumberForChecklist(idOfNextChecklist) } returns Single.just(tailNumber)
        every { mockRepository.getChecklistIdFromChecklistCollectionId(nonEmptyChecklist.checklistCollectionId, orderNumber + 1) } returns Single.just(idOfNextChecklist)
    }

    // FIXME - need a test to assert on the "all done" icon being used
//    val playIconDrawable = shadowOf(checklistFragment.requireView().findViewById<ImageView>(R.id.play_button).drawable)
//    assertThat(playIconDrawable.createdFromResId, `is`(R.drawable.ic_done_all))

    @Test
    fun `should Display the Active Checklist Action Buttons`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_ID, nonEmptyChecklistId)

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            it.setParentActivity(mockChecklistFragmentListener)
            it.activateChecklist()
        }

        onView(withId(R.id.play_button)).check(matches(isDisplayed()))
        onView(withId(R.id.revisit_previously_skipped_button)).check(matches(isDisplayed()))
        onView(withId(R.id.skip_button)).check(matches(isDisplayed()))
    }

    @Test
    fun `should update the UI for an NonEmpty PrePlayed Checklist`() {

        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_ID, nonEmptyChecklistId)

        launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        onView(withId(R.id.play_button)).check(matches(isDisplayed()))
        onView(withId(R.id.empty)).check(matches(not(isDisplayed())))
    }

    @Test
    fun `should set the Keep Screen On flag`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_ID, nonEmptyChecklistId)

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            val flagKeepScreenOn = WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
            val shadowWindow = shadowOf(it.requireActivity().window)

            assertThat(shadowWindow.getFlag(flagKeepScreenOn), `is`(true))
        }
    }

    @Test
    fun `should display the Checklist Completed AlertDialog`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_ID, nonEmptyChecklistId)

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            it.displayChecklistCompleteDialog()

            val latestAlertDialog = ShadowAlertDialog.getLatestAlertDialog()
            val shadowAlertDialog = shadowOf(latestAlertDialog)

            val activity = it.requireActivity()
            val expectedMessage = activity.getString(R.string.end_of_checklist_message)
            assertThat(shadowAlertDialog.message, `is`<CharSequence>(expectedMessage))

            val actualNeutralButtonText = latestAlertDialog.getButton(AlertDialog.BUTTON_NEUTRAL).text.toString()
            val expectedNeutralButtonText = activity.getString(R.string.no)
            assertThat(actualNeutralButtonText, `is`(expectedNeutralButtonText))

            val actualPositiveButtonText = latestAlertDialog.getButton(AlertDialog.BUTTON_POSITIVE).text.toString()
            val expectedPositiveButtonText = activity.getString(R.string.yes)
            assertThat(actualPositiveButtonText, `is`(expectedPositiveButtonText))
        }
    }

    @Test
    fun `should start the next Checklist after a Yes Tap on the Checklist Complete Dialog`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_ID, nonEmptyChecklistId)

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            it.displayChecklistCompleteDialog()
        }

        val latestAlertDialog = ShadowAlertDialog.getLatestAlertDialog()
        latestAlertDialog.getButton(DialogInterface.BUTTON_POSITIVE).performClick()

        verify { mockRepository.getChecklist(idOfNextChecklist) }
    }

    @Test
    fun `should update the Checklist Play buttons when tapping 'No' on the Checklist Completed AlertDialog`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_ID, nonEmptyChecklistId)

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            it.displayChecklistCompleteDialog()
        }

        ShadowAlertDialog.getLatestAlertDialog().getButton(DialogInterface.BUTTON_NEUTRAL).performClick()

        // FIXME - assert on drawable.ic_done_all
        onView(withId(R.id.play_button)).check(matches(isDisplayed()))
        onView(withId(R.id.skip_button)).check(matches(not(isDisplayed())))
        onView(withId(R.id.revisit_previously_skipped_button)).check(matches(not(isDisplayed())))


    }

    @Test
    fun `should update the Checklist Play buttons when canceling the Checklist Completed AlertDialog`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_ID, nonEmptyChecklistId)

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            it.displayChecklistCompleteDialog()
        }

        ShadowAlertDialog.getLatestAlertDialog().cancel()

        // FIXME - assert on drawable.ic_done_all
        onView(withId(R.id.play_button)).check(matches(isDisplayed()))
        onView(withId(R.id.skip_button)).check(matches(not(isDisplayed())))
        onView(withId(R.id.revisit_previously_skipped_button)).check(matches(not(isDisplayed())))
    }

    @Test
    fun `should update the Checklist Control buttons for a Completed Checklist`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_ID, nonEmptyChecklistId)

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            it.updateCompletedChecklistButtons(false)
        }

        // FIXME - assert on drawable.ic_done_all
        onView(withId(R.id.play_button)).check(matches(isDisplayed()))
        onView(withId(R.id.skip_button)).check(matches(not(isDisplayed())))
        onView(withId(R.id.revisit_previously_skipped_button)).check(matches(not(isDisplayed())))
    }

    @Test
    fun `should display the Checklist Completed With Skipped Items AlertDialog`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_ID, nonEmptyChecklistId)
        nonEmptyChecklist.items[0].state = ChecklistItemState.SKIPPED

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            it.displayChecklistCompleteWithSkippedItemsDialog()
        }

        val latestAlertDialog = ShadowAlertDialog.getLatestAlertDialog()
        val shadowAlertDialog = shadowOf(latestAlertDialog)
        val context = ApplicationProvider.getApplicationContext<FossFlightApplication>()

        val expectedMessage = context.getString(R.string.end_of_checklist_with_skipped_items_message)
        assertThat(shadowAlertDialog.message, `is`<CharSequence>(expectedMessage))

        val actualNegativeButtonText = latestAlertDialog.getButton(AlertDialog.BUTTON_NEUTRAL).text.toString()
        val expectedNegativeButtonText = context.getString(R.string.no)
        assertThat(actualNegativeButtonText, `is`(expectedNegativeButtonText))

        val actualPositiveButtonText = latestAlertDialog.getButton(AlertDialog.BUTTON_POSITIVE).text.toString()
        val expectedPositiveButtonText = context.getString(R.string.yes)
        assertThat(actualPositiveButtonText, `is`(expectedPositiveButtonText))
    }

    @Test
    fun `should update the Checklist Play buttons when tapping 'No' on the Checklist Completed With Skipped Items AlertDialog`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_ID, nonEmptyChecklistId)
        nonEmptyChecklist.items[0].state = ChecklistItemState.SKIPPED

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            it.displayChecklistCompleteWithSkippedItemsDialog()
        }

        ShadowAlertDialog.getLatestAlertDialog().getButton(DialogInterface.BUTTON_NEUTRAL).performClick()

        // FIXME - assert on drawable.ic_done_all
        onView(withId(R.id.play_button)).check(matches(isDisplayed()))
        onView(withId(R.id.skip_button)).check(matches(not(isDisplayed())))
        onView(withId(R.id.revisit_previously_skipped_button)).check(matches(isDisplayed()))
    }

    @Test
    fun `should update the Checklist Play buttons when canceling the Checklist Completed with Skipped Items AlertDialog`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_ID, nonEmptyChecklistId)
        nonEmptyChecklist.items[0].state = ChecklistItemState.SKIPPED

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            it.displayChecklistCompleteWithSkippedItemsDialog()
        }

        ShadowAlertDialog.getLatestAlertDialog().cancel()

        // FIXME - assert on drawable.ic_done_all
        onView(withId(R.id.play_button)).check(matches(isDisplayed()))
        onView(withId(R.id.skip_button)).check(matches(not(isDisplayed())))
        onView(withId(R.id.revisit_previously_skipped_button)).check(matches(isDisplayed()))
    }

    @Test
    fun `should update the Checklist Control buttons for a Completed Checklist With Skipped Items`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_ID, nonEmptyChecklistId)
        nonEmptyChecklist.items[0].state = ChecklistItemState.SKIPPED

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            it.updateCompletedChecklistButtons(hasSkippedItems = true)
        }

        // FIXME - assert on drawable.ic_done_all
        onView(withId(R.id.play_button)).check(matches(isDisplayed()))
        onView(withId(R.id.skip_button)).check(matches(not(isDisplayed())))
        onView(withId(R.id.revisit_previously_skipped_button)).check(matches(isDisplayed()))
    }

    @Test
    fun `should update the checklist buttons for an inactive checklist`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_ID, nonEmptyChecklistId)

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            it.displayButtonsForInactiveChecklist()
        }

        // FIXME - assert on the Play Arrow Icon
        onView(withId(R.id.skip_button)).check(matches(not(isDisplayed())))
        onView(withId(R.id.revisit_previously_skipped_button)).check(matches(not(isDisplayed())))
    }

    @Test
    fun `should Update the UI for an Empty Item List`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_ID, emptyChecklistId)

        launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        // FIXME - assert on the Play Arrow Icon
        onView(withId(R.id.empty)).check(matches(isDisplayed()))
        val emptyListString = "It looks like you don\'t have any checklist items yet.\nTap \'Add Checklist Item\' in the options menu to get started."

        onView(withText(emptyListString)).check(matches(isDisplayed()))
        onView(withId(R.id.skip_button)).check(matches(not(isDisplayed())))
        onView(withId(R.id.revisit_previously_skipped_button)).check(matches(not(isDisplayed())))
    }

    @Test
    fun `should include Reset Checklist on the Options Menu on the ActionBar`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_ID, nonEmptyChecklistId)

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            val expectedMenuItemText = it.requireActivity().getString(R.string.reset_checklist)
            val menuItem = shadowOf(it.activity).optionsMenu.findItem(R.id.reset_checklist)
            assertThat(menuItem.title.toString(), `is`(expectedMenuItemText))
            assertThat(menuItem.isVisible, `is`(true))
        }
    }

    @Test
    fun `should include Edit Checklist Item on the Options Menu`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_ID, nonEmptyChecklistId)

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            val expectedMenuItemText = it.requireActivity().getString(R.string.add_checklist_item)
            val menuItem = shadowOf(it.activity).optionsMenu.findItem(R.id.add_checklist_item)

            assertThat(menuItem.title.toString(), `is`(expectedMenuItemText))
            assertThat(menuItem.isVisible, `is`(true))
        }
    }

    @Test
    fun `should adjust the Options Menu for an Empty Checklist`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_ID, emptyChecklistId)

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            val roboMenu = RoboMenu(it.activity)
            roboMenu.add(R.menu.checklist_options)
            val menuInflater = it.requireActivity().menuInflater
            it.onCreateOptionsMenu(roboMenu, menuInflater)
            it.onPrepareOptionsMenu(roboMenu)

            val menuItem = roboMenu.findItem(R.id.reset_checklist)
            assertThat(menuItem.isEnabled, `is`(false))
        }
    }

    @Test
    fun `should Adjust the Options Menu for a Checklist that is at Rest`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_ID, nonEmptyChecklistId)

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            val roboMenu = RoboMenu(it.activity)
            roboMenu.add(R.menu.checklist_options)
            val menuInflater = it.requireActivity().menuInflater
            it.onCreateOptionsMenu(roboMenu, menuInflater)
            it.onPrepareOptionsMenu(roboMenu)

            val resetChecklistMenuItem = roboMenu.findItem(R.id.reset_checklist)
            assertThat(resetChecklistMenuItem.isEnabled, `is`(false))

            val addChecklistItemMenuItem = roboMenu.findItem(R.id.add_checklist_item)
            assertThat(addChecklistItemMenuItem.isEnabled, `is`(true))
        }
    }

    @Test
    fun `should Inflate the Default Checklist Options Menu Default Items`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_ID, nonEmptyChecklistId)

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            val menuInflater = it.requireActivity().menuInflater
            val menu = MenuBuilder(it.requireActivity())

            it.onCreateOptionsMenu(menu, menuInflater)

            val resources = it.resources

            assertThat(menu.hasVisibleItems(), `is`(true))
            assertThat(menu.size(), `is`(2))
            assertThat(menu.getItem(0).title, `is`<CharSequence>(resources.getString(R.string.add_checklist_item)))
            assertThat(menu.getItem(1).title, `is`<CharSequence>(resources.getString(R.string.reset_checklist)))
        }
    }

    @Test
    fun `should Finish the Fragment After Home Tap on the Options Menu`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_ID, nonEmptyChecklistId)

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            it.onOptionsItemSelected(withMenuItem(android.R.id.home))
            verify { mockChecklistFragmentListener.finishFragment() }
        }
    }

    @Test
    fun `should Start the EditChecklistItemFragment when Adding a checklist Item from the Options Menu`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_ID, nonEmptyChecklistId)

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            it.onOptionsItemSelected(withMenuItem(R.id.add_checklist_item))
            verify {
                mockChecklistFragmentListener.startEditChecklistItemFragment(
                        checklistId = nonEmptyChecklistId,
                        checklistItemId = BundleKeys.UNDEFINED
                )
            }
        }
    }

    @Test
    fun `should Update the Title Information of the Fragment with Aircraft and Checklist Information`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_ID, nonEmptyChecklistId)

        launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        verify { mockChecklistFragmentListener.updateToolbarTitle(checklistName) }

    }

    @Test
    fun `should Display the Deleted Checklist Item Dialog after Deleting a ChecklistItem`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_ID, nonEmptyChecklistId)

        val scenario = launchFragmentInContainer(
            instantiate = instantiate,
            fragmentArgs = arguments
        )

        val checklistItemName = randomName()

        scenario.onFragment { fragment -> fragment.displayDeletedChecklistItemDialog(checklistItemName) }

        val expectedMessage = "Deleted Checklist Item: $checklistItemName"
        verify { mockChecklistFragmentListener.displaySnackbar(expectedMessage) }
    }

    @Test
    fun `should store the active checklist in the outstate bundle when saving the instance state`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_ID, nonEmptyChecklistId)

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        val outstateBundle = Bundle()
        scenario.onFragment { fragment -> fragment.onSaveInstanceState(outstateBundle) }

        val checklistAfterSave: Checklist? = outstateBundle.getParcelable(BundleKeys.CHECKLIST)
        assertThat(checklistAfterSave, `is`(nonEmptyChecklist))
    }
// ---------------------------------------
//    @Ignore("NEEDS MANUAL TEST - Robolectric and Recyclerviews don't play nicely")
//    @Test
//    fun `should Adjust the Options Menu for a Checklist that is being Played`() {
//        val checklistFragment = startVisibleChecklistFragment(nonEmptyChecklistId)
//
//        val playButton = checklistFragment.requireView().findViewById<Button>(R.id.play_button)
//        playButton.performClick()  // start, highlight 0
//        playButton.performClick()  // check 0, highlight 1
//
//        val roboMenu = openOptionsMenu(checklistFragment)
//
//        val menuItem = roboMenu.findItem(R.id.reset_checklist)
//        assertThat(menuItem.isEnabled, `is`(true))
//
//        val addChecklistItemMenuItem = roboMenu.findItem(R.id.add_checklist_item)
//        assertThat(addChecklistItemMenuItem.isEnabled, `is`(false))
//    }
//
//    @Ignore("NEEDS MANUAL TEST - Robolectric and Recyclerviews don't play nicely")
//    @Test
//    fun `should Adjust the Options Menu for a Checklist that is completed`() {
//        val checklistFragment = startVisibleChecklistFragment(nonEmptyChecklistId)
//
//        val playButton = checklistFragment.requireView().findViewById<Button>(R.id.play_button)
//        playButton.performClick()  // start, highlight 0
//        playButton.performClick()  // check 0, highlight 1
//        playButton.performClick()  // check 1, show completed dialog
//
//        val roboMenu = openOptionsMenu(checklistFragment)
//
//        val menuItem = roboMenu.findItem(R.id.reset_checklist)
//        assertThat(menuItem.isEnabled, `is`(true))
//
//        val addChecklistItemMenuItem = roboMenu.findItem(R.id.add_checklist_item)
//        assertThat(addChecklistItemMenuItem.isEnabled, `is`(false))
//    }
//
//    @Ignore("NEEDS MANUAL TEST - Robolectric and Recyclerviews don't play nicely")
//    @Test
//    fun `should Reset the Buttons when the Checklist is set to the Preplayed State`() {
//        val checklistFragment = startVisibleChecklistFragment(nonEmptyChecklistId)
//
//        val playButton = checklistFragment.requireView().findViewById<Button>(R.id.play_button)
//        playButton.performClick()
//
//        val resetChecklistMenuItem = mockk<MenuItem>()
//        every { resetChecklistMenuItem.itemId } returns R.id.reset_checklist
//        every { resetChecklistMenuItem.itemId } returns R.id.reset_checklist
//
//        checklistFragment.onOptionsItemSelected(resetChecklistMenuItem)
//
//        checklistFragment.onOptionsItemSelected(resetChecklistMenuItem)
//
//        val altPlayButton = checklistFragment.requireView().findViewById<ImageView>(R.id.play_button)
//        val playIconDrawable = shadowOf(altPlayButton.drawable)
//        assertThat(playIconDrawable.createdFromResId, `is`(R.drawable.ic_play_arrow))
//
//        val altResetButton = checklistFragment.requireView().findViewById<ImageView>(R.id.revisit_previously_skipped_button)
//        assertThat(altResetButton.visibility, `is`(INVISIBLE))
//
//        val altSkipButton = checklistFragment.requireView().findViewById<ImageView>(R.id.skip_button)
//        assertThat(altSkipButton.visibility, `is`(INVISIBLE))
//    }

    private fun withMenuItem(menuItemId: Int): MenuItem {
        val mockMenuItem = mockk<MenuItem>()
        every { mockMenuItem.itemId } returns menuItemId
        return mockMenuItem
    }
}