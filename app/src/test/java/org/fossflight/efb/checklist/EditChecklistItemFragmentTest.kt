package org.fossflight.efb.checklist

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.Spinner
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withSpinnerText
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.android.material.textfield.TextInputLayout
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Completable
import io.reactivex.Single
import org.fossflight.efb.BundleKeys
import org.fossflight.efb.ForceTrampolineSchedulers
import org.fossflight.efb.FossFlightFragmentListener
import org.fossflight.efb.FossFlightTestApplication
import org.fossflight.efb.R
import org.fossflight.efb.TestAppModule
import org.fossflight.efb.data.model.Checklist
import org.fossflight.efb.data.model.ChecklistItemType
import org.fossflight.efb.data.persistence.FFRepository
import org.fossflight.efb.data.randomChecklistItemAction
import org.fossflight.efb.data.randomId
import org.fossflight.efb.data.randomName
import org.fossflight.efb.data.testChecklistItem
import org.hamcrest.CoreMatchers.containsString
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@Config(application = FossFlightTestApplication::class)
@RunWith(AndroidJUnit4::class)
class EditChecklistItemFragmentTest {

    @get:Rule
    var forceTrampolineSchedulers = ForceTrampolineSchedulers()

    private val checklistName = randomName()
    private val checklistId = randomId()

    val itemId = randomId()
    val checklistItemName = randomName()
    val checklistItemAction = randomChecklistItemAction()
    val checklistItem = testChecklistItem(
            id = itemId,
            checkListId = checklistId,
            name = checklistItemName,
            action = checklistItemAction,
            type = ChecklistItemType.SECTION_HEADER.toString()
    )

    private val mockRepository = mockk<FFRepository>()
    private val mockFossFlightFragmentListener = mockk<FossFlightFragmentListener>()

    val bundleForExistingItem = Bundle()

    val instantiate: () -> EditChecklistItemFragment = {
        val fragment = EditChecklistItemFragment()
        fragment.setParentActivity(mockFossFlightFragmentListener)
        fragment
    }

    @Before
    fun `set up mocks and activity`() {
        val application = ApplicationProvider.getApplicationContext<FossFlightTestApplication>().apply {
            setTheme(R.style.AppTheme)
        }
        val testAppModule = TestAppModule(
                context = application.applicationContext,
                repository = mockRepository
        )

        application.setAppModule(testAppModule)

        every { mockFossFlightFragmentListener.updateToolbarTitle(any()) } returns Unit
        every { mockFossFlightFragmentListener.updateToolbarSubtitle(any()) } returns Unit
        every { mockFossFlightFragmentListener.displayToast(any()) } returns Unit
        every { mockFossFlightFragmentListener.finishFragment() } returns Unit

        every { mockRepository.getChecklistItem(itemId) } returns Single.just(checklistItem)
        every { mockRepository.getChecklist(checklistId) } returns Single.just(Checklist(name = checklistName))
        every { mockRepository.saveChecklistItem(any()) } returns Completable.complete()

        bundleForExistingItem.putLong(BundleKeys.CHECKLIST_ID, checklistId)
        bundleForExistingItem.putLong(BundleKeys.CHECKLIST_ITEM_ID, itemId)
    }

    @Test
    fun `should Update the Title Information with Checklist Information when adding a checklist item`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_ID, checklistId)
        arguments.putLong(BundleKeys.CHECKLIST_ITEM_ID, BundleKeys.UNDEFINED)

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            val expectedTitle = it.requireActivity().getString(R.string.add_checklist_item)
            verify { mockFossFlightFragmentListener.updateToolbarTitle(expectedTitle) }
            val expectedSubtitle = "$checklistName Checklist"
            verify { mockFossFlightFragmentListener.updateToolbarSubtitle(expectedSubtitle) }
        }
    }

    @Test
    fun `should Update the Title Information with Checklist Information when updating a checklist item`() {
        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = bundleForExistingItem
        )

        scenario.onFragment {
            val expectedTitle = it.requireActivity().getString(R.string.edit_checklist_item)
            verify { mockFossFlightFragmentListener.updateToolbarTitle(expectedTitle) }
            val expectedSubtitle = "$checklistName Checklist"
            verify { mockFossFlightFragmentListener.updateToolbarSubtitle(expectedSubtitle) }
        }
    }

    @Test
    fun `should Display The Checklist Item Information when Editing An existing ChecklistItem`() {
        launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = bundleForExistingItem
        )

        onView(withId(R.id.checklist_item_name_edit)).check(matches(withText(containsString(checklistItemName))))
        onView(withId(R.id.checklist_item_action_edit)).check(matches(withText(containsString(checklistItemAction))))
        onView(withId(R.id.checklist_item_type_spinner)).check(matches(withSpinnerText(containsString(ChecklistItemType.SECTION_HEADER.toString()))))
    }

    @Test
    fun `should Save the ChecklistItem Details, display a successful toast and finish the fragment when Save Icon Is Clicked`() {
        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = bundleForExistingItem
        )

        val newChecklistItemName = randomName()
        val newChecklistItemAction = randomChecklistItemAction()
        val updatedChecklistItem = testChecklistItem(
                id = itemId,
                checkListId = checklistId,
                name = newChecklistItemName,
                action = newChecklistItemAction,
                type = ChecklistItemType.SECTION_HEADER.toString()
        )

        scenario.onFragment {
            it.requireActivity().findViewById<EditText>(R.id.checklist_item_name_edit).setText(newChecklistItemName)
            it.requireActivity().findViewById<EditText>(R.id.checklist_item_action_edit).setText(newChecklistItemAction)
            it.requireActivity().findViewById<Spinner>(R.id.checklist_item_type_spinner).setSelection(ChecklistItemType.SECTION_HEADER.ordinal)

            it.onOptionsItemSelected(withMenuItem(R.id.save))
        }

        verify { mockRepository.saveChecklistItem(updatedChecklistItem) }
        verify { mockFossFlightFragmentListener.displayToast("Saved $newChecklistItemName") }
        verify { mockFossFlightFragmentListener.finishFragment() }
    }

    @Test
    fun `should finish the fragment after a Cancel click`() {
        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = bundleForExistingItem
        )

        scenario.onFragment { it.onOptionsItemSelected(withMenuItem(android.R.id.home)) }

        verify (exactly = 0) { mockRepository.saveChecklistItem(any()) }
        verify (exactly = 0) { mockFossFlightFragmentListener.displayToast(any()) }
        verify (exactly = 1) { mockFossFlightFragmentListener.finishFragment() }
    }

    @Test
    fun `should display A Name Required Violation when attempting to save with an empty Item Name`() {
        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = bundleForExistingItem
        )

        scenario.onFragment {
            it.requireActivity().findViewById<EditText>(R.id.checklist_item_name_edit).setText("")

            it.onOptionsItemSelected(withMenuItem(R.id.save))
        }

        val expectedErrorMessage = "The Checklist Item Name is Required"
        verify { mockFossFlightFragmentListener.displayToast(expectedErrorMessage) }
        onView(withId(R.id.input_layout_name_one)).check(matches(hasTextInputLayoutErrorText(expectedErrorMessage)))
    }

    private fun withMenuItem(menuItemId: Int): MenuItem {
        val mockMenuItem = mockk<MenuItem>()
        every { mockMenuItem.itemId } returns menuItemId
        return mockMenuItem
    }
}

fun hasTextInputLayoutErrorText(expectedErrorText: String): Matcher<View> = object : TypeSafeMatcher<View>() {
    override fun describeTo(description: Description?) { }
    override fun matchesSafely(item: View?): Boolean {
        if (item !is TextInputLayout) return false
        val error = item.error ?: return false
        val errorMessage = error.toString()
        return expectedErrorText == errorMessage
    }
}