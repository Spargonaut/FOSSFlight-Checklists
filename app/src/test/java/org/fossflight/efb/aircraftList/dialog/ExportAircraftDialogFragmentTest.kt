package org.fossflight.efb.aircraftList.dialog

import android.app.Dialog
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ApplicationProvider
import androidx.test.core.app.launchActivity
import androidx.test.ext.junit.runners.AndroidJUnit4
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Flowable
import io.reactivex.Single
import org.fossflight.efb.FossFlightTestApplication
import org.fossflight.efb.MainActivity
import org.fossflight.efb.PermissionsManager
import org.fossflight.efb.TestAppModule
import org.fossflight.efb.aircraftList.AircraftListFragment
import org.fossflight.efb.data.persistence.FFRepository
import org.fossflight.efb.data.randomId
import org.fossflight.efb.data.randomTailNumber
import org.fossflight.efb.data.testAircraft
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import org.robolectric.shadows.ShadowAlertDialog
import java.io.File

@Config(application = FossFlightTestApplication::class)
@RunWith(AndroidJUnit4::class)
class ExportAircraftDialogFragmentTest {
    private lateinit var latestDialog: Dialog

    @Before
    fun `create Aircraft and Display Dialog`() {
        val mockRepository = mockk<FFRepository>()
        every { mockRepository.allAircraft } returns Flowable.just(listOf())
        every { mockRepository.getAircraft(any()) } returns Single.just(testAircraft())

        val mockPermissionsManager = mockk<PermissionsManager>()
        every { mockPermissionsManager.hasWritePermission(any()) } returns true
        every { mockPermissionsManager.canWriteToStorage() } returns true
        every { mockPermissionsManager.getWritableDirectory(any()) } returns mockk<File>(relaxed = true)

        val application = ApplicationProvider.getApplicationContext<FossFlightTestApplication>()
        val testAppModule = TestAppModule(
                context = application.applicationContext,
                repository = mockRepository,
                permissionsManager = mockPermissionsManager
        )
        application.setAppModule(testAppModule)

        val scenario = launchActivity<MainActivity>()
        scenario.moveToState(Lifecycle.State.CREATED)
        scenario.moveToState(Lifecycle.State.RESUMED)
        scenario.onActivity {
            val aircraftListFragment = it.supportFragmentManager
                    .findFragmentByTag(AircraftListFragment::class.java.simpleName) as AircraftListFragment
            aircraftListFragment.displayExportAircraftDialog(randomId(), randomTailNumber())
        }

        latestDialog = ShadowAlertDialog.getLatestDialog()
    }

    @Test
    fun `should Display The Export Aircraft Dialog`() {
        assertThat(latestDialog.isShowing, `is`(true))
    }

    @Test
    fun `should Dismiss The Dialog When Clicking The Cancel Button`() {
        val cancelButton = (latestDialog as AlertDialog).getButton(AlertDialog.BUTTON_NEGATIVE)
        val actualCancelButtonText = cancelButton.text.toString()

        assertThat(actualCancelButtonText, `is`("Cancel"))

        cancelButton.performClick()
        assertThat(latestDialog.isShowing, `is`(false))
    }

    @Test
    fun `should Dismissing The Dialog After Clicking The Export Button`() {
        val export = (latestDialog as AlertDialog).getButton(AlertDialog.BUTTON_POSITIVE)
        val actualExportButtonText = export.text.toString()

        assertThat(actualExportButtonText, `is`("Export"))

        export.performClick()
        assertThat(latestDialog.isShowing, `is`(false))
    }
}