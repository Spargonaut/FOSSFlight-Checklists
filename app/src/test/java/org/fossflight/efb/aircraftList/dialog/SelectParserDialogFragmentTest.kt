package org.fossflight.efb.aircraftList.dialog

import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import androidx.test.core.app.launchActivity
import androidx.test.ext.junit.runners.AndroidJUnit4
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Flowable
import org.fossflight.efb.FossFlightTestApplication
import org.fossflight.efb.MainActivity
import org.fossflight.efb.R
import org.fossflight.efb.TestAppModule
import org.fossflight.efb.aircraftList.AircraftListFragment
import org.fossflight.efb.data.persistence.FFRepository
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import org.robolectric.shadows.ShadowAlertDialog

@Config(application = FossFlightTestApplication::class)
@RunWith(AndroidJUnit4::class)
class SelectParserDialogFragmentTest {

    private lateinit var scenario: ActivityScenario<MainActivity>

    @Before
    fun `create Aircraft Fragment and Open Import Dialog`() {
        val mockRepository = mockk<FFRepository>()
        every { mockRepository.allAircraft } returns Flowable.just(listOf())

        val application = ApplicationProvider.getApplicationContext<FossFlightTestApplication>()
        val testAppModule = TestAppModule(
                context = application.applicationContext,
                repository = mockRepository
        )
        application.setAppModule(testAppModule)

        scenario = launchActivity<MainActivity>()
        scenario.moveToState(Lifecycle.State.CREATED)
        scenario.moveToState(Lifecycle.State.RESUMED)
        scenario.onActivity {
            val aircraftListFragment = it.supportFragmentManager
                    .findFragmentByTag(AircraftListFragment::class.java.simpleName) as AircraftListFragment
            val indexForImportingAnAircraft = 0
            aircraftListFragment.addAircraft(indexForImportingAnAircraft)
        }
    }

    @Test
    fun shouldDisplayTheSelectParserDialog() {
        val latestDialog = ShadowAlertDialog.getLatestDialog()
        assertThat(latestDialog.isShowing, `is`(true))
    }

    @Test
    fun `should dismiss the Dialog when tapping the Cancel button`() {
        scenario.onActivity {
            val latestDialog = ShadowAlertDialog.getLatestDialog()
            val cancelButton = (latestDialog as AlertDialog).getButton(AlertDialog.BUTTON_NEGATIVE)
            val actualCancelButtonText = cancelButton.text.toString()

            val expectedCancelButtonText = it.getString(R.string.cancel)
            assertThat(actualCancelButtonText, `is`(expectedCancelButtonText))

            cancelButton.performClick()
            assertThat(latestDialog.isShowing(), `is`(false))
        }
    }

    @Test
    fun `should include FOSSFlight Checklist as first item on menu`() {
        scenario.onActivity {
            val latestDialog = ShadowAlertDialog.getLatestDialog()
            val indexForOptionOne = 0
            val actualItem = (latestDialog as AlertDialog).listView.getItemAtPosition(indexForOptionOne).toString()

            val expectedOptionOneText = it.getString(R.string.fossflight_checklist)
            assertThat(actualItem, `is`(expectedOptionOneText))
        }
    }

    @Test
    fun `should include FltPlanGo checklist as second item on menu`() {
        scenario.onActivity {
            val latestDialog = ShadowAlertDialog.getLatestDialog()
            val indexForOptionTwo = 1
            val actualItem = (latestDialog as AlertDialog).listView.getItemAtPosition(indexForOptionTwo).toString()

            val expectedOptionTwoText = it.getString(R.string.fltplan_go_checklist)
            assertThat(actualItem, `is`(expectedOptionTwoText))
        }
    }
}