package org.fossflight.efb.aircraftList

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import org.fossflight.efb.BundleKeys
import org.fossflight.efb.ForceTrampolineSchedulers
import org.fossflight.efb.data.io.FileImporter
import org.fossflight.efb.data.persistence.FFRepository
import org.fossflight.efb.data.randomId
import org.fossflight.efb.data.randomTailNumber
import org.fossflight.efb.data.testAircraft
import org.hamcrest.core.Is.`is`
import org.junit.Assert.assertThat
import org.junit.Rule
import org.junit.Test
import java.io.InputStream

class AircraftListPresenterTest {

    @get:Rule
    var forceTrampolineSchedulers = ForceTrampolineSchedulers()

    private val path = "filePath"
    private val fileName = "fileName"

    private var mockRepository = mockk<FFRepository>()
    private var mockAircraftListView = mockk<AircraftListView>(relaxed = true)
    private var mockInputStream = mockk<InputStream>()
    private var mockFileImporter = mockk<FileImporter>()

    private var aircraftListPresenter = AircraftListPresenter(
            aircraftListView = mockAircraftListView,
            repository = mockRepository
    )

    @Test
    @Throws(Exception::class)
    fun `should import the files to database when able to read directory`() {

        val aircraft = testAircraft(id = BundleKeys.UNDEFINED)

        every { mockFileImporter.importAircraftFromStream(path, mockInputStream) } returns aircraft

        aircraft.id = 1L
        every { mockRepository.insertAircraft(aircraft) } returns Observable.just(aircraft)

        val insertedAircraftList = listOf(aircraft)
        every { mockRepository.allAircraft } returns Flowable.just(insertedAircraftList)

        aircraftListPresenter.importChecklist(path, mockInputStream, mockFileImporter)

        verify { mockRepository.insertAircraft(aircraft) }
        verify { mockAircraftListView.updateRecyclerView(insertedAircraftList) }
    }

    @Test
    @Throws(Exception::class)
    fun `should create an Import Problem Dialog when catching an IOException during Import`() {

        every { mockFileImporter.importAircraftFromStream(fileName, mockInputStream) } throws Exception()

        aircraftListPresenter.importChecklist(fileName, mockInputStream, mockFileImporter)

        verify { mockAircraftListView.displayImportProblemDialog() }
    }

    @Test
    fun `should hide the Empty List Message when presenting the data at least one Aircraft`() {
        every { mockRepository.allAircraft } returns Flowable.just(listOf(testAircraft()))

        aircraftListPresenter.presentData()

        verify { mockAircraftListView.updateUIForNonEmptyItemList() }
    }

    @Test
    @Throws(Exception::class)
    fun `should hide the Empty List Message after Importing`() {

        val importedAircraft = testAircraft(id = BundleKeys.UNDEFINED)
        val insertedAircraft = testAircraft(id = 1L)

        val aircraftsFromRepo = listOf(insertedAircraft)

        every {
            mockRepository.allAircraft
        } answers {
            Flowable.just(listOf())
        } andThen {
            Flowable.just(aircraftsFromRepo)
        }

        every { mockFileImporter.importAircraftFromStream(path, mockInputStream) } returns importedAircraft

        every { mockRepository.insertAircraft(importedAircraft) } returns Observable.just(insertedAircraft)

        aircraftListPresenter.presentData()

        aircraftListPresenter.importChecklist(path, mockInputStream, mockFileImporter)

        verify { mockAircraftListView.updateUIForNonEmptyItemList() }
    }

    @Test
    fun `should display the Empty List Message when the Aircraft List is Empty`() {
        every { mockRepository.allAircraft } returns Flowable.just(listOf())

        aircraftListPresenter.presentData()

        verify { mockAircraftListView.updateUIForEmptyItemList() }
    }

    @Test
    fun `should present Data after Loading from Database`() {
        val aircraftList = listOf(testAircraft())
        every { mockRepository.allAircraft } returns Flowable.just(aircraftList)

        aircraftListPresenter.presentData()

        verify { mockAircraftListView.updateRecyclerView(aircraftList) }
    }

    @Test
    fun `should Delete An Aircraft From The Repository`() {
        val aircraft = testAircraft()

        every { mockRepository.allAircraft } returns Flowable.just(mutableListOf(aircraft))
        every { mockRepository.deleteAircraft(aircraft) } returns Completable.complete()

        aircraftListPresenter.presentData()

        aircraftListPresenter.deleteAircraft(aircraft)

        verify { mockRepository.deleteAircraft(aircraft) }
    }

    @Test
    fun `should Display A Message Dialog after Deleting an Aircraft`() {
        val aircraft = testAircraft()

        every { mockRepository.allAircraft } returns Flowable.just(mutableListOf(aircraft))
        every { mockRepository.deleteAircraft(aircraft) } returns Completable.complete()

        aircraftListPresenter.presentData()

        aircraftListPresenter.deleteAircraft(aircraft)

        verify { mockAircraftListView.displayDeletedAircraftDialog(aircraft.tailNumber) }
    }

    @Test
    fun `should Display the Empty List Message after Deleting the Last Aircraft`() {
        val aircraft = testAircraft()

        every { mockRepository.allAircraft  } returns Flowable.just(mutableListOf(aircraft))
        every { mockRepository.deleteAircraft(aircraft) } returns Completable.complete()

        aircraftListPresenter.presentData()

        aircraftListPresenter.deleteAircraft(aircraft)

        verify { mockAircraftListView.updateUIForEmptyItemList() }
    }

    @Test
    fun `should Update the Aircraft Collection After Deleting an Aircraft`() {
        val aircraftOne = testAircraft()
        val aircraftTwo = testAircraft()

        val originalAircraftList = listOf(aircraftOne, aircraftTwo)
        val aircraftListAfterDeleting = listOf(aircraftTwo)

        every { mockRepository.allAircraft } returns Flowable.just(originalAircraftList)
        every { mockRepository.deleteAircraft(aircraftOne) } returns Completable.complete()

        aircraftListPresenter.presentData()

        aircraftListPresenter.deleteAircraft(aircraftOne)

        verify { mockAircraftListView.updateRecyclerView(originalAircraftList) }
        verify { mockAircraftListView.updateRecyclerView(aircraftListAfterDeleting) }
        verify { mockAircraftListView.displayDeletedAircraftDialog(aircraftOne.tailNumber) }
    }

    @Test
    fun `should Start the Checklist Collection from the AircraftListView`() {
        val aircraftId = randomId()
        val aircraft = testAircraft(id = aircraftId)

        every { mockRepository.allAircraft } returns Flowable.just(listOf(aircraft))

        aircraftListPresenter.presentData()

        aircraftListPresenter.startChecklistCollectionFragment(aircraft.id)

        verify { mockAircraftListView.startChecklistCollectionFragment(aircraftId) }
    }

    @Test
    fun `should Edit the Aircraft from the AircraftListView`() {
        val aircraftId = randomId()
        val aircraft = testAircraft(id = aircraftId)

        every { mockRepository.allAircraft } returns Flowable.just(listOf(aircraft))

        aircraftListPresenter.presentData()

        aircraftListPresenter.editAircraft(aircraft.id)

        verify { mockAircraftListView.editAircraft(aircraftId) }
    }

    @Test
    fun `should Export the Aircraft from the AircraftListView`() {
        val aircraftId = randomId()
        val tailNumber = randomTailNumber()
        val aircraft = testAircraft(id = aircraftId, tailNumber = tailNumber)

        every { mockRepository.allAircraft } returns Flowable.just(listOf(aircraft))
        every { mockRepository.getAircraftTailNumber(aircraftId) } returns Single.just(tailNumber)

        aircraftListPresenter.presentData()

        aircraftListPresenter.exportAircraft(aircraft.id)

        verify { mockAircraftListView.displayExportAircraftDialog(aircraftId, tailNumber) }
    }

    @Test
    fun `should Dispose of the CompositeDisposable`() {

        every { mockRepository.allAircraft } returns Flowable.just(listOf(testAircraft()))

        val compositeDisposable = CompositeDisposable()
        val aircraftListPresenter = AircraftListPresenter(
                aircraftListView = mockAircraftListView,
                repository = mockRepository,
                compositeDisposable = compositeDisposable
        )

        aircraftListPresenter.presentData()
        aircraftListPresenter.dispose()

        assertThat(compositeDisposable.isDisposed, `is`(true))
    }

    @Test
    fun `should add a subscription to the CompositeDisposable after Presenting the Data`() {
        every { mockRepository.allAircraft } returns Flowable.just(listOf(testAircraft()))

        val compositeDisposable = CompositeDisposable()
        val aircraftListPresenter = AircraftListPresenter(
                aircraftListView = mockAircraftListView,
                repository = mockRepository,
                compositeDisposable = compositeDisposable
        )

        aircraftListPresenter.presentData()

        assertThat(compositeDisposable.size(), `is`(1))
    }

    @Test
    @Throws(Exception::class)
    fun `should add two subscriptions to the CompositeDisposable after deleting an Aircraft`() {
        val importedAircraft = testAircraft(BundleKeys.UNDEFINED)

        val insertedAircraft = testAircraft(id = 1L)

        val aircraftsFromRepo = listOf(insertedAircraft)

        every {
            mockRepository.allAircraft
        } answers {
            Flowable.just(listOf())
        } andThen {
            Flowable.just(aircraftsFromRepo)
        }

        every { mockFileImporter.importAircraftFromStream(path, mockInputStream) } returns importedAircraft

        every { mockRepository.insertAircraft(importedAircraft) } returns Observable.just(insertedAircraft)

        val compositeDisposable = CompositeDisposable()
        val aircraftListPresenter = AircraftListPresenter(
                aircraftListView = mockAircraftListView,
                repository = mockRepository,
                compositeDisposable = compositeDisposable
        )

        aircraftListPresenter.presentData()
        aircraftListPresenter.importChecklist(path, mockInputStream, mockFileImporter)

        assertThat(compositeDisposable.size(), `is`(3))
    }

    @Test
    fun `should add two subscriptions to the CompositeDisposable after Importing an Aircraft`() {
        val aircraft = testAircraft()

        every { mockRepository.allAircraft } returns Flowable.just(mutableListOf(aircraft))
        every { mockRepository.deleteAircraft(aircraft) } returns Completable.complete()

        val compositeDisposable = CompositeDisposable()
        val aircraftListPresenter = AircraftListPresenter(
                aircraftListView = mockAircraftListView,
                repository = mockRepository,
                compositeDisposable = compositeDisposable
        )
        aircraftListPresenter.presentData()

        aircraftListPresenter.deleteAircraft(aircraft)

        assertThat(compositeDisposable.size(), `is`(2))
    }
}