package org.fossflight.efb.aircraftList

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import org.fossflight.efb.BundleKeys
import org.fossflight.efb.BundleKeys.EMERGENCY_CHECKLIST_COLLECTION_NAME
import org.fossflight.efb.BundleKeys.STANDARD_CHECKLIST_COLLECTION_NAME
import org.fossflight.efb.ForceTrampolineSchedulers
import org.fossflight.efb.data.model.Aircraft
import org.fossflight.efb.data.persistence.FFRepository
import org.fossflight.efb.data.randomId
import org.fossflight.efb.data.randomName
import org.fossflight.efb.data.randomTailNumber
import org.fossflight.efb.data.testAircraft
import org.fossflight.efb.data.testChecklistCollection
import org.hamcrest.core.Is.`is`
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.util.concurrent.ThreadLocalRandom

class EditAircraftPresenterTest {

    @get:Rule
    var forceTrampolineSchedulers = ForceTrampolineSchedulers()

    private val tailNumber = randomTailNumber()
    private val aircraftId = randomId()

    private val mockEditAircraftView = mockk<EditAircraftView>()
    private val mockRepository = mockk<FFRepository>()

    @Before
    fun `set up mocks`() {
        every { mockEditAircraftView.abortEdit() } returns Unit
        every { mockEditAircraftView.saveSuccessful(any()) } returns Unit
        every { mockEditAircraftView.updateAircraftInfo(any()) } returns Unit
        every { mockEditAircraftView.updateToolbarForAdd() } returns Unit
        every { mockEditAircraftView.updateToolbarForUpdate() } returns Unit

        every { mockRepository.saveAircraft(any()) } returns Observable.just(testAircraft())
        every { mockRepository.updateAircraft(any()) } returns Completable.complete()
    }

    @Test
    fun `should update the toolbar info when adding an Aircraft`() {
        val editAircraftPresenter = EditAircraftPresenter(
                editAircraftView = mockEditAircraftView,
                repository = mockRepository,
                aircraftId = BundleKeys.UNDEFINED
        )
        editAircraftPresenter.presentData()

        verify { mockEditAircraftView.updateToolbarForAdd() }
    }

    @Test
    fun `should update the toolbar info when updating an Aircraft`() {

        every {
            mockRepository.getShallowAircraft(aircraftId)
        } returns Single.just(Aircraft(id = aircraftId, tailNumber = tailNumber))

        val editAircraftPresenter = EditAircraftPresenter(
                editAircraftView = mockEditAircraftView,
                repository = mockRepository,
                aircraftId = aircraftId
        )
        editAircraftPresenter.presentData()

        verify { mockEditAircraftView.updateToolbarForUpdate() }
    }

    @Test
    fun `should save a new Aircraft when adding an aircraft`() {
        val editAircraftPresenter = EditAircraftPresenter(
                editAircraftView = mockEditAircraftView,
                repository = mockRepository,
                aircraftId = BundleKeys.UNDEFINED
        )
        editAircraftPresenter.presentData()

        val manufacturer = randomName()
        val model = randomName()
        val year = ThreadLocalRandom.current().nextInt(Integer.MAX_VALUE).toString()
        val collectionOne = testChecklistCollection(
                id = 0, aircraftId = 0,
                name = STANDARD_CHECKLIST_COLLECTION_NAME,
                orderNumber = 0

        )
        val collectionTwo = testChecklistCollection(
                id = 0, aircraftId = 0,
                name = EMERGENCY_CHECKLIST_COLLECTION_NAME,
                orderNumber = 1
        )
        val defaultCollections = listOf(collectionOne, collectionTwo)

        val expectedAircraft = Aircraft(
                id = BundleKeys.UNDEFINED,
                tailNumber = tailNumber,
                manufacturer = manufacturer,
                model = model,
                year = year.toInt(),
                checklistCollections = defaultCollections
        )

        every { mockRepository.saveAircraft(any()) } returns Observable.just(expectedAircraft)

        editAircraftPresenter.saveAircraft(
                tailNumber = tailNumber,
                manufacturer = manufacturer,
                model = model,
                year = year
        )

        verify { mockRepository.saveAircraft(expectedAircraft) }
    }

    @Test
    fun `should save a new Aircraft with the year as zero when adding an aircraft and the year is an empty string`() {
        val editAircraftPresenter = EditAircraftPresenter(
                editAircraftView = mockEditAircraftView,
                repository = mockRepository,
                aircraftId = BundleKeys.UNDEFINED
        )
        editAircraftPresenter.presentData()

        val collectionOne = testChecklistCollection(
                id = 0, aircraftId = 0,
                name = STANDARD_CHECKLIST_COLLECTION_NAME,
                orderNumber = 0

        )
        val collectionTwo = testChecklistCollection(
                id = 0, aircraftId = 0,
                name = EMERGENCY_CHECKLIST_COLLECTION_NAME,
                orderNumber = 1
        )
        val defaultCollections = listOf(collectionOne, collectionTwo)

        val expectedAircraft = Aircraft(
                id = BundleKeys.UNDEFINED,
                tailNumber = tailNumber,
                year = 0,
                checklistCollections = defaultCollections
        )

        editAircraftPresenter.saveAircraft(tailNumber = tailNumber, year = "")

        verify { mockRepository.saveAircraft(expectedAircraft) }
    }

    @Test
    fun `should save updated Aircraft information when updating an aircraft`() {

        val aircraft = Aircraft(id = aircraftId, tailNumber = tailNumber)

        every { mockRepository.getShallowAircraft(aircraftId) } returns Single.just(aircraft)

        val editAircraftPresenter = EditAircraftPresenter(
                editAircraftView = mockEditAircraftView,
                repository = mockRepository,
                aircraftId = aircraftId
        )
        editAircraftPresenter.presentData()

        val manufacturer = randomName()
        val model = randomName()
        val year = ThreadLocalRandom.current().nextInt(Integer.MAX_VALUE).toString()

        val expectedAircraft = aircraft.copy(
                manufacturer = manufacturer,
                model = model,
                year = year.toInt()
        )

        editAircraftPresenter.saveAircraft(
                tailNumber = tailNumber,
                manufacturer = manufacturer,
                model = model,
                year = year
        )

        verify { mockRepository.updateAircraft(expectedAircraft) }
    }

    @Test
    fun `should save updated Aircraft with the year as zero when updating an aircraft and the year is an empty string`() {
        val aircraft = Aircraft(id = aircraftId, tailNumber = tailNumber)
        every { mockRepository.getShallowAircraft(aircraftId) } returns Single.just(aircraft)

        val editAircraftPresenter = EditAircraftPresenter(
                editAircraftView = mockEditAircraftView,
                repository = mockRepository,
                aircraftId = aircraftId
        )
        editAircraftPresenter.presentData()

        val expectedAircraft = aircraft.copy(year = 0)

        editAircraftPresenter.saveAircraft(tailNumber = tailNumber, year = "")

        verify { mockRepository.updateAircraft(expectedAircraft) }
    }

    @Test
    fun `should Communicate A Successful Save To The Edit Aircraft View`() {
        val editAircraftPresenter = EditAircraftPresenter(
                editAircraftView = mockEditAircraftView,
                repository = mockRepository,
                aircraftId = BundleKeys.UNDEFINED
        )
        editAircraftPresenter.presentData()

        editAircraftPresenter.saveAircraft(tailNumber)

        verify { mockEditAircraftView.saveSuccessful(tailNumber) }
    }

    @Test
    fun `should display the Tail Number Violation when attempting to save an aircraft with an empty TailNumber when adding an Aircraft`() {
        every { mockEditAircraftView.displayTailNumberRequiredViolation() } returns Unit
        val editAircraftPresenter = EditAircraftPresenter(
                editAircraftView = mockEditAircraftView,
                repository = mockRepository,
                aircraftId = BundleKeys.UNDEFINED
        )
        editAircraftPresenter.presentData()

        editAircraftPresenter.saveAircraft("   ")

        verify { mockEditAircraftView.displayTailNumberRequiredViolation() }
    }

    @Test
    fun `should display the Tail Number Violation when attempting to save an aircraft with an empty TailNumber when updating an Aircraft`() {

        every {
            mockRepository.getShallowAircraft(aircraftId)
        } returns Single.just(Aircraft(id = aircraftId, tailNumber = randomTailNumber()))

        every { mockEditAircraftView.displayTailNumberRequiredViolation() } returns Unit
        val editAircraftPresenter = EditAircraftPresenter(
                editAircraftView = mockEditAircraftView,
                repository = mockRepository,
                aircraftId = aircraftId
        )
        editAircraftPresenter.presentData()

        editAircraftPresenter.saveAircraft(tailNumber = "   ")

        verify { mockEditAircraftView.displayTailNumberRequiredViolation() }
    }

    @Test
    fun `should Update The Aircraft Info on the view with Empty String When adding an Aircraft`() {
        val editAircraftPresenter = EditAircraftPresenter(
                editAircraftView = mockEditAircraftView,
                repository = mockRepository,
                aircraftId = BundleKeys.UNDEFINED
        )

        editAircraftPresenter.presentData()

        val expectedAircraft = Aircraft(
                id = BundleKeys.UNDEFINED,
                tailNumber = ""
        )

        verify { mockEditAircraftView.updateAircraftInfo(expectedAircraft) }
    }

    @Test
    fun `should Update The Aircraft Info on the view when updating an Aircraft`() {
        val aircraft = Aircraft(id = aircraftId, tailNumber = tailNumber)

        every { mockRepository.getShallowAircraft(aircraftId) } returns Single.just(aircraft)
        every { mockRepository.updateAircraft(any()) } returns Completable.complete()

        val editAircraftPresenter = EditAircraftPresenter(
                editAircraftView = mockEditAircraftView,
                repository = mockRepository,
                aircraftId = aircraftId
        )

        editAircraftPresenter.presentData()

        verify { mockEditAircraftView.updateAircraftInfo(aircraft) }
    }

    @Test
    fun `should Dispose Of The CompositeDisposable`() {
        val compositeDisposable = CompositeDisposable()
        val editAircraftPresenter = EditAircraftPresenter(
                editAircraftView = mockEditAircraftView,
                repository = mockRepository,
                compositeDisposable = compositeDisposable,
                aircraftId = BundleKeys.UNDEFINED
        )

        editAircraftPresenter.dispose()
        assertThat(compositeDisposable.isDisposed, `is`(true))
    }

    @Test
    fun `should Add A Subscription When Presenting The Data`() {
        every { mockRepository.getShallowAircraft(aircraftId) } returns Single.just(Aircraft())

        val compositeDisposable = CompositeDisposable()
        val editAircraftPresenter = EditAircraftPresenter(
                editAircraftView = mockEditAircraftView,
                repository = mockRepository,
                compositeDisposable = compositeDisposable,
                aircraftId = aircraftId
        )

        editAircraftPresenter.presentData()

        assertThat(compositeDisposable.size(), `is`(1))
    }

    @Test
    fun `should Add Two Subscriptions When Saving An Aircraft`() {
        every { mockRepository.getShallowAircraft(aircraftId) } returns Single.just(Aircraft())
        every { mockRepository.updateAircraft(any()) } returns Completable.complete()

        val compositeDisposable = CompositeDisposable()
        val editAircraftPresenter = EditAircraftPresenter(
                editAircraftView = mockEditAircraftView,
                repository = mockRepository,
                compositeDisposable = compositeDisposable,
                aircraftId = aircraftId
        )

        editAircraftPresenter.presentData()

        editAircraftPresenter.saveAircraft(randomTailNumber())

        assertThat(compositeDisposable.size(), `is`(2))
    }
}