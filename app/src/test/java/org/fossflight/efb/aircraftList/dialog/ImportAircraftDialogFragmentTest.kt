package org.fossflight.efb.aircraftList.dialog

import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import androidx.test.core.app.launchActivity
import androidx.test.ext.junit.runners.AndroidJUnit4
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Flowable
import org.fossflight.efb.FossFlightTestApplication
import org.fossflight.efb.MainActivity
import org.fossflight.efb.R
import org.fossflight.efb.TestAppModule
import org.fossflight.efb.aircraftList.AircraftListFragment
import org.fossflight.efb.data.persistence.FFRepository
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import org.robolectric.shadows.ShadowAlertDialog

@Config(application = FossFlightTestApplication::class)
@RunWith(AndroidJUnit4::class)
class ImportAircraftDialogFragmentTest {

    private lateinit var scenario: ActivityScenario<MainActivity>

    @Before
    fun `create Dialog`() {
        val mockRepository = mockk<FFRepository>()
        every { mockRepository.allAircraft } returns Flowable.just(listOf())

        val application = ApplicationProvider.getApplicationContext<FossFlightTestApplication>()
        val testAppModule = TestAppModule(
                context = application.applicationContext,
                repository = mockRepository
        )
        application.setAppModule(testAppModule)

        scenario = launchActivity<MainActivity>()
        scenario.moveToState(Lifecycle.State.CREATED)
        scenario.moveToState(Lifecycle.State.RESUMED)
        scenario.onActivity {
            val aircraftListFragment = it.supportFragmentManager
                    .findFragmentByTag(AircraftListFragment::class.java.simpleName) as AircraftListFragment
            val mockMenuItem = mockk<MenuItem>()
            every { mockMenuItem.itemId } returns R.id.add_aircraft
            aircraftListFragment.onOptionsItemSelected(mockMenuItem)
        }
    }

    @Test
    fun `should Display the Add Aircraft Dialog`() {
        val latestDialog = ShadowAlertDialog.getLatestDialog()
        assertThat(latestDialog.isShowing, `is`(true))
    }

    @Test
    fun `should dismiss the Dialog when tapping The cancel Button`() {
        val latestDialog = ShadowAlertDialog.getLatestDialog()
        val cancelButton = (latestDialog as AlertDialog).getButton(AlertDialog.BUTTON_NEGATIVE)
        val actualCancelButtonText = cancelButton.text.toString()

        assertThat(actualCancelButtonText, `is`("Cancel"))

        cancelButton.performClick()
        assertThat(latestDialog.isShowing, `is`(false))
    }

    @Test
    fun `should include Import Aircraft as first item on the Menu`() {
        scenario.onActivity {
            val latestDialog = ShadowAlertDialog.getLatestDialog()
            val actualItem = (latestDialog as AlertDialog).listView.getItemAtPosition(0).toString()
            val expectedItem = it.getString(R.string.import_aircraft)
            assertThat(actualItem, `is`(expectedItem))
        }
    }

    @Test
    fun `should include Add Aircraft Manually as the second item on the menu`() {
        scenario.onActivity {
            val latestDialog = ShadowAlertDialog.getLatestDialog()
            val actualItem = (latestDialog as AlertDialog).listView.getItemAtPosition(1).toString()
            val expectedItem = it.getString(R.string.add_aircraft_manually)
            assertThat(actualItem, `is`(expectedItem))
        }
    }

    @Test
    fun `should include Import Example Checklist as third Item on the Menu`() {
        scenario.onActivity {
            val latestDialog = ShadowAlertDialog.getLatestDialog()
            val actualItem = (latestDialog as AlertDialog).listView.getItemAtPosition(2).toString()
            val expectedItem = it.getString(R.string.import_example_aircraft)
            assertThat(actualItem, `is`(expectedItem))
        }
    }
}