package org.fossflight.efb.aircraftList

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.android.material.textfield.TextInputLayout
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import org.fossflight.efb.BundleKeys
import org.fossflight.efb.BundleKeys.EMERGENCY_CHECKLIST_COLLECTION_NAME
import org.fossflight.efb.BundleKeys.STANDARD_CHECKLIST_COLLECTION_NAME
import org.fossflight.efb.ForceTrampolineSchedulers
import org.fossflight.efb.FossFlightFragmentListener
import org.fossflight.efb.FossFlightTestApplication
import org.fossflight.efb.R
import org.fossflight.efb.TestAppModule
import org.fossflight.efb.data.model.Aircraft
import org.fossflight.efb.data.model.ChecklistCollection
import org.fossflight.efb.data.persistence.FFRepository
import org.fossflight.efb.data.randomId
import org.fossflight.efb.data.randomManufacturer
import org.fossflight.efb.data.randomModel
import org.fossflight.efb.data.randomTailNumber
import org.fossflight.efb.data.randomYearInt
import org.fossflight.efb.data.testAircraft
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@Config(application = FossFlightTestApplication::class)
@RunWith(AndroidJUnit4::class)
class EditAircraftFragmentTest {

    @get:Rule
    var forceTrampolineSchedulers = ForceTrampolineSchedulers()

    private val mockRepository = mockk<FFRepository>()
    private val mockFossFlightFragmentListener = mockk<FossFlightFragmentListener>()

    val instantiate: () -> EditAircraftFragment = {
        val fragment = EditAircraftFragment()
        fragment.setParentActivity(mockFossFlightFragmentListener)
        fragment
    }

    @Before
    fun `set up mocks and activity`() {
        val application = ApplicationProvider.getApplicationContext<FossFlightTestApplication>().apply {
            setTheme(R.style.AppTheme)
        }
        val testAppModule = TestAppModule(
                context = application.applicationContext,
                repository = mockRepository
        )

        application.setAppModule(testAppModule)

        every { mockFossFlightFragmentListener.updateToolbarTitle(any()) } returns Unit
        every { mockFossFlightFragmentListener.updateToolbarSubtitle(any()) } returns Unit
        every { mockFossFlightFragmentListener.displayToast(any()) } returns Unit
        every { mockFossFlightFragmentListener.finishFragment() } returns Unit
    }

    @Test
    fun `should Set The Toolbar Text Appropriately when adding an Aircraft`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.AIRCRAFT_ID, BundleKeys.UNDEFINED)

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            val expectedTitle = it.requireActivity().getString(R.string.add_aircraft)
            verify { mockFossFlightFragmentListener.updateToolbarTitle(expectedTitle) }
            verify { mockFossFlightFragmentListener.updateToolbarSubtitle("") }
        }
    }

    @Test
    fun `should Set The Toolbar Text Appropriately when updating an Aircraft`() {
        val aircraftId = randomId()
        val tailNumber = randomTailNumber()
        val aircraft = Aircraft(id = aircraftId, tailNumber = tailNumber)

        every { mockRepository.getShallowAircraft(aircraftId) } returns Single.just(aircraft)

        val arguments = Bundle()
        arguments.putLong(BundleKeys.AIRCRAFT_ID, aircraftId)

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            val expectedTitle = it.requireActivity().getString(R.string.edit_aircraft)
            verify { mockFossFlightFragmentListener.updateToolbarTitle(expectedTitle) }
            verify { mockFossFlightFragmentListener.updateToolbarSubtitle("") }
        }
    }

    @Test
    fun `should display the Aircraft Information when adding an Aircraft`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.AIRCRAFT_ID, BundleKeys.UNDEFINED)

        launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        onView(withId(R.id.aircraft_tail_number_edit)).check(matches(withText("")))
        onView(withId(R.id.input_layout_aircraft_tail_number)).check(matches(hasHintText("Tail Number")))
        onView(withId(R.id.aircraft_manufacturer_edit)).check(matches(withText("")))
        onView(withId(R.id.input_layout_aircraft_manufacturer)).check(matches(hasHintText("Manufacturer")))
        onView(withId(R.id.aircraft_model_edit)).check(matches(withText("")))
        onView(withId(R.id.input_layout_aircraft_model)).check(matches(hasHintText("Model")))
        onView(withId(R.id.aircraft_year_edit)).check(matches(withText("")))
        onView(withId(R.id.input_layout_aircraft_year)).check(matches(hasHintText("Year")))
    }

    @Test
    fun `should display the Aircraft Information when updating the Aircraft`() {
        val aircraftId = randomId()
        val tailNumber = randomTailNumber()
        val manufacturer = randomManufacturer()
        val model = randomModel()
        val year = randomYearInt()
        val aircraft = Aircraft(
                id = aircraftId,
                tailNumber = tailNumber,
                manufacturer = manufacturer,
                model = model,
                year = year
        )

        every { mockRepository.getShallowAircraft(aircraftId) } returns Single.just(aircraft)

        val arguments = Bundle()
        arguments.putLong(BundleKeys.AIRCRAFT_ID, aircraftId)

        launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        onView(withId(R.id.aircraft_tail_number_edit)).check(matches(withText(tailNumber)))
        onView(withId(R.id.aircraft_manufacturer_edit)).check(matches(withText(manufacturer)))
        onView(withId(R.id.aircraft_model_edit)).check(matches(withText(model)))
        onView(withId(R.id.aircraft_year_edit)).check(matches(withText(year.toString())))
    }

    @Test
    fun `should Save the Aircraft, display a successful toast and finish the fragment when Save Icon is Clicked`() {
        val tailNumber = randomTailNumber()
        val manufacturer = randomManufacturer()
        val model = randomModel()
        val year = randomYearInt()

        every { mockRepository.updateAircraft(any()) } returns Completable.complete()
        every { mockRepository.saveAircraft(any()) } returns Observable.just(testAircraft())

        val arguments = Bundle()
        arguments.putLong(BundleKeys.AIRCRAFT_ID, BundleKeys.UNDEFINED)

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            it.requireActivity().findViewById<EditText>(R.id.aircraft_tail_number_edit).setText(tailNumber)
            it.requireActivity().findViewById<EditText>(R.id.aircraft_manufacturer_edit).setText(manufacturer)
            it.requireActivity().findViewById<EditText>(R.id.aircraft_model_edit).setText(model)
            it.requireActivity().findViewById<EditText>(R.id.aircraft_year_edit).setText(year.toString())

            it.onOptionsItemSelected(withMenuItem(R.id.save))
        }

        val aircraft = Aircraft(
                id = BundleKeys.UNDEFINED,
                tailNumber = tailNumber,
                manufacturer = manufacturer,
                model = model,
                year = year,
                checklistCollections = listOf(
                        ChecklistCollection(name = STANDARD_CHECKLIST_COLLECTION_NAME),
                        ChecklistCollection(name = EMERGENCY_CHECKLIST_COLLECTION_NAME, orderNumber = 1)
                )
        )

        verify { mockRepository.saveAircraft(aircraft) }
        verify { mockFossFlightFragmentListener.displayToast("Saved $tailNumber") }
        verify { mockFossFlightFragmentListener.finishFragment() }
    }

    @Test
    fun `should display A Tail Number Required Violation when attempting to save with an empty Tail Number when adding an Aircraft`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.AIRCRAFT_ID, BundleKeys.UNDEFINED)

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            it.requireActivity().findViewById<EditText>(R.id.aircraft_tail_number_edit).setText("")

            it.onOptionsItemSelected(withMenuItem(R.id.save))
        }

        val expectedErrorMessage = "The Aircraft's Tail Number is Required"
        verify { mockFossFlightFragmentListener.displayToast(expectedErrorMessage) }
        onView(withId(R.id.input_layout_aircraft_tail_number)).check(matches(hasErrorText(expectedErrorMessage)))
    }

    @Test
    fun `should finish the fragment after a Cancel click`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.AIRCRAFT_ID, BundleKeys.UNDEFINED)

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment { it.onOptionsItemSelected(withMenuItem(android.R.id.home)) }

        verify (exactly = 0) { mockRepository.saveChecklistItem(any()) }
        verify (exactly = 0) { mockFossFlightFragmentListener.displayToast(any()) }
        verify (exactly = 1) { mockFossFlightFragmentListener.finishFragment() }
    }

    private fun withMenuItem(menuItemId: Int): MenuItem {
        val mockMenuItem = mockk<MenuItem>()
        every { mockMenuItem.itemId } returns menuItemId
        return mockMenuItem
    }
}

fun hasHintText(expectedHintText: String): Matcher<View> = object : TypeSafeMatcher<View>() {
    override fun describeTo(description: Description?) { }
    override fun matchesSafely(item: View?): Boolean {
        if (item !is TextInputLayout) return false
        val hint = item.hint ?: return false
        val hintMessage = hint.toString()
        return expectedHintText == hintMessage
    }
}

fun hasErrorText(expectedErrorText: String): Matcher<View> = object : TypeSafeMatcher<View>() {
    override fun describeTo(description: Description?) { }
    override fun matchesSafely(item: View?): Boolean {
        if (item !is TextInputLayout) return false
        val error = item.error ?: return false
        val errorMessage = error.toString()
        return expectedErrorText == errorMessage
    }
}