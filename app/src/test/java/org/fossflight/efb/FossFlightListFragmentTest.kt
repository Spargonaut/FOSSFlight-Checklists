package org.fossflight.efb

import android.content.pm.ActivityInfo
import android.view.View.VISIBLE
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class FossFlightListFragmentTest {

    @Test
    fun `should Set the Screen Orientation to Unspecified`() {
        val scenario = launchFragmentInContainer(
                instantiate =  { FossFlightListFragmentTestImpl() }
        )

        scenario.onFragment {
            val requestedOrientation = it.requireActivity().requestedOrientation
            assertThat(requestedOrientation, `is`(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED))
        }
    }

    @Test
    fun `should Inflate the ListFragmentLayout`() {
        val scenario = launchFragmentInContainer(
                instantiate =  { FossFlightListFragmentTestImpl() }
        )

        scenario.onFragment {
            val actualView = it.view
            assertThat(actualView!!.visibility, `is`(VISIBLE))
            assertThat(actualView.id, `is`(R.id.list_fragment))
        }
    }

    @Test
    fun `should Hide the Empty ListMessage`() {
        val scenario = launchFragmentInContainer(
                instantiate =  { FossFlightListFragmentTestImpl() }
        )

        scenario.onFragment {
            it.updateUIForNonEmptyItemList()
        }

        onView(withId(R.id.empty)).check(matches(not(isDisplayed())))

    }

    @Test
    fun `should Display the Empty ListMessage`() {
        val scenario = launchFragmentInContainer(
                instantiate =  { FossFlightListFragmentTestImpl() }
        )

        scenario.onFragment {
            it.updateUIForEmptyItemList()
        }

        onView(withId(R.id.empty)).check(ViewAssertions.matches(isDisplayed()))
    }

    @Test
    fun `should Apply the Application Name to the Toolbar by Default`() {
        val scenario = launchFragmentInContainer(
                instantiate =  { FossFlightListFragmentTestImpl() }
        )

        scenario.onFragment {
            val expectedTitle = it.requireActivity().getString(R.string.app_name)
            val title = it.requireActivity().title.toString()
            assertThat(title, `is`(expectedTitle))
        }
    }
}