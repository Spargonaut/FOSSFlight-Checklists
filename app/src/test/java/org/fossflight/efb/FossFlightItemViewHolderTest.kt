package org.fossflight.efb

import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.test.core.app.ActivityScenario
import androidx.test.ext.junit.runners.AndroidJUnit4
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Flowable
import org.fossflight.efb.checklist.ChecklistItemListener
import org.fossflight.efb.data.persistence.FFRepository
import org.fossflight.efb.data.testChecklistItem
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.startsWith
import org.junit.Assert.assertNull
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Shadows.shadowOf
import org.robolectric.shadows.ShadowPopupMenu
import javax.inject.Inject


@RunWith(AndroidJUnit4::class)
class FossFlightItemViewHolderTest {

    private val mockChecklistItemListener = mockk<ChecklistItemListener>()

    @Inject
    private val mockRepository = mockk<FFRepository>()

    @Before
    fun `set up mocks and root view`() {
        every { mockRepository.allAircraft } returns Flowable.just(listOf())
        every { mockChecklistItemListener.deleteChecklistItem(any()) } returns Unit
        every { mockChecklistItemListener.editChecklistItem(any()) } returns Unit
    }

    @Test
    fun `should get the Text for the ContentView as the String`() {
        val checklistItem = testChecklistItem()

        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { activity ->
                val view = createInflatedView(activity)
                val fossFlightItemViewHolder = FossFlightItemViewHolderImpl(view, mockChecklistItemListener)
                fossFlightItemViewHolder.bind(checklistItem)

                assertThat(fossFlightItemViewHolder.toString(), startsWith("FossFlightItemViewHolderImpl"))
            }
        }
    }

    @Test
    fun `should display the Delete Option on the Popup Menu for the Item`() {
        every { mockChecklistItemListener.isEditable() } returns true

        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { activity ->
                val view = createInflatedView(activity)

                FossFlightItemViewHolderImpl(view, mockChecklistItemListener)

                view.performLongClick()

                val latestPopupMenu = ShadowPopupMenu.getLatestPopupMenu()

                val menu = latestPopupMenu.menu
                val deleteItem = menu.getItem(1)

                assertThat(deleteItem.title.toString(), `is`("Delete"))
            }
        }
    }

    @Test
    fun `should display the Edit the Option on the Popup Menu for the Item`() {
        every { mockChecklistItemListener.isEditable() } returns true

        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { activity ->
                val view = createInflatedView(activity)

                FossFlightItemViewHolderImpl(view, mockChecklistItemListener)

                view.performLongClick()

                val latestPopupMenu = ShadowPopupMenu.getLatestPopupMenu()

                val menu = latestPopupMenu.menu
                val deleteItem = menu.getItem(0)

                assertThat(deleteItem.title.toString(), `is`("Edit"))
            }
        }
    }

    @Test
    fun `should Delete an Item from the Popup Context Menu`() {
        val checklistItem = testChecklistItem()

        every { mockChecklistItemListener.isEditable() } returns true

        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { activity ->
                val view = createInflatedView(activity)

                val fossFlightItemViewHolder = FossFlightItemViewHolderImpl(view, mockChecklistItemListener)
                fossFlightItemViewHolder.bind(checklistItem)

                view.performLongClick()

                val latestPopupMenu = ShadowPopupMenu.getLatestPopupMenu()
                val shadowLatestPopupMenu = shadowOf(latestPopupMenu)

                val onMenuItemClickListener = shadowLatestPopupMenu.onMenuItemClickListener

                val menuItemId = R.id.delete_item
                val mockMenuItem = mockk<MenuItem>()
                every { mockMenuItem.itemId } returns menuItemId

                onMenuItemClickListener.onMenuItemClick(mockMenuItem)

                verify { mockChecklistItemListener.deleteChecklistItem(checklistItem) }
            }
        }
    }

    @Test
    fun `should Edit an Item from the Popup Menu`() {
        val checklistItem = testChecklistItem()

        every { mockChecklistItemListener.isEditable() } returns true

        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { activity ->
                val view = createInflatedView(activity)

                val fossFlightItemViewHolder = FossFlightItemViewHolderImpl(view, mockChecklistItemListener)
                fossFlightItemViewHolder.bind(checklistItem)

                view.performLongClick()

                val latestPopupMenu = ShadowPopupMenu.getLatestPopupMenu()
                val shadowLatestPopupMenu = shadowOf(latestPopupMenu)

                val onMenuItemClickListener = shadowLatestPopupMenu.onMenuItemClickListener

                val menuItemId = R.id.edit_item
                val mockMenuItem = mockk<MenuItem>()
                every { mockMenuItem.itemId } returns menuItemId

                onMenuItemClickListener.onMenuItemClick(mockMenuItem)

                verify { mockChecklistItemListener.editChecklistItem(checklistItem.id) }
            }
        }
    }

    @Test
    fun `should Ignore a Long Click when a Checklist is Not Editable`() {
        val checklistItem = testChecklistItem()

        every { mockChecklistItemListener.isEditable() } returns false

        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { activity ->
                val view = createInflatedView(activity)
                val fossFlightItemViewHolder = FossFlightItemViewHolderImpl(view, mockChecklistItemListener)
                fossFlightItemViewHolder.bind(checklistItem)

                view.performLongClick()

                val latestPopupMenu = ShadowPopupMenu.getLatestPopupMenu()

                assertNull(latestPopupMenu)
            }
        }
    }

    fun createInflatedView(activity: MainActivity): View {
        val root = CardView(activity)
        val layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        )
        layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
        root.layoutParams = layoutParams
        return LayoutInflater.from(activity).inflate(R.layout.listable_card, root)
    }
}