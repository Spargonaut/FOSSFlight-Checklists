package org.fossflight.efb.data.model

import org.fossflight.efb.data.randomId
import org.fossflight.efb.data.randomName
import org.fossflight.efb.data.randomOrderNumber
import org.fossflight.efb.data.testChecklistItem
import org.hamcrest.core.Is.`is`
import org.junit.Assert.assertThat
import org.junit.Test

class ChecklistTest {

    private val checklistId = randomId()
    private val checklistName = randomName()
    private val checklistCollectionId = randomId()
    private val orderNumber = randomOrderNumber()


    private val baseChecklist = Checklist(
            id = checklistId,
            checklistCollectionId = checklistCollectionId,
            name = checklistName,
            orderNumber = orderNumber,
            items = listOf()
    )

    @Test
    fun `checklists with same values should Be Equal`() {
        val checklistTwo = Checklist(
                id = checklistId,
                checklistCollectionId = checklistCollectionId,
                name = checklistName,
                orderNumber = orderNumber,
                items = listOf()
        )

        assertThat(baseChecklist == checklistTwo, `is`(true))
        assertThat(checklistTwo == baseChecklist, `is`(true))
    }

    @Test
    fun `checklists with Different Ids should be Different`() {
        val checklistTwo = baseChecklist.copy(id = randomId())

        assertThat(baseChecklist == checklistTwo, `is`(false))
        assertThat(checklistTwo == baseChecklist, `is`(false))
    }

    @Test
    fun `checklists with Different Names should be Different`() {
        val checklistTwo = baseChecklist.copy(name = randomName())

        assertThat(baseChecklist == checklistTwo, `is`(false))
        assertThat(checklistTwo == baseChecklist, `is`(false))
    }

    @Test
    fun `checklists with Different Checklist Collection Ids should be Different`() {
        val checklistTwo = baseChecklist.copy(checklistCollectionId = randomId())

        assertThat(baseChecklist == checklistTwo, `is`(false))
        assertThat(checklistTwo == baseChecklist, `is`(false))
    }

    @Test
    fun `checklists with Different Order Numbers should be Different`() {
        val checklistTwo = baseChecklist.copy(orderNumber = randomOrderNumber())

        assertThat(baseChecklist == checklistTwo, `is`(false))
        assertThat(checklistTwo == baseChecklist, `is`(false))
    }

    @Test
    fun `checklists with Different Item Count should be Different`() {
        val itemOne = testChecklistItem()
        val itemTwo = testChecklistItem()
        val itemThree = testChecklistItem()

        val checklistOne = baseChecklist.copy(items = listOf(itemOne, itemTwo))
        val checklistTwo = baseChecklist.copy(items = listOf(itemThree))

        assertThat(checklistOne == checklistTwo, `is`(false))
        assertThat(checklistTwo == checklistOne, `is`(false))
    }

    @Test
    fun `a Checklist with Items should be Different from a Checklist with an Empty Item List`() {
        val checklistTwo = baseChecklist.copy(
                items = listOf(
                        testChecklistItem(), testChecklistItem(), testChecklistItem(),
                        testChecklistItem(), testChecklistItem(), testChecklistItem(),
                        testChecklistItem(), testChecklistItem(), testChecklistItem()
                )
        )

        assertThat(baseChecklist == checklistTwo, `is`(false))
        assertThat(checklistTwo == baseChecklist, `is`(false))
    }

    @Test
    fun `checklists with Different Items should be Different`() {
        val itemOne = testChecklistItem()
        val itemTwo = testChecklistItem()
        val itemThree = testChecklistItem()
        val itemFour = testChecklistItem()

        val checklistOne = baseChecklist.copy(items = listOf(itemOne, itemTwo))
        val checklistTwo = baseChecklist.copy(items = listOf(itemThree, itemFour))

        assertThat(checklistOne == checklistTwo, `is`(false))
        assertThat(checklistTwo == checklistOne, `is`(false))
    }

}