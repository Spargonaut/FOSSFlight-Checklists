package org.fossflight.efb.data.io

import org.hamcrest.core.Is.`is`
import org.junit.Assert.assertThat
import org.junit.Test

class FltPlanGoChecklistParserTest {

    private val parser = FltPlanGoChecklistParser()
    private var fileContents = listOf(
                "\"Standard Checklist\"",
                "\"PREFLIGHT\"",
                "\"Maintenance Status\",\"Check\"",
                "\"Weather\",\"Check\"",
                "\"AROW Documents\",\"Check\"",
                "\"BEFORE STARTING ENGINE\"",
                "\"Seatbelts\",\"On\"",
                "\"Doors\",\"Closed and Latched\"",
                "\"Fuel Selector\",\"Both\"",
                "\"Mixture\",\"Full Rich\"",
                "\"Am I missing anything?\",\"well, am I?\"",
                "\"Emergency Checklist\"",
                "\"ICING\"",
                "\"Pitot Heat\",\"On\"",
                "\"Carb Heat\",\"On\""
        )

    @Test
    fun `should Identify A Checklist Collection Name`() {
        val collections = parser.parse(fileContents).checklistCollections
        assertThat(collections[0].name, `is`("Standard Checklist"))
        assertThat(collections[1].name, `is`("Emergency Checklist"))
    }

    @Test
    fun `should Identify A Checklist Title When Line Has One Token`() {
        val collections = parser.parse(fileContents).checklistCollections

        val checklists = collections[0].checklists

        assertThat(checklists[0].name, `is`("PREFLIGHT"))
        assertThat(checklists[1].name, `is`("BEFORE STARTING ENGINE"))
    }

    @Test
    fun `should Preserve The Order Of Checklists As Read`() {
        val collections = parser.parse(fileContents).checklistCollections
        val checklists = collections[0].checklists

        val (_, _, name, orderNumber) = checklists[0]
        assertThat(name, `is`("PREFLIGHT"))
        assertThat(orderNumber, `is`(0L))

        val (_, _, name1, orderNumber1) = checklists[1]
        assertThat(name1, `is`("BEFORE STARTING ENGINE"))
        assertThat(orderNumber1, `is`(1L))
    }

    @Test
    fun `should Identify Checklist Items When Line Has Two Tokens`() {
        val collections = parser.parse(fileContents).checklistCollections

        val checklists = collections[0].checklists

        val (_, _, _, _, firstChecklistItems) = checklists[0]

        assertThat(firstChecklistItems[0].name, `is`("Maintenance Status"))
        assertThat(firstChecklistItems[0].action, `is`("Check"))
        assertThat(firstChecklistItems[1].name, `is`("Weather"))
        assertThat(firstChecklistItems[1].action, `is`("Check"))
        assertThat(firstChecklistItems[2].name, `is`("AROW Documents"))
        assertThat(firstChecklistItems[2].action, `is`("Check"))

        val (_, _, _, _, secondChecklistItems) = checklists[1]
        assertThat(secondChecklistItems[0].name, `is`("Seatbelts"))
        assertThat(secondChecklistItems[0].action, `is`("On"))
        assertThat(secondChecklistItems[1].name, `is`("Doors"))
        assertThat(secondChecklistItems[1].action, `is`("Closed and Latched"))
        assertThat(secondChecklistItems[2].name, `is`("Fuel Selector"))
        assertThat(secondChecklistItems[2].action, `is`("Both"))
        assertThat(secondChecklistItems[3].name, `is`("Mixture"))
        assertThat(secondChecklistItems[3].action, `is`("Full Rich"))
        assertThat(secondChecklistItems[4].name, `is`("Am I missing anything?"))
        assertThat(secondChecklistItems[4].action, `is`("well, am I?"))
    }
}
