package org.fossflight.efb.data.model

import org.hamcrest.core.Is.`is`
import org.junit.Assert.assertThat
import org.junit.Test

class ChecklistItemTypeTest {

    @Test
    fun `should Parse a String to its Corresponding Enum Type`() {
        assertThat(ChecklistItemType.parseCommonName("Normal"), `is`(ChecklistItemType.NORMAL))
        assertThat(ChecklistItemType.parseCommonName("Section Header"), `is`(ChecklistItemType.SECTION_HEADER))
    }

    @Test
    fun `should Parse Unknown Strings to Normal by Default`() {
        assertThat(ChecklistItemType.parseCommonName("asdfasdf"), `is`(ChecklistItemType.NORMAL))
        assertThat(ChecklistItemType.parseCommonName(""), `is`(ChecklistItemType.NORMAL))
        assertThat(ChecklistItemType.parseCommonName(null), `is`(ChecklistItemType.NORMAL))
    }

    @Test
    fun `should Get the Common Name of the ChecklistItem type`() {
        assertThat(ChecklistItemType.SECTION_HEADER.toString(), `is`("SECTION HEADER"))
        assertThat(ChecklistItemType.NORMAL.toString(), `is`("NORMAL"))
    }

}