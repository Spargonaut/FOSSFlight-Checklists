package org.fossflight.efb.data.io

import io.mockk.every
import io.mockk.mockk
import org.fossflight.efb.data.model.Aircraft
import org.fossflight.efb.data.testChecklistCollection
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import java.io.InputStream

class FileImporterTest {

    private val fossFlightFileReader = mockk<FossFlightFileReader>()
    private val checklistParser = mockk<FltPlanGoChecklistParser>()
    private val inputStream = mockk<InputStream>()

    private val fileContents = listOf("The contents")
    private val checklistCollections = listOf(testChecklistCollection())

    private val fileImporter = FileImporter(fossFlightFileReader, checklistParser)

    @Before
    fun `setup mocks`() {
        every { fossFlightFileReader.readFile(inputStream) } returns fileContents
    }

    @Test
    @Throws(Exception::class)
    fun `should Parse File Extension For Aircraft Tail Number When Importing Filename`() {
        val tailNumber = "tailNumber"
        val fileName = "$tailNumber.csv"
        val path = "content:some/path/$fileName"

        every {
            checklistParser.parse(fileContents)
        } returns Aircraft(checklistCollections = checklistCollections)

        val aircraft = fileImporter.importAircraftFromStream(path, inputStream)

        assertThat(aircraft.tailNumber, `is`(tailNumber))
        assertThat(aircraft.checklistCollections, `is`(checklistCollections))
    }

    @Test
    @Throws(Exception::class)
    fun `should Read Aircraft Tail Number From File Name when the checklist parser produces an Aircraft with an empty tail number`() {
        val tailNumber = "191"
        val path = "content:some/path/$tailNumber"

        every {
            checklistParser.parse(fileContents)
        } returns Aircraft(checklistCollections = checklistCollections)

        val aircraft = fileImporter.importAircraftFromStream(path, inputStream)

        assertThat(aircraft.tailNumber, `is`(tailNumber))
        assertThat(aircraft.checklistCollections, `is`(checklistCollections))
    }

    @Test
    @Throws(Exception::class)
    fun `should use Aircraft Tail Number From parser when the checklist parser produces an Aircraft with a non-empty tail number`() {
        val tailNumber = "191"
        val path = "content:some/path/245.csv"

        every {
            checklistParser.parse(fileContents)
        } returns Aircraft(tailNumber = tailNumber, checklistCollections = checklistCollections)

        val aircraft = fileImporter.importAircraftFromStream(path, inputStream)

        assertThat(aircraft.tailNumber, `is`(tailNumber))
        assertThat(aircraft.checklistCollections, `is`(checklistCollections))
    }
}