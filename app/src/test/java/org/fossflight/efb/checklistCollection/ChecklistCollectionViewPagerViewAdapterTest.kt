package org.fossflight.efb.checklistCollection

import androidx.test.ext.junit.runners.AndroidJUnit4
import io.mockk.mockk
import org.fossflight.efb.data.testChecklistCollection
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertThat
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ChecklistCollectionViewPagerViewAdapterTest {

    private val collections = listOf(
            testChecklistCollection(name = "first list"),
            testChecklistCollection(name = "second list")
    )

    @Test
    fun `should Create a ChecklistCollectionPagerAdapter`() {
        val adapter = ChecklistCollectionViewPagerViewAdapter(mockk())
        assertNotNull(adapter)
    }

    @Test
    fun `should Get the Size of the Checklist Types for the Count`() {
        val adapter = ChecklistCollectionViewPagerViewAdapter(mockk())
        adapter.setCollections(collections)
        assertThat(adapter.count, `is`(2))
    }

    @Test
    fun `should Get the Item of the List as the Page Title`() {
        val adapter = ChecklistCollectionViewPagerViewAdapter(mockk())
        adapter.setCollections(collections)

        assertThat<CharSequence>(adapter.getPageTitle(0), `is`("first list"))
        assertThat<CharSequence>(adapter.getPageTitle(1), `is`("second list"))
    }

    @Test
    fun `should Create a Stub Fragment with the Position`() {
        val adapter = ChecklistCollectionViewPagerViewAdapter(mockk())
        adapter.setCollections(collections)

        val fragment = adapter.getItem(0)
        assertThat(fragment.javaClass.simpleName, `is`(ChecklistCollectionPageFragment::class.java.simpleName))
    }
}