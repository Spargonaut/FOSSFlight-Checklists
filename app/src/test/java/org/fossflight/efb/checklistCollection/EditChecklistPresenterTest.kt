package org.fossflight.efb.checklistCollection

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import org.fossflight.efb.BundleKeys
import org.fossflight.efb.ForceTrampolineSchedulers
import org.fossflight.efb.data.model.Checklist
import org.fossflight.efb.data.model.ChecklistCollection
import org.fossflight.efb.data.persistence.FFRepository
import org.fossflight.efb.data.randomId
import org.fossflight.efb.data.randomName
import org.fossflight.efb.data.randomTailNumber
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.assertThat
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class EditChecklistPresenterTest {

    @get:Rule
    var forceTrampolineSchedulers = ForceTrampolineSchedulers()

    private val mockEditChecklistView = mockk<EditChecklistView>()
    private val mockRepository = mockk<FFRepository>()

    private val tailNumber = randomTailNumber()

    private val aircraftIdForAircraftWithMultipleCollections = randomId()
    private val aircraftIdForAircraftWithZeroCollections = randomId()

    private val newCollectionId = randomId()

    private val collectionOneId = randomId()
    private val collectionOneName = randomName()
    private val checklistCollectionOne = ChecklistCollection(
            id = collectionOneId,
            aircraftId = aircraftIdForAircraftWithMultipleCollections,
            name = collectionOneName
    )

    private val collectionTwoId = randomId()
    private val collectionTwoName = randomName()
    private val checklistCollectionTwo = ChecklistCollection(
            id = collectionTwoId,
            aircraftId = aircraftIdForAircraftWithMultipleCollections,
            name = collectionTwoName
    )
    private val collectionList = mutableListOf(checklistCollectionOne, checklistCollectionTwo)

    private val checklistOneId = randomId()
    private val checklistOneName = randomName()
    private val checklistOne = Checklist(
            id = checklistOneId,
            checklistCollectionId = collectionOneId,
            name = checklistOneName,
            orderNumber = 0
    )
    private val checklistTwoId = randomId()
    private val checklistTwoName = randomName()
    private val checklistTwo = Checklist(
            id = checklistTwoId,
            checklistCollectionId = collectionTwoId,
            name = checklistTwoName,
            orderNumber = 0
    )

    @Before
    fun `set up mocks`() {
        every { mockEditChecklistView.updateChecklistName(any()) } returns Unit
        every { mockEditChecklistView.updateToolbarInfoForAdd(any()) } returns Unit
        every { mockEditChecklistView.updateToolbarInfoForUpdate(any()) } returns Unit
        every { mockEditChecklistView.updateCollectionNames(any()) } returns Unit
        every { mockEditChecklistView.updateSelectedCollectionIndex(any()) } returns Unit
        every { mockEditChecklistView.abortEdit() } returns Unit
        every { mockEditChecklistView.displayNameRequiredViolation() } returns Unit
        every { mockEditChecklistView.saveSuccessful(any()) } returns Unit

        every { mockRepository.updateChecklist(any()) } returns Completable.complete()

        setupMocksForAircraftWithMultipleCollections()
        setupMocksForAircraftWithZeroCollections()
    }

    @Test
    fun `should update the toolbar info when adding a checklist (for a new checklist)`() {

        val editChecklistPresenter = EditChecklistPresenter(
                editChecklistView = mockEditChecklistView,
                repository = mockRepository,
                aircraftId = aircraftIdForAircraftWithMultipleCollections
        )

        editChecklistPresenter.presentData()
        verify { mockEditChecklistView.updateToolbarInfoForAdd(tailNumber) }
    }

    @Test
    fun `should update the toolbar info when editing a checklist (for an existing checklist)`() {

        val editChecklistPresenter = EditChecklistPresenter(
                editChecklistView = mockEditChecklistView,
                repository = mockRepository,
                checklistId = checklistOneId
        )

        editChecklistPresenter.presentData()

        verify { mockEditChecklistView.updateToolbarInfoForUpdate(tailNumber) }
    }

    @Test
    fun `should display the checklist info on the EditChecklistView when adding a checklist to an aircraft with zero checklist collections`() {

        val aircraftIdForAircraftWithZeroCollections = randomId()

        every {
            mockRepository.getChecklistCollections(aircraftIdForAircraftWithZeroCollections)
        } returns Single.just(mutableListOf())

        every {
            mockRepository.getAircraftTailNumber(aircraftIdForAircraftWithZeroCollections)
        } returns Single.just(tailNumber)

        val editChecklistPresenter = EditChecklistPresenter(
                editChecklistView = mockEditChecklistView,
                repository = mockRepository,
                aircraftId = aircraftIdForAircraftWithZeroCollections
        )

        editChecklistPresenter.presentData()

        verify { mockEditChecklistView.updateChecklistName("") }
        verify {
            mockEditChecklistView.updateCollectionNames(listOf(BundleKeys.STANDARD_CHECKLIST_COLLECTION_NAME))
        }
        verify { mockEditChecklistView.updateSelectedCollectionIndex(0) }
    }

    @Test
    fun `should display the checklist info on the EditChecklistView when adding a checklist to an aircraft with multiple checklist collections`() {

        val editChecklistPresenter = EditChecklistPresenter(
                editChecklistView = mockEditChecklistView,
                repository = mockRepository,
                aircraftId = aircraftIdForAircraftWithMultipleCollections
        )

        editChecklistPresenter.presentData()

        verify { mockEditChecklistView.updateChecklistName("") }
        verify { mockEditChecklistView.updateCollectionNames(listOf(collectionOneName, collectionTwoName)) }
        verify { mockEditChecklistView.updateSelectedCollectionIndex(0) }
    }

    @Test
    fun `should display the checklist info on the EditChecklistView when editing an existing checklist`() {

        val editChecklistPresenter = EditChecklistPresenter(
                editChecklistView = mockEditChecklistView,
                repository = mockRepository,
                checklistId = checklistTwoId
        )

        editChecklistPresenter.presentData()

        verify { mockEditChecklistView.updateChecklistName(checklistTwoName) }
        verify { mockEditChecklistView.updateCollectionNames(listOf(collectionOneName, collectionTwoName)) }
        verify { mockEditChecklistView.updateSelectedCollectionIndex(1) }
    }

    @Test
    fun `should abort an edit when attempting to save a checklist with an empty name`() {

        val editChecklistPresenter = EditChecklistPresenter(
                editChecklistView = mockEditChecklistView,
                repository = mockRepository,
                checklistId = checklistOneId
        )

        editChecklistPresenter.presentData()

        editChecklistPresenter.saveChecklist(name = "", collectionName = randomName())

        verify { mockEditChecklistView.displayNameRequiredViolation() }
    }

    @Test
    fun `should save a default collection when adding a checklist to an aircraft with zero checklist collections`() {

        val editChecklistPresenter = EditChecklistPresenter(
                editChecklistView = mockEditChecklistView,
                repository = mockRepository,
                aircraftId = aircraftIdForAircraftWithZeroCollections
        )
        editChecklistPresenter.presentData()

        editChecklistPresenter.saveChecklist(
                name = randomName(),
                collectionName = BundleKeys.STANDARD_CHECKLIST_COLLECTION_NAME
        )

        val expectedCollection = ChecklistCollection(
                id = BundleKeys.UNDEFINED,
                aircraftId = aircraftIdForAircraftWithZeroCollections,
                name = BundleKeys.STANDARD_CHECKLIST_COLLECTION_NAME,
                orderNumber = 0,
                checklists = listOf()
        )

        verify { mockRepository.saveChecklistCollection(expectedCollection) }
    }

    @Test
    fun `should save a checklist to the default collection when adding a checklist to an aircraft with zero checklist collections`() {

        val editChecklistPresenter = EditChecklistPresenter(
                editChecklistView = mockEditChecklistView,
                repository = mockRepository,
                aircraftId = aircraftIdForAircraftWithZeroCollections
        )
        editChecklistPresenter.presentData()

        val newChecklistName = randomName()
        editChecklistPresenter.saveChecklist(
                name = newChecklistName,
                collectionName = BundleKeys.STANDARD_CHECKLIST_COLLECTION_NAME
        )

        val expectedChecklist = Checklist(
                id = BundleKeys.UNDEFINED,
                checklistCollectionId = newCollectionId,
                name = newChecklistName,
                orderNumber = 0,
                items = listOf()
        )

        verify { mockRepository.saveChecklist(expectedChecklist) }
    }

    @Test
    fun `should save a checklist to an existing collection when adding a checklist to an aircraft with existing checklist collections`() {
        val editChecklistPresenter = EditChecklistPresenter(
                editChecklistView = mockEditChecklistView,
                repository = mockRepository,
                aircraftId = aircraftIdForAircraftWithMultipleCollections
        )
        editChecklistPresenter.presentData()

        val newChecklistName = randomName()
        editChecklistPresenter.saveChecklist(
                name = newChecklistName,
                collectionName = collectionTwoName
        )

        val expectedChecklist = Checklist(
                id = BundleKeys.UNDEFINED,
                checklistCollectionId = collectionTwoId,
                name = newChecklistName,
                orderNumber = 2,
                items = listOf()
        )

        verify { mockRepository.saveChecklist(expectedChecklist) }
    }

    @Test
    fun `should save an existing checklist to a different collection when updating a checklist`() {
        val editChecklistPresenter = EditChecklistPresenter(
                editChecklistView = mockEditChecklistView,
                repository = mockRepository,
                checklistId = checklistOneId
        )
        editChecklistPresenter.presentData()

        editChecklistPresenter.saveChecklist(
                name = checklistOneName,
                collectionName = collectionTwoName
        )

        val expectedChecklist = checklistOne.copy(
                checklistCollectionId = collectionTwoId,
                orderNumber = 2
        )

        verify { mockRepository.updateChecklist(expectedChecklist) }
    }

    @Test
    fun `should save an existing checklist with a new name when updating a checklist`() {
        val editChecklistPresenter = EditChecklistPresenter(
                editChecklistView = mockEditChecklistView,
                repository = mockRepository,
                checklistId = checklistOneId
        )
        editChecklistPresenter.presentData()

        val newChecklistName = randomName()
        editChecklistPresenter.saveChecklist(
                name = newChecklistName,
                collectionName = collectionOneName
        )

        val expectedChecklist = checklistOne.copy(name = newChecklistName)

        verify { mockRepository.updateChecklist(expectedChecklist) }
    }

    @Test
    fun `should display the save successful message with the checklist name when adding a checklist to an aircraft with zero checklist collections`() {
        val editChecklistPresenter = EditChecklistPresenter(
                editChecklistView = mockEditChecklistView,
                repository = mockRepository,
                aircraftId = aircraftIdForAircraftWithZeroCollections
        )
        editChecklistPresenter.presentData()

        val newChecklistName = randomName()
        editChecklistPresenter.saveChecklist(
                name = newChecklistName,
                collectionName = BundleKeys.STANDARD_CHECKLIST_COLLECTION_NAME
        )

        verify { mockEditChecklistView.saveSuccessful(newChecklistName) }
    }

    @Test
    fun `should display the save successful message with the checklist name when adding a checklist to an aircraft with existing checklist collections`() {
        val editChecklistPresenter = EditChecklistPresenter(
                editChecklistView = mockEditChecklistView,
                repository = mockRepository,
                aircraftId = aircraftIdForAircraftWithMultipleCollections
        )
        editChecklistPresenter.presentData()

        val newChecklistName = randomName()
        editChecklistPresenter.saveChecklist(
                name = newChecklistName,
                collectionName = collectionTwoName
        )

        verify { mockEditChecklistView.saveSuccessful(newChecklistName) }
    }

    @Test
    fun `should display the save successful message with the checklist name when editing a checklist`() {
        val editChecklistPresenter = EditChecklistPresenter(
                editChecklistView = mockEditChecklistView,
                repository = mockRepository,
                checklistId = checklistOneId
        )
        editChecklistPresenter.presentData()

        val newChecklistName = randomName()
        editChecklistPresenter.saveChecklist(
                name = newChecklistName,
                collectionName = collectionTwoName
        )

        verify { mockEditChecklistView.saveSuccessful(newChecklistName) }
    }

    @Test
    fun `should collect 5 disposable subscriptions when adding a checklist to an aircraft with zero checklist collections`() {
        val compositeDisposable = CompositeDisposable()

        val editChecklistPresenter = EditChecklistPresenter(
                editChecklistView = mockEditChecklistView,
                repository = mockRepository,
                compositeDisposable = compositeDisposable,
                aircraftId = aircraftIdForAircraftWithZeroCollections
        )
        editChecklistPresenter.presentData()

        editChecklistPresenter.saveChecklist(
                name = randomName(),
                collectionName = BundleKeys.STANDARD_CHECKLIST_COLLECTION_NAME
        )

        assertThat(compositeDisposable.size(), `is`(5))
    }

    @Test
    fun `should collect 4 disposable subscriptions when adding a checklist to an aircraft with existing checklist collections`() {
        val compositeDisposable = CompositeDisposable()
        val editChecklistPresenter = EditChecklistPresenter(
                editChecklistView = mockEditChecklistView,
                repository = mockRepository,
                compositeDisposable = compositeDisposable,
                aircraftId = aircraftIdForAircraftWithMultipleCollections
        )
        editChecklistPresenter.presentData()

        val newChecklistName = randomName()
        editChecklistPresenter.saveChecklist(
                name = newChecklistName,
                collectionName = collectionTwoName
        )

        assertThat(compositeDisposable.size(), `is`(4))
    }

    @Test
    fun `should collect 4 disposable subscriptions when editing a checklist`() {
        val compositeDisposable = CompositeDisposable()
        val editChecklistPresenter = EditChecklistPresenter(
                editChecklistView = mockEditChecklistView,
                repository = mockRepository,
                compositeDisposable = compositeDisposable,
                checklistId = checklistOneId
        )
        editChecklistPresenter.presentData()

        val newChecklistName = randomName()
        editChecklistPresenter.saveChecklist(
                name = newChecklistName,
                collectionName = collectionTwoName
        )

        assertThat(compositeDisposable.size(), `is`(5))
    }

    @Test
    fun `should dispose of the composite disposable`() {
        val compositeDisposable = CompositeDisposable()
        val editChecklistPresenter = EditChecklistPresenter(
                editChecklistView = mockEditChecklistView,
                repository = mockRepository,
                compositeDisposable = compositeDisposable,
                checklistId = checklistOneId
        )
        editChecklistPresenter.presentData()

        editChecklistPresenter.dispose()

        assertTrue(compositeDisposable.isDisposed)
    }

    private fun setupMocksForAircraftWithMultipleCollections() {
        every {
            mockRepository.saveChecklistCollection(any())
        } returns Single.just(randomId())

        every {
            mockRepository.getAircraftTailNumber(aircraftIdForAircraftWithMultipleCollections)
        } returns Single.just(tailNumber)

        every {
            mockRepository.getChecklistCollections(aircraftIdForAircraftWithMultipleCollections)
        } returns Single.just(collectionList)

        every {
            mockRepository.getChecklist(checklistOneId)
        } returns Single.just(checklistOne)

        every {
            mockRepository.getChecklist(checklistTwoId)
        } returns Single.just(checklistTwo)

        every {
            mockRepository.getSiblingChecklistCollections(checklistOneId)
        } returns Single.just(collectionList)

        every {
            mockRepository.getSiblingChecklistCollections(checklistTwoId)
        } returns Single.just(collectionList)

        every {
            mockRepository.getChecklistOrderNumbersFromCollection(collectionTwoId)
        } returns Single.just(listOf(0L, 1L))
    }

    private fun setupMocksForAircraftWithZeroCollections() {
        every {
            mockRepository.getChecklistCollections(aircraftIdForAircraftWithZeroCollections)
        } returns Single.just(mutableListOf())

        every {
            mockRepository.getAircraftTailNumber(aircraftIdForAircraftWithZeroCollections)
        } returns Single.just(tailNumber)

        every {
            mockRepository.saveChecklistCollection(any())
        } returns Single.just(newCollectionId)

        every {
            mockRepository.getChecklistOrderNumbersFromCollection(newCollectionId)
        } returns Single.just(listOf())

        every {
            mockRepository.saveChecklist(any())
        } returns Completable.complete()
    }
}