package org.fossflight.efb.checklistCollection

import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.test.core.app.ActivityScenario
import androidx.test.ext.junit.runners.AndroidJUnit4
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Flowable
import org.fossflight.efb.MainActivity
import org.fossflight.efb.R
import org.fossflight.efb.data.model.Checklist
import org.fossflight.efb.data.persistence.FFRepository
import org.fossflight.efb.data.testChecklist
import org.fossflight.efb.data.testChecklistItem
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.endsWith
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Shadows.shadowOf
import org.robolectric.shadows.ShadowPopupMenu
import javax.inject.Inject

@RunWith(AndroidJUnit4::class)
class ChecklistCollectionPageItemViewHolderTest {

    private val mockChecklistCollectionViewPagerFragmentListener = mockk<ChecklistCollectionPageItemListener>()
    private val checklist: Checklist = testChecklist(
        name = "checklist name",
        items = listOf(testChecklistItem(), testChecklistItem())
    )

    @Inject
    private val mockRepository = mockk<FFRepository>()

    @Before
    fun `set up`() {
        every { mockRepository.allAircraft } returns Flowable.just(listOf())
        every { mockChecklistCollectionViewPagerFragmentListener.deleteChecklist(any()) } returns Unit
        every { mockChecklistCollectionViewPagerFragmentListener.startChecklistFragment(any()) } returns Unit
        every { mockChecklistCollectionViewPagerFragmentListener.editChecklist(any()) } returns Unit
    }

    @Test
    fun `should get the Text for the ContentView as the String`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->
                val view = LayoutInflater.from(mainActivity).inflate(R.layout.listable_card, null)

                val checklistCollectionPageItemViewHolder = ChecklistCollectionPageItemViewHolder(view, mockk())
                checklistCollectionPageItemViewHolder.bind(checklist)

                assertThat(checklistCollectionPageItemViewHolder.toString(), endsWith("'checklist name'"))
            }
        }
    }

    @Test
    fun `should Start the Checklist when Clicked`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->
                val view = LayoutInflater.from(mainActivity).inflate(R.layout.listable_card, null)

                val checklistCollectionPageItemViewHolder = ChecklistCollectionPageItemViewHolder(view, mockChecklistCollectionViewPagerFragmentListener)
                checklistCollectionPageItemViewHolder.bind(checklist)

                view.performClick()

                verify(exactly = 1) { mockChecklistCollectionViewPagerFragmentListener.startChecklistFragment(checklist.id) }
            }
        }
    }

    @Test
    fun `should Bind the Checklist to the View Holder`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->
                val view = LayoutInflater.from(mainActivity).inflate(R.layout.listable_card, null)
                val checklistCollectionPageItemViewHolder = ChecklistCollectionPageItemViewHolder(view, mockChecklistCollectionViewPagerFragmentListener)

                checklistCollectionPageItemViewHolder.bind(checklist)

                val actualName = (view.findViewById<View>(R.id.content) as TextView).text.toString()

                assertThat(actualName, `is`("checklist name"))
            }
        }
    }

    @Test
    fun `should hide the Short Description View`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->
                val view = LayoutInflater.from(mainActivity).inflate(R.layout.listable_card, null)
                val checklistCollectionPageItemViewHolder = ChecklistCollectionPageItemViewHolder(view, mockChecklistCollectionViewPagerFragmentListener)

                val emptyChecklist = testChecklist()

                checklistCollectionPageItemViewHolder.bind(emptyChecklist)

                val shortDescriptionView = view.findViewById<View>(R.id.short_description)

                assertThat(shortDescriptionView.visibility, `is`(View.GONE))
            }
        }
    }

    @Test
    fun `should display the Delete Option on the Popup Menu for the Checklist Item`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->
                val view = LayoutInflater.from(mainActivity).inflate(R.layout.listable_card, null)
                val checklistCollectionPageItemViewHolder = ChecklistCollectionPageItemViewHolder(view, mockChecklistCollectionViewPagerFragmentListener)

                checklistCollectionPageItemViewHolder.bind(checklist)

                view.performLongClick()

                val latestPopupMenu = ShadowPopupMenu.getLatestPopupMenu()
                val shadowLatestPopupMenu = shadowOf(latestPopupMenu)

                val menu = latestPopupMenu.menu
                val deleteItem = menu.getItem(1)

                assertThat(shadowLatestPopupMenu.isShowing, `is`(true))
                assertThat(deleteItem.title.toString(), `is`("Delete"))
            }
        }
    }

    @Test
    fun `should Attach the Checklist Collection Popup Menu Item Click Listener`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->
                val view = LayoutInflater.from(mainActivity).inflate(R.layout.listable_card, null)
                val checklistCollectionPageItemViewHolder = ChecklistCollectionPageItemViewHolder(view, mockChecklistCollectionViewPagerFragmentListener)

                checklistCollectionPageItemViewHolder.bind(checklist)

                view.performLongClick()

                val latestPopupMenu = ShadowPopupMenu.getLatestPopupMenu()
                val shadowLatestPopupMenu = shadowOf(latestPopupMenu)
                val onMenuItemClickListener = shadowLatestPopupMenu.onMenuItemClickListener

                val mockMenuItem = getMenuItem(R.id.delete_item)
                onMenuItemClickListener.onMenuItemClick(mockMenuItem)

                verify(exactly = 1) { mockChecklistCollectionViewPagerFragmentListener.deleteChecklist(checklist) }
            }
        }
    }

    @Test
    fun `should display the Edit Option on the Popup Menu`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->
                val view = LayoutInflater.from(mainActivity).inflate(R.layout.listable_card, null)
                val checklistCollectionPageItemViewHolder = ChecklistCollectionPageItemViewHolder(view, mockChecklistCollectionViewPagerFragmentListener)

                checklistCollectionPageItemViewHolder.bind(checklist)

                view.performLongClick()

                val latestPopupMenu = ShadowPopupMenu.getLatestPopupMenu()
                val shadowLatestPopupMenu = shadowOf(latestPopupMenu)

                val menu = latestPopupMenu.menu
                val editMenuItem = menu.getItem(0)

                assertThat(shadowLatestPopupMenu.isShowing, `is`(true))
                assertThat(editMenuItem.title.toString(), `is`("Edit"))
            }
        }
    }

    @Test
    fun `should Edit a Checklist Item from the Popup Menu`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->
                val view = LayoutInflater.from(mainActivity).inflate(R.layout.listable_card, null)
                val checklistCollectionPageItemViewHolder = ChecklistCollectionPageItemViewHolder(view, mockChecklistCollectionViewPagerFragmentListener)

                checklistCollectionPageItemViewHolder.bind(checklist)

                view.performLongClick()

                val latestPopupMenu = ShadowPopupMenu.getLatestPopupMenu()
                val shadowLatestPopupMenu = shadowOf(latestPopupMenu)
                val onMenuItemClickListener = shadowLatestPopupMenu.onMenuItemClickListener

                val mockMenuItem = getMenuItem(R.id.edit_item)

                onMenuItemClickListener.onMenuItemClick(mockMenuItem)

                verify(exactly = 1) { mockChecklistCollectionViewPagerFragmentListener.editChecklist(checklist.id) }
            }
        }
    }

    @Test
    @Ignore("This test should go away once the checklist count is replaced by the checklist description")
    fun `should use Singular Items when Checklist has One Item`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->
                val view = LayoutInflater.from(mainActivity).inflate(R.layout.listable_card, null)
                val checklistCollectionPageItemViewHolder = ChecklistCollectionPageItemViewHolder(view, mockChecklistCollectionViewPagerFragmentListener)

                val checklist = testChecklist(items = listOf(testChecklistItem()))

                checklistCollectionPageItemViewHolder.bind(checklist)

                val actualDescription = (view.findViewById<View>(R.id.short_description) as TextView).text.toString()

                assertThat(actualDescription, `is`("It has 1 Item"))
            }
        }
    }

    private fun getMenuItem(menuItemId: Int): MenuItem {
        val mockMenuItem = mockk<MenuItem>()
        every { mockMenuItem.itemId } returns menuItemId
        return mockMenuItem
    }
}