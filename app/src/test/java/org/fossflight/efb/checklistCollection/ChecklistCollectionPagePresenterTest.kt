package org.fossflight.efb.checklistCollection

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import org.fossflight.efb.ForceTrampolineSchedulers
import org.fossflight.efb.data.persistence.FFRepository
import org.fossflight.efb.data.randomId
import org.fossflight.efb.data.testChecklist
import org.hamcrest.core.Is.`is`
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class ChecklistCollectionPagePresenterTest {

    @get:Rule
    var forceTrampolineSchedulers = ForceTrampolineSchedulers()

    private val checklistCollectionId = randomId()
    private val checklistOneId = randomId()

    private val checklistOne = testChecklist(id = checklistOneId)

    private val checklistTwo = testChecklist()
    private val checklists = mutableListOf(checklistOne, checklistTwo)

    private val compositeDisposable = CompositeDisposable()

    private val mockChecklistCollectionPageView = mockk<ChecklistCollectionPageView>()
    private val mockRepository = mockk<FFRepository>()

    private val presenter = ChecklistCollectionPagePresenter(
            checklistCollectionPageView = mockChecklistCollectionPageView,
            repository = mockRepository,
            compositeDisposable = compositeDisposable,
            checklistCollectionId = checklistCollectionId
    )

    @Before
    fun `set up mocks`() {
        every { mockRepository.getChecklists(checklistCollectionId) } returns Single.just(checklists)
        every { mockRepository.deleteChecklist(checklistOne) } returns Completable.complete()
        every { mockRepository.deleteChecklist(checklistOne) } returns Completable.complete()
        every { mockRepository.deleteChecklist(checklistTwo) } returns Completable.complete()

        every { mockChecklistCollectionPageView.updateRecyclerView(any()) } returns Unit
        every { mockChecklistCollectionPageView.hideEmptyListMessage() } returns Unit
        every { mockChecklistCollectionPageView.displayEmptyListMessage() } returns Unit
        every { mockChecklistCollectionPageView.startChecklistFragment(any()) } returns Unit
        every { mockChecklistCollectionPageView.editChecklist(any()) } returns Unit
        every { mockChecklistCollectionPageView.displayDeletedChecklistDialog(any()) } returns Unit
    }

    @Test
    fun `should update the RecyclerView with the Aircraft Checklists`() {
        presenter.presentData()
        verify { mockChecklistCollectionPageView.updateRecyclerView(checklists) }
    }

    @Test
    fun `should hide the Empty List Message when Presenting at least one Checklist`() {
        presenter.presentData()
        verify { mockChecklistCollectionPageView.hideEmptyListMessage() }
    }

    @Test
    fun `should display the Empty Checklist Message when presenting Zero Checklists`() {
        every { mockRepository.getChecklists(checklistCollectionId) } returns Single.just(listOf())

        val anotherPresenter = ChecklistCollectionPagePresenter(
                checklistCollectionPageView = mockChecklistCollectionPageView,
                repository = mockRepository,
                checklistCollectionId = checklistCollectionId
        )

        anotherPresenter.presentData()
        verify { mockChecklistCollectionPageView.displayEmptyListMessage() }
    }

    @Test
    fun `should display the Empty Checklist Message after deleting the last Checklist`() {
        presenter.presentData()
        presenter.deleteChecklist(checklistOne)
        presenter.deleteChecklist(checklistTwo)
        verify { mockChecklistCollectionPageView.displayEmptyListMessage() }
    }

    @Test
    fun `should start the Checklist Fragment`() {
        presenter.presentData()
        presenter.startChecklistFragment(checklistOneId)
        verify { mockChecklistCollectionPageView.startChecklistFragment(checklistOneId) }
    }

    @Test
    fun `should delete a Checklist from the Checklist Collection`() {

        val originalChecklist = checklists.toMutableList()
        val checklistsAfterDeleting = listOf(checklistTwo)
        presenter.presentData()
        presenter.deleteChecklist(checklistOne)

        verify { mockChecklistCollectionPageView.updateRecyclerView(originalChecklist) }
        verify { mockChecklistCollectionPageView.updateRecyclerView(checklistsAfterDeleting) }
    }

    @Test
    fun `should display a message dialog after deleting a Checklist`() {
        presenter.presentData()
        presenter.deleteChecklist(checklistOne)
        verify { mockRepository.deleteChecklist(checklistOne) }
        verify { mockChecklistCollectionPageView.displayDeletedChecklistDialog(checklistOne.name) }
    }

    @Test
    fun `should Edit the Checklist Information from the Checklist Collection Page View`() {
        presenter.presentData()
        presenter.editChecklist(checklistOneId)
        verify { mockChecklistCollectionPageView.editChecklist(checklistOneId) }
    }

    @Test
    fun `should dispose of the CompositeDisposable`() {
        presenter.presentData()
        presenter.dispose()
        assertThat(compositeDisposable.isDisposed, `is`(true))
    }

    @Test
    fun `should add a subscription to the Composite Disposable when presenting the data`() {
        presenter.presentData()
        assertThat(compositeDisposable.size(), `is`(1))
    }

    @Test
    fun `should add another subscription to the CompositeDisposable when deleting a Checklist`() {
        presenter.presentData()
        presenter.deleteChecklist(checklistOne)
        assertThat(compositeDisposable.size(), `is`(2))
    }
}