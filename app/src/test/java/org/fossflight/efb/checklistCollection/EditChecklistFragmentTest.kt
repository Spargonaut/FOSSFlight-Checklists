package org.fossflight.efb.checklistCollection

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.Spinner
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withSpinnerText
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.android.material.textfield.TextInputLayout
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Completable
import io.reactivex.Single
import org.fossflight.efb.BundleKeys
import org.fossflight.efb.BundleKeys.EMERGENCY_CHECKLIST_COLLECTION_NAME
import org.fossflight.efb.BundleKeys.STANDARD_CHECKLIST_COLLECTION_NAME
import org.fossflight.efb.ForceTrampolineSchedulers
import org.fossflight.efb.FossFlightFragmentListener
import org.fossflight.efb.FossFlightTestApplication
import org.fossflight.efb.R
import org.fossflight.efb.TestAppModule
import org.fossflight.efb.data.model.Checklist
import org.fossflight.efb.data.persistence.FFRepository
import org.fossflight.efb.data.randomId
import org.fossflight.efb.data.randomName
import org.fossflight.efb.data.randomTailNumber
import org.fossflight.efb.data.testChecklist
import org.fossflight.efb.data.testChecklistCollection
import org.hamcrest.CoreMatchers.containsString
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@Config(application = FossFlightTestApplication::class)
@RunWith(AndroidJUnit4::class)
class EditChecklistFragmentTest {

    @get:Rule
    var forceTrampolineSchedulers = ForceTrampolineSchedulers()

    private val checklistId = randomId()
    private val landingChecklistCollectionId = randomId()
    private val checklistName = randomName()
    private val aircraftId = randomId()
    private val tailNumber = randomTailNumber()
    private val checklistCollectionName = "Landing Checklist"
    private val collections = listOf(
            testChecklistCollection(
                    aircraftId = aircraftId, name = STANDARD_CHECKLIST_COLLECTION_NAME,
                    checklists = listOf(testChecklist(), testChecklist(), testChecklist())
            ),
            testChecklistCollection(
                    aircraftId = aircraftId, name = EMERGENCY_CHECKLIST_COLLECTION_NAME,
                    checklists = listOf(testChecklist(), testChecklist(), testChecklist())
            ),
            testChecklistCollection(
                    aircraftId = aircraftId, name = "Take Off Checklist",
                    checklists = listOf(testChecklist(), testChecklist(), testChecklist())
            ),
            testChecklistCollection(
                    id = landingChecklistCollectionId, aircraftId = aircraftId, name = checklistCollectionName,
                    checklists = listOf(testChecklist(), testChecklist(), testChecklist())
            ),
            testChecklistCollection(
                    aircraftId = aircraftId, name = "Other Checklist",
                    checklists = listOf(testChecklist(), testChecklist(), testChecklist())
            )
    )

    private val checklist = testChecklist(
            id = checklistId,
            checklistCollectionId = landingChecklistCollectionId,
            name = checklistName
    )

    private val mockRepository = mockk<FFRepository>()
    private val mockFossFlightFragmentListener = mockk<FossFlightFragmentListener>()

    val instantiate: () -> EditChecklistFragment = {
        val fragment = EditChecklistFragment()
        fragment.setParentActivity(mockFossFlightFragmentListener)
        fragment
    }

    @Before
    fun `set up`() {
        val application = ApplicationProvider.getApplicationContext<FossFlightTestApplication>().apply {
            setTheme(R.style.AppTheme)
        }

        val testAppModule = TestAppModule(
                context = application.applicationContext,
                repository = mockRepository
        )

        application.setAppModule(testAppModule)

        every { mockRepository.getChecklistCollections(aircraftId) } returns Single.just(collections)
        every { mockRepository.getAircraftTailNumber(aircraftId) } returns Single.just(tailNumber)
        every { mockRepository.getChecklist(checklistId) } returns Single.just(checklist)
        every {
            mockRepository.getSiblingChecklistCollections(checklistId)
        } returns Single.just(collections)

        every { mockFossFlightFragmentListener.updateToolbarTitle(any()) } returns Unit
        every { mockFossFlightFragmentListener.updateToolbarSubtitle(any()) } returns Unit
        every { mockFossFlightFragmentListener.displayToast(any()) } returns Unit
        every { mockFossFlightFragmentListener.finishFragment() } returns Unit
    }

    @Test
    fun `should set the Toolbar Text appopriately when adding a Checklist`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.AIRCRAFT_ID, aircraftId)

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            val expectedTitle = it.requireActivity().getString(R.string.add_checklist)
            verify { mockFossFlightFragmentListener.updateToolbarTitle(expectedTitle) }
            verify { mockFossFlightFragmentListener.updateToolbarSubtitle(tailNumber) }
        }
    }

    @Test
    fun `should update the Checklist Information when adding a checklist`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.AIRCRAFT_ID, aircraftId)

        launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        onView(withId(R.id.checklist_name_edit)).check(matches(withText("")))
        onView(withId(R.id.checklist_type_spinner)).check(matches(withSpinnerText(containsString(STANDARD_CHECKLIST_COLLECTION_NAME))))
    }

    @Test
    fun `should set the Toolbar Text appropriately when updating a Checklist`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_ID, checklistId)

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            val expectedTitle = it.requireActivity().getString(R.string.edit_checklist)
            verify { mockFossFlightFragmentListener.updateToolbarTitle(expectedTitle) }
            verify { mockFossFlightFragmentListener.updateToolbarSubtitle(tailNumber) }
        }
    }

    @Test
    fun `should update the Checklist Information when updating a checklist`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_ID, checklistId)

        launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        onView(withId(R.id.checklist_name_edit)).check(matches(withText(checklistName)))
        onView(withId(R.id.checklist_type_spinner)).check(matches(withSpinnerText(containsString(checklistCollectionName))))
    }

    @Test
    fun `should Save the Checklist Details, display a successful toast and finish the fragment when Save Icon Is Clicked`() {
        every {
            mockRepository.getChecklistOrderNumbersFromCollection(any())
        } returns Single.just(listOf())

        every { mockRepository.saveChecklist(any()) } returns Completable.complete()

        val positionOfLandingChecklist = 3

        val arguments = Bundle()
        arguments.putLong(BundleKeys.AIRCRAFT_ID, aircraftId)

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        val newChecklistName = randomName()

        scenario.onFragment {
            it.requireActivity().findViewById<EditText>(R.id.checklist_name_edit).setText(newChecklistName)
            it.requireActivity().findViewById<Spinner>(R.id.checklist_type_spinner).setSelection(positionOfLandingChecklist)

            it.onOptionsItemSelected(withMenuItem(R.id.save))
        }

        val expectedChecklist = Checklist(
                id = BundleKeys.UNDEFINED,
                checklistCollectionId = landingChecklistCollectionId,
                name = newChecklistName,
                orderNumber = 0
        )

        verify { mockRepository.saveChecklist(expectedChecklist) }
        verify { mockFossFlightFragmentListener.displayToast("Saved $newChecklistName") }
        verify { mockFossFlightFragmentListener.finishFragment() }
    }

    @Test
    fun `should finish the fragment after a Cancel click`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.AIRCRAFT_ID, aircraftId)

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment { it.onOptionsItemSelected(withMenuItem(android.R.id.home)) }

        verify (exactly = 0) { mockRepository.saveChecklistItem(any()) }
        verify (exactly = 0) { mockFossFlightFragmentListener.displayToast(any()) }
        verify (exactly = 1) { mockFossFlightFragmentListener.finishFragment() }
    }

    @Test
    fun `should display A Name Required Violation when attempting to save with an empty Checklist Name`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.AIRCRAFT_ID, aircraftId)

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            it.requireActivity().findViewById<EditText>(R.id.checklist_name_edit).setText("")

            it.onOptionsItemSelected(withMenuItem(R.id.save))
        }

        val expectedErrorMessage = "The Checklist Name is Required"
        verify { mockFossFlightFragmentListener.displayToast(expectedErrorMessage) }
        onView(withId(R.id.input_layout_name)).check(matches(hasTextInputLayoutErrorText(expectedErrorMessage)))
    }

    private fun withMenuItem(menuItemId: Int): MenuItem {
        val mockMenuItem = mockk<MenuItem>()
        every { mockMenuItem.itemId } returns menuItemId
        return mockMenuItem
    }
}

fun hasTextInputLayoutErrorText(expectedErrorText: String): Matcher<View> = object : TypeSafeMatcher<View>() {
    override fun describeTo(description: Description?) { }
    override fun matchesSafely(item: View?): Boolean {
        if (item !is TextInputLayout) return false
        val error = item.error ?: return false
        val errorMessage = error.toString()
        return expectedErrorText == errorMessage
    }
}