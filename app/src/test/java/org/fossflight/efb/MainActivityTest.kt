package org.fossflight.efb

import android.view.View.GONE
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ApplicationProvider
import androidx.test.core.app.launchActivity
import androidx.test.ext.junit.runners.AndroidJUnit4
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Flowable
import io.reactivex.Single
import org.apache.commons.lang3.RandomStringUtils
import org.fossflight.efb.aircraftList.AircraftListFragment
import org.fossflight.efb.aircraftList.EditAircraftFragment
import org.fossflight.efb.aircraftList.dialog.ExportAircraftDialogFragment
import org.fossflight.efb.aircraftList.dialog.ImportAircraftDialogFragment
import org.fossflight.efb.aircraftList.dialog.SelectParserDialogFragment
import org.fossflight.efb.checklist.ChecklistFragment
import org.fossflight.efb.checklist.EditChecklistItemFragment
import org.fossflight.efb.checklistCollection.ChecklistCollectionPageFragment
import org.fossflight.efb.checklistCollection.ChecklistCollectionViewPagerFragment
import org.fossflight.efb.checklistCollection.EditChecklistFragment
import org.fossflight.efb.data.model.Aircraft
import org.fossflight.efb.data.persistence.FFRepository
import org.fossflight.efb.data.randomId
import org.fossflight.efb.data.randomName
import org.fossflight.efb.data.randomTailNumber
import org.fossflight.efb.data.testChecklist
import org.fossflight.efb.data.testChecklistItem
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Shadows.shadowOf
import org.robolectric.annotation.Config
import org.robolectric.shadows.ShadowAlertDialog
import org.robolectric.shadows.ShadowToast

@Config(application = FossFlightTestApplication::class)
@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    private val mockRepository = mockk<FFRepository>()

    private val aircraftListFragmentClassName = AircraftListFragment::class.java.simpleName

    private val messageFromLatestAlertDialog: String
        get() {
            return shadowOf(ShadowAlertDialog.getLatestAlertDialog()).message.toString()
        }

    @Before
    fun `set up`() {
        val application = ApplicationProvider.getApplicationContext<FossFlightTestApplication>()
        val testAppModule = TestAppModule(
                context = application.applicationContext,
                repository = mockRepository
        )
        application.setAppModule(testAppModule)

        every { mockRepository.allAircraft } returns Flowable.just(listOf())
    }

    @Test
    fun `should Start The AircraftListFragment when being Created`() {
        val scenario = launchActivity<MainActivity>()
        scenario.onActivity {
            val fragment = it.supportFragmentManager.findFragmentByTag(aircraftListFragmentClassName)
            assertThat(fragment!!.isVisible, `is`(true))
        }
    }

    @Test
    fun `should Display the AircraftListFragment Visible as the First Fragment`() {
        val scenario = launchActivity<MainActivity>()
        scenario.moveToState(Lifecycle.State.CREATED)
        scenario.onActivity {
            val fragment = it.supportFragmentManager.findFragmentByTag(aircraftListFragmentClassName)!!
            assertThat(fragment.isVisible, `is`(true))
        }
    }

    @Test
    fun `should display the Add Aircraft Dialog Fragment`() {
        val scenario = launchActivity<MainActivity>()
        scenario.onActivity {
            val aircraftListFragment = it.supportFragmentManager.findFragmentByTag(aircraftListFragmentClassName)
            it.startAddAircraftDialog(aircraftListFragment as AircraftListFragment)

            val addAircraftDialogFragmentClassName = ImportAircraftDialogFragment::class.java.simpleName
            val addAircraftDialogFragment = it.supportFragmentManager.findFragmentByTag(addAircraftDialogFragmentClassName)!!
            assertNotNull(addAircraftDialogFragment)
            val targetFragment = addAircraftDialogFragment.targetFragment as AircraftListFragment
            assertThat(targetFragment, `is`(aircraftListFragment))
        }
    }

    @Test
    fun `should display the Export Aircraft Dialog Fragment`() {
        val scenario = launchActivity<MainActivity>()
        scenario.onActivity {
            val aircraftListFragment = it.supportFragmentManager.findFragmentByTag(aircraftListFragmentClassName)
            it.startExportAircraftDialog(aircraftListFragment as AircraftListFragment, randomId(), randomTailNumber())

            val exportAircraftDialogFragmentClassName = ExportAircraftDialogFragment::class.java.simpleName
            val exportAircraftDialogFragment = it.supportFragmentManager.findFragmentByTag(exportAircraftDialogFragmentClassName)!!
            assertNotNull(exportAircraftDialogFragment)
            val targetFragment = exportAircraftDialogFragment.targetFragment as AircraftListFragment
            assertThat(targetFragment, `is`(aircraftListFragment))
        }
    }

    @Test
    fun `should display the Select Parser Dialog Fragment`() {
        val scenario = launchActivity<MainActivity>()
        scenario.onActivity {
            val aircraftListFragment = it.supportFragmentManager.findFragmentByTag(aircraftListFragmentClassName)
            it.startSelectParserDialogFragment(aircraftListFragment as AircraftListFragment)

            val selectParserDialogFragmentClassName = SelectParserDialogFragment::class.java.simpleName
            val selectParserDialogFragment = it.supportFragmentManager.findFragmentByTag(selectParserDialogFragmentClassName)!!
            assertNotNull(selectParserDialogFragment)
            val targetFragment = selectParserDialogFragment.targetFragment as AircraftListFragment
            assertThat(targetFragment, `is`(aircraftListFragment))
        }
    }

    @Test
    fun `should make the ChecklistFragment Visible`() {
        val checklistId = randomId()
        val checklist = testChecklist(
                id = checklistId,
                items = listOf(testChecklistItem(), testChecklistItem())
        )

        every { mockRepository.getChecklist(checklistId) } returns Single.just(checklist)
        every {
            mockRepository.getAircraftTailNumberForChecklist(checklistId)
        } returns Single.just(randomTailNumber())

        val scenario = launchActivity<MainActivity>()
        scenario.onActivity {
            it.startChecklistFragment(checklistId)
            val checklistFragmentClassName = ChecklistFragment::class.java.simpleName
            val checklistListFragment = it.supportFragmentManager.findFragmentByTag(checklistFragmentClassName)!!
            assertThat(checklistListFragment.isVisible, `is`(true))
        }
    }

    @Test
    fun `should make the ChecklistCollectionFragment Visible`() {
        val aircraftId = randomId()

        every { mockRepository.getAircraftTailNumber(aircraftId) } returns Single.just(randomTailNumber())

        every { mockRepository.getChecklists(BundleKeys.UNDEFINED) } returns Single.just(listOf())

        every { mockRepository.getChecklistCollections(aircraftId) } returns Single.just(listOf())

        val scenario = launchActivity<MainActivity>()
        scenario.onActivity {
            it.startChecklistCollectionFragment(aircraftId)
            val checklistCollectionFragmentClassName = ChecklistCollectionViewPagerFragment::class.java.simpleName
            val fragment = it.supportFragmentManager.findFragmentByTag(checklistCollectionFragmentClassName)!!
            assertThat(fragment.isVisible, `is`(true))
        }
    }

    @Test
    fun `should display the need for Write Permissions when Write Permissions are Denied`() {
        val grantResults = intArrayOf(-1)
        val permissions = arrayOf<String>()
        val requestCode = RequestCodes.WRITE_EXTERNAL_STORAGE

        val scenario = launchActivity<MainActivity>()
        scenario.onActivity {
            it.onRequestPermissionsResult(requestCode, permissions, grantResults)
            val actualMessage = messageFromLatestAlertDialog
            val expectedMessage = it.getString(R.string.need_write_permissions_message)
            assertThat(actualMessage, `is`("We need write permissions to export!"))
            assertThat(actualMessage, `is`(expectedMessage))
        }
    }

    @Test
    fun `should Finish Activity when BackStack is Emptied`() {
        val scenario = launchActivity<MainActivity>()
        scenario.onActivity {
            assertThat(it.supportFragmentManager.backStackEntryCount, `is`(1))
            it.onBackPressed()
            assertThat(it.isFinishing, `is`(true))
        }
    }

    @Test
    fun `should make the EditChecklistFragment Visible to Add a Checklist`() {
        val aircraftId = randomId()

        every { mockRepository.getChecklistCollections(aircraftId) } returns Single.just(listOf())

        val scenario = launchActivity<MainActivity>()
        scenario.onActivity {
            it.startAddChecklistFragment(aircraftId)
            val editChecklistFragmentClassName = EditChecklistFragment::class.java.simpleName
            val fragment = it.supportFragmentManager.findFragmentByTag(editChecklistFragmentClassName)!!
            assertThat(fragment.isVisible, `is`(true))
        }
    }

    @Test
    fun `should make the EditChecklistFragment Visible to Edit a Checklist`() {
        val checklistId = randomId()

        every { mockRepository.getChecklist(checklistId) } returns Single.just(testChecklist())

        val scenario = launchActivity<MainActivity>()
        scenario.onActivity {
            it.startEditChecklistFragment(checklistId)
            val editChecklistFragmentClassName = EditChecklistFragment::class.java.simpleName
            val fragment = it.supportFragmentManager.findFragmentByTag(editChecklistFragmentClassName)!!
            assertThat(fragment.isVisible, `is`(true))
        }
    }

    @Test
    fun `should Display a Snackbar with a Custom Message`() {
        val someRandomMessage = RandomStringUtils.random(20)

        val scenario = launchActivity<MainActivity>()
        scenario.onActivity {
            it.displaySnackbar(someRandomMessage)
            val snackBarTextView = it.findViewById<TextView>(R.id.snackbar_text)
            assertThat(snackBarTextView.text.toString(), `is`(someRandomMessage))
        }
    }

    @Test
    fun `should Hide the Action Button Text on the Snackbar by Default`() {
        val scenario = launchActivity<MainActivity>()
        scenario.onActivity {
            it.displaySnackbar(RandomStringUtils.random(20))

            val snackBarActionButton = it.findViewById<TextView>(R.id.snackbar_action)
            assertThat(snackBarActionButton.hasOnClickListeners(), `is`(false))
            assertThat(snackBarActionButton.visibility, `is`(GONE))
        }
    }

    @Test
    fun `Should display a short Toast message`() {
        val message = randomName()

        val scenario = launchActivity<MainActivity>()
        scenario.onActivity {
            it.displayToast(message)
        }

        assertThat(ShadowToast.getTextOfLatestToast(), `is`(message))
        assertThat(ShadowToast.getLatestToast().duration, `is`(Toast.LENGTH_SHORT))
    }

    @Test
    fun `should set up the toolbar appropriately when starting the AircraftListFragment`() {
        val scenario = launchActivity<MainActivity>()

        scenario.moveToState(Lifecycle.State.CREATED)

        scenario.onActivity {
            val displayOptions = it.supportActionBar!!.displayOptions
            val expectedDisplayOptions = 15
            assertThat(displayOptions, `is`(expectedDisplayOptions))

            val toolbar = it.findViewById<Toolbar>(R.id.toolbar)

            val actualDrawable = toolbar.navigationIcon
            val actualResId = shadowOf(actualDrawable).createdFromResId
            val expectedDrawable = it.getDrawable(R.drawable.ic_airplane)
            val expectedResId = shadowOf(expectedDrawable).createdFromResId
            assertThat(actualResId, `is`(expectedResId))

            val expectedTitle = it.getString(R.string.app_name)
            val expectedSubtitle = it.getString(R.string.your_aircraft_list)

            val actualTitle = toolbar.title.toString()
            assertThat(actualTitle, `is`(expectedTitle))
            assertThat(toolbar.subtitle, `is`<CharSequence>(expectedSubtitle))
        }
    }

    @Test
    fun `should update the toolbar with the AircraftIcon`() {
        val scenario = launchActivity<MainActivity>()
        scenario.onActivity {
            it.updateToolbarIcon(R.drawable.ic_airplane)

            val actualDrawable = it.findViewById<Toolbar>(R.id.toolbar).navigationIcon
            val actualResId = shadowOf(actualDrawable).createdFromResId
            val expectedDrawable = it.getDrawable(R.drawable.ic_airplane)
            val expectedResId = shadowOf(expectedDrawable).createdFromResId
            assertThat(actualResId, `is`(expectedResId))
        }
    }

    @Test
    fun `should update the title of the toolbar`() {
        val testTitle = randomName()

        val scenario = launchActivity<MainActivity>()
        scenario.onActivity {
            it.updateToolbarTitle(testTitle)

            val toolbar = it.findViewById<Toolbar>(R.id.toolbar)
            val actualTitle = toolbar.title.toString()
            assertThat(actualTitle, `is`<String>(testTitle))
        }
    }

    @Test
    fun `should update the subtitle of the toolbar`() {
        val testSubtitle = randomName()
        val scenario = launchActivity<MainActivity>()
        scenario.onActivity {
            it.updateToolbarSubtitle(testSubtitle)

            val toolbar = it.findViewById<Toolbar>(R.id.toolbar)
            val actualSubtitle = toolbar.subtitle.toString()
            assertThat(actualSubtitle, `is`<String>(testSubtitle))
        }
    }

    @Test
    fun `should set up the toolbar appropriately when starting the ChecklistFragment`() {
        val checklistId = randomId()
        val checklist = testChecklist(
                id = checklistId,
                items = listOf(testChecklistItem(), testChecklistItem())
        )

        every { mockRepository.getChecklist(checklistId) } returns Single.just(checklist)
        every {
            mockRepository.getAircraftTailNumberForChecklist(checklistId)
        } returns Single.just(randomTailNumber())

        val scenario = launchActivity<MainActivity>()
        scenario.onActivity {
            it.startChecklistFragment(checklistId)

            val displayOptions = it.supportActionBar!!.displayOptions
            val expectedDisplayOptions = 15
            assertThat(displayOptions, `is`(expectedDisplayOptions))

            val actualDrawable = it.findViewById<Toolbar>(R.id.toolbar).navigationIcon
            val actualResId = shadowOf(actualDrawable).createdFromResId
            val expectedDrawable = it.getDrawable(R.drawable.ic_arrow_back)
            val expectedResId = shadowOf(expectedDrawable).createdFromResId
            assertThat(actualResId, `is`(expectedResId))
        }
    }

    @Test
    fun `should set up the toolbar appropriately when starting the EditAircraftFragment`() {
        val aircraftId = randomId()
        val tailNumber = randomTailNumber()
        val aircraft = Aircraft(id = aircraftId, tailNumber = tailNumber)

        every { mockRepository.getShallowAircraft(aircraftId) } returns Single.just(aircraft)

        val scenario = launchActivity<MainActivity>()
        scenario.onActivity {
            it.startEditAircraftFragment(aircraftId)

            val displayOptions = it.supportActionBar!!.displayOptions
            val expectedDisplayOptions = 15
            assertThat(displayOptions, `is`(expectedDisplayOptions))
            val actualDrawable = it.findViewById<Toolbar>(R.id.toolbar).navigationIcon
            val actualResId = shadowOf(actualDrawable).createdFromResId
            val expectedDrawable = it.getDrawable(R.drawable.ic_close)
            val expectedResId = shadowOf(expectedDrawable).createdFromResId
            assertThat(actualResId, `is`(expectedResId))
        }
    }

    @Test
    fun `should set up the toolbar appropriately when starting the AddChecklistFragment`() {
        val checklistId = randomId()

        every { mockRepository.getChecklist(checklistId) } returns Single.just(testChecklist())
        every { mockRepository.getChecklistCollections(checklistId) } returns Single.just(listOf())

        val scenario = launchActivity<MainActivity>()
        scenario.onActivity {
            it.startEditChecklistFragment(checklistId)

            val displayOptions = it.supportActionBar!!.displayOptions
            val expectedDisplayOptions = 15
            assertThat(displayOptions, `is`(expectedDisplayOptions))

            val actualDrawable = it.findViewById<Toolbar>(R.id.toolbar).navigationIcon
            val actualResId = shadowOf(actualDrawable).createdFromResId
            val expectedDrawable = it.getDrawable(R.drawable.ic_close)
            val expectedResId = shadowOf(expectedDrawable).createdFromResId
            assertThat(actualResId, `is`(expectedResId))
        }
    }

    @Test
    fun `should set up the toolbar appropriately when starting the EditChecklistFragment`() {
        val aircraftId = randomId()
        val tailNumber = randomTailNumber()
        val aircraft = Aircraft(id = aircraftId, tailNumber = tailNumber)

        every { mockRepository.getShallowAircraft(aircraftId) } returns Single.just(aircraft)
        every { mockRepository.getChecklistCollections(aircraftId) } returns Single.just(listOf())

        val scenario = launchActivity<MainActivity>()
        scenario.onActivity {
            it.startAddChecklistFragment(aircraftId)

            val displayOptions = it.supportActionBar!!.displayOptions
            val expectedDisplayOptions = 15
            assertThat(displayOptions, `is`(expectedDisplayOptions))

            val actualDrawable = it.findViewById<Toolbar>(R.id.toolbar).navigationIcon
            val actualResId = shadowOf(actualDrawable).createdFromResId
            val expectedDrawable = it.getDrawable(R.drawable.ic_close)
            val expectedResId = shadowOf(expectedDrawable).createdFromResId
            assertThat(actualResId, `is`(expectedResId))
        }
    }

    @Test
    fun `should set up the toolbar appropriately when starting the EditChecklistItemFragment`() {
        val checklistId = randomId()
        val checklistItemId = randomId()

        every { mockRepository.getChecklistItem(checklistItemId) } returns Single.just(testChecklistItem())

        val scenario = launchActivity<MainActivity>()
        scenario.onActivity {
            it.startEditChecklistItemFragment(checklistId, checklistItemId)

            val displayOptions = it.supportActionBar!!.displayOptions
            val expectedDisplayOptions = 15
            assertThat(displayOptions, `is`(expectedDisplayOptions))

            val actualDrawable = it.findViewById<Toolbar>(R.id.toolbar).navigationIcon
            val actualResId = shadowOf(actualDrawable).createdFromResId
            val expectedDrawable = it.getDrawable(R.drawable.ic_close)
            val expectedResId = shadowOf(expectedDrawable).createdFromResId
            assertThat(actualResId, `is`(expectedResId))
        }
    }

    @Test
    fun `should set up the toolbar appropriately when starting the ChecklistCollectionViewPagerFragment`() {
        val aircraftId = randomId()
        every { mockRepository.getChecklistCollections(aircraftId) } returns Single.just(listOf())
        every { mockRepository.getAircraftTailNumber(aircraftId) } returns Single.just(randomTailNumber())

        val scenario = launchActivity<MainActivity>()
        scenario.onActivity {
            it.startChecklistCollectionFragment(aircraftId)

            val displayOptions = it.supportActionBar!!.displayOptions

            val actualDrawable = it.findViewById<Toolbar>(R.id.toolbar).navigationIcon
            val actualResId = shadowOf(actualDrawable).createdFromResId

            val expectedDrawable = it.getDrawable(R.drawable.ic_arrow_back)
            val expectedResId = shadowOf(expectedDrawable).createdFromResId

            val expectedDisplayOptions = 15
            assertThat(displayOptions, `is`(expectedDisplayOptions))
            assertThat(actualResId, `is`(expectedResId))
        }
    }

    @Test
    fun `should add itself as the parent activity to the AircraftListFragment`() {
        val mockAircraftListFragment = mockk<AircraftListFragment>(relaxed = true)
        val scenario = launchActivity<MainActivity>()
        scenario.onActivity {
            it.onAttachFragment(mockAircraftListFragment)
            verify { mockAircraftListFragment.setParentActivity(it) }
        }
    }

    @Test
    fun `should add itself as the parent activity to the ChecklistCollectionViewPagerFragment`() {
        val mockChecklistCollectionViewPagerFragment = mockk<ChecklistCollectionViewPagerFragment>(relaxed = true)
        val scenario = launchActivity<MainActivity>()
        scenario.onActivity {
            it.onAttachFragment(mockChecklistCollectionViewPagerFragment)
            verify { mockChecklistCollectionViewPagerFragment.setParentActivity(it) }
        }
    }

    @Test
    fun `should add itself as the parent activity to the ChecklistFragment`() {
        val mockChecklistFragment = mockk<ChecklistFragment>(relaxed = true)
        val scenario = launchActivity<MainActivity>()
        scenario.onActivity {
            it.onAttachFragment(mockChecklistFragment)
            verify { mockChecklistFragment.setParentActivity(it) }
        }
    }

    @Test
    fun `should add itself as the parent activity to the ChecklistCollectionPageFragment`() {
        val mockChecklistCollectionPageFragment = mockk<ChecklistCollectionPageFragment>(relaxed = true)
        val scenario = launchActivity<MainActivity>()
        scenario.onActivity {
            it.onAttachFragment(mockChecklistCollectionPageFragment)
            verify { mockChecklistCollectionPageFragment.setParentActivity(it) }
        }
    }

    @Test
    fun `should add itself as the parent activity to the EditAircraftFragment`() {
        val mockEditAircraftFragment = mockk<EditAircraftFragment>(relaxed = true)
        val scenario = launchActivity<MainActivity>()
        scenario.onActivity {
            it.onAttachFragment(mockEditAircraftFragment)
            verify { mockEditAircraftFragment.setParentActivity(it) }
        }
    }

    @Test
    fun `should add itself as the parent activity to the EditChecklistFragment`() {
        val mockEditChecklistFragment = mockk<EditChecklistFragment>(relaxed = true)
        val scenario = launchActivity<MainActivity>()
        scenario.onActivity {
            it.onAttachFragment(mockEditChecklistFragment)
            verify { mockEditChecklistFragment.setParentActivity(it) }
        }
    }

    @Test
    fun `should add itself as the parent activity to the EditChecklistItemFragment`() {
        val mockEditChecklistItemFragment = mockk<EditChecklistItemFragment>(relaxed = true)
        val scenario = launchActivity<MainActivity>()
        scenario.onActivity {
            it.onAttachFragment(mockEditChecklistItemFragment)
            verify { mockEditChecklistItemFragment.setParentActivity(it) }
        }
    }
}