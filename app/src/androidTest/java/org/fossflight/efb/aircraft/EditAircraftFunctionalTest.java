package org.fossflight.efb.aircraft;

import org.fossflight.efb.MainActivity;
import org.junit.Rule;
import org.junit.Test;

import androidx.test.rule.ActivityTestRule;

import static androidx.test.espresso.Espresso.pressBack;
import static org.fossflight.efb.objectmodels.ChecklistModel.openOptionsMenu;
import static org.fossflight.efb.objectmodels.FossFlightActivityModel.tapCancelButton;
import static org.fossflight.efb.objectmodels.FossFlightActivityModel.tapSaveButton;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.deleteCardWithText;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.editCardWithText;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.numberOfCardsInTheRecyclerView;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.tapCardWithText;
import static org.fossflight.efb.objectmodels.action.AddAircraftDialogAction.tapAddAircraftManuallyItemInDialog;
import static org.fossflight.efb.objectmodels.action.AddAircraftDialogAction.tapImportExampleAircraftItemInDialog;
import static org.fossflight.efb.objectmodels.action.AircraftListAction.tapAddAircraftText;
import static org.fossflight.efb.objectmodels.action.ChecklistCollectionAction.scrollViewPagerToFirstPage;
import static org.fossflight.efb.objectmodels.action.ChecklistCollectionAction.tapChecklistWithText;
import static org.fossflight.efb.objectmodels.action.EditAircraftAction.updateManufacturerTo;
import static org.fossflight.efb.objectmodels.action.EditAircraftAction.updateModelTo;
import static org.fossflight.efb.objectmodels.action.EditAircraftAction.updateTailNumberTo;
import static org.fossflight.efb.objectmodels.action.EditAircraftAction.updateYearTo;
import static org.fossflight.efb.objectmodels.assertion.EditAircraftModelAssert.assertCardAtPositionHasContentDescription;
import static org.fossflight.efb.objectmodels.assertion.EditAircraftModelAssert.assertCardAtPositionHasTailNumber;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class EditAircraftFunctionalTest {

    @Rule
    public ActivityTestRule<MainActivity> mainActivityTestRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void shouldEditAnAircraftsInformation() {

        openOptionsMenu();
        tapAddAircraftText();
        tapImportExampleAircraftItemInDialog();

        String exampleAircraftTailNumber = "NEXFC1";
        editCardWithText(exampleAircraftTailNumber);

        String newTailNumber = "N99999";
        String manufacturer = "Cessna";
        String model = "Skyhawk";
        String year = "1976";

        updateTailNumberTo(newTailNumber);
        updateManufacturerTo(manufacturer);
        updateModelTo(model);
        updateYearTo(year);

        tapSaveButton();

        assertCardAtPositionHasTailNumber(0, newTailNumber);
        String contentString = year + " " + manufacturer + " " + model;
        assertCardAtPositionHasContentDescription(0, contentString);

        tapCardWithText(newTailNumber);
        scrollViewPagerToFirstPage();
        tapChecklistWithText("Climb Out");

        pressBack();
        pressBack();

        deleteCardWithText(newTailNumber);
    }

    @Test
    public void shouldCancelAddingAnAircraft() {
        int itemCountBeforeAdding = numberOfCardsInTheRecyclerView();

        openOptionsMenu();
        tapAddAircraftText();
        tapAddAircraftManuallyItemInDialog();

        tapCancelButton();

        int itemCountAfterAdding = numberOfCardsInTheRecyclerView();

        assertThat(itemCountAfterAdding - itemCountBeforeAdding, is(0));
    }
}
