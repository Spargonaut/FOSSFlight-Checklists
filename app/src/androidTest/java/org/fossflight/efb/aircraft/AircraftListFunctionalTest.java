package org.fossflight.efb.aircraft;

import android.os.Build;

import org.fossflight.efb.MainActivity;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import androidx.test.rule.ActivityTestRule;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiObjectNotFoundException;
import androidx.test.uiautomator.UiSelector;

import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.fossflight.efb.objectmodels.ChecklistModel.openOptionsMenu;
import static org.fossflight.efb.objectmodels.DeviceFilePickerModel.cancelFileSelection;
import static org.fossflight.efb.objectmodels.DeviceFilePickerModel.importFileFromInternalStorage;
import static org.fossflight.efb.objectmodels.FossFlightActivityModel.tapSaveButton;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.deleteCardWithText;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.editCardWithText;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.longClickCardWithText;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.numberOfCardsInTheRecyclerView;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.scrollRecyclerViewToPosition;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.tapCardWithText;
import static org.fossflight.efb.objectmodels.ViewPagerActions.scrollTabListToTheLeft;
import static org.fossflight.efb.objectmodels.ViewPagerActions.scrollTabListToTheRight;
import static org.fossflight.efb.objectmodels.action.AddAircraftDialogAction.tapAddAircraftManuallyItemInDialog;
import static org.fossflight.efb.objectmodels.action.AddAircraftDialogAction.tapImportAircraftItemInDialog;
import static org.fossflight.efb.objectmodels.action.AddAircraftDialogAction.tapImportExampleAircraftItemInDialog;
import static org.fossflight.efb.objectmodels.action.AircraftListAction.tapAddAircraftText;
import static org.fossflight.efb.objectmodels.action.AircraftListAction.tapExport;
import static org.fossflight.efb.objectmodels.action.AircraftListAction.tapExportAircraft;
import static org.fossflight.efb.objectmodels.action.AircraftListAction.tapOK;
import static org.fossflight.efb.objectmodels.action.ChecklistAction.markItemAsCompleted;
import static org.fossflight.efb.objectmodels.action.ChecklistAction.playChecklist;
import static org.fossflight.efb.objectmodels.action.ChecklistAction.tapAddChecklistItemText;
import static org.fossflight.efb.objectmodels.action.ChecklistAction.tapYesToStartNextChecklist;
import static org.fossflight.efb.objectmodels.action.ChecklistCollectionAction.scrollViewPagerToFirstPage;
import static org.fossflight.efb.objectmodels.action.EditAircraftAction.fillInTailNumber;
import static org.fossflight.efb.objectmodels.action.EditAircraftAction.selectChecklistCollectionType;
import static org.fossflight.efb.objectmodels.action.EditAircraftAction.updateTailNumberTo;
import static org.fossflight.efb.objectmodels.action.EditChecklistAction.fillInNameOfChecklist;
import static org.fossflight.efb.objectmodels.action.EditChecklistAction.tapAddChecklistText;
import static org.fossflight.efb.objectmodels.action.EditChecklistItemAction.addChecklistItem;
import static org.fossflight.efb.objectmodels.action.ImportChecklistTypeDialogAction.tapFltPlanGoChecklistTextInDialog;
import static org.fossflight.efb.objectmodels.action.ImportChecklistTypeDialogAction.tapFossFlightChecklistTextInDialog;
import static org.fossflight.efb.objectmodels.assertion.AircraftListAssert.assertCardAtPositionHasAircraftDescription;
import static org.fossflight.efb.objectmodels.assertion.AircraftListAssert.assertCardAtPositionHasTailNumber;
import static org.fossflight.efb.objectmodels.assertion.AircraftListAssert.assertHasAircraftCount;
import static org.fossflight.efb.objectmodels.assertion.AircraftListAssert.assertPermissionsExplanationDialogIsShown;
import static org.fossflight.efb.objectmodels.assertion.ChecklistCollectionAssert.assertCardAtPositionHasChecklistName;
import static org.fossflight.efb.objectmodels.assertion.ChecklistCollectionAssert.assertHasChecklistCount;
import static org.fossflight.efb.objectmodels.assertion.ChecklistModelAssert.assertCardAtPositionIsSectionHeaderWithText;
import static org.fossflight.efb.objectmodels.assertion.ChecklistModelAssert.assertHasItemCount;
import static org.fossflight.efb.objectmodels.assertion.RecyclerViewAssertions.assertTextIsDisplayed;
import static org.fossflight.efb.objectmodels.assertion.RecyclerViewAssertions.assertTextIsNotDisplayed;

public class AircraftListFunctionalTest {

    @Rule
    public ActivityTestRule<MainActivity> testRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void setup() {
        while (numberOfCardsInTheRecyclerView() > 0) {
            deleteCardWithText("NEXFC1");
        }
    }

    @Test
    public void shouldHandleNotGettingFileFromFilePicker() {
        openOptionsMenu();
        tapAddAircraftText();
        tapImportAircraftItemInDialog();
        tapFltPlanGoChecklistTextInDialog();
        cancelFileSelection();

        assertHasAircraftCount(0);
    }

    @Test
    public void shouldImportAndDeleteAnExampleAircraft() {
        openOptionsMenu();
        tapAddAircraftText();
        tapImportExampleAircraftItemInDialog();
        assertHasAircraftCount(1);

        assertCardAtPositionHasTailNumber(0, "NEXFC1");
        assertCardAtPositionHasAircraftDescription(0, "1976 Cessna 172N");

        deleteCardWithText("NEXFC1");
        assertHasAircraftCount(0);
    }

    @Test
    public void shouldImportAnAircraftsChecklistsExportedFromFltPlanGo() {
        openOptionsMenu();
        tapAddAircraftText();
        tapImportAircraftItemInDialog();
        tapFltPlanGoChecklistTextInDialog();
        importFileFromInternalStorage("Download", "N12345.csv");
        tapCardWithText("N12345");
        scrollViewPagerToFirstPage();
        assertHasChecklistCount(14);

        tapCardWithText("PREFLIGHT");
        assertHasItemCount(68);

        pressBack();
        pressBack();
        deleteCardWithText("N12345");
        assertHasAircraftCount(0);
    }

    @Test
    public void shouldExportAnAircraftAndReimportIt() {
        openOptionsMenu();
        tapAddAircraftText();
        tapImportExampleAircraftItemInDialog();

        String exampleAircraftTailNumber = "NEXFC1";
        longClickCardWithText(exampleAircraftTailNumber);
        tapExportAircraft();
        assertTextIsDisplayed("Export Aircraft NEXFC1");
        tapExport();
        tapPermissionButtonWithText("DENY");
        assertPermissionsExplanationDialogIsShown();
        tapOK();

        longClickCardWithText(exampleAircraftTailNumber);
        tapExportAircraft();
        tapExport();
        tapPermissionButtonWithText("ALLOW");

        editCardWithText(exampleAircraftTailNumber);
        String newTailNumber = "NEXFC1 - original";
        updateTailNumberTo(newTailNumber);
        tapSaveButton();

        openOptionsMenu();
        tapAddAircraftText();
        tapImportAircraftItemInDialog();
        tapFossFlightChecklistTextInDialog();
        importFileFromInternalStorage("FossFlight", "NEXFC1_ffc.csv");

        tapCardWithText("NEXFC1");
        assertHasItemCount(13);

        tapCardWithText("Preflight");
        assertHasItemCount(64);
        assertCardAtPositionIsSectionHeaderWithText(0, "Interior Preflight");
        scrollRecyclerViewToPosition(21);
        assertCardAtPositionIsSectionHeaderWithText(21, "Exterior Preflight");

        pressBack();

        tapCardWithText("Shutdown");
        assertHasItemCount(4);
        pressBack();

        scrollTabListToTheRight();      // This should put it on the set of Take Off Checklists
        assertHasItemCount(2);

        tapCardWithText("Normal Take Off");
        assertHasItemCount(8);
        pressBack();

        scrollTabListToTheRight();      // This should put it on the set of Take Off Checklists

        tapCardWithText("(Vx) Max Angle of Climb Take-Off");
        assertHasItemCount(7);
        pressBack();

        scrollTabListToTheRight();      // This should put it on the set of Take Off Checklists
        scrollTabListToTheRight();      // This should put it on the set of Landing Checklists
        assertHasItemCount(1);

        tapCardWithText("Balked Landing");
        assertHasItemCount(4);
        pressBack();

        scrollTabListToTheRight();      // This should put it on the set of Take Off Checklists
        scrollTabListToTheRight();      // This should put it on the set of Landing Checklists
        scrollTabListToTheRight();      // This should put it on the set of Emergency Checklist
        assertHasItemCount(4);

        tapCardWithText("Engine Failure");
        assertHasItemCount(12);
        pressBack();

        scrollTabListToTheRight();      // This should put it on the set of Take Off Checklists
        scrollTabListToTheRight();      // This should put it on the set of Landing Checklists
        scrollTabListToTheRight();      // This should put it on the set of Emergency Checklist

        tapCardWithText("Icing");
        assertHasItemCount(8);
        pressBack();

        pressBack();
        deleteCardWithText(exampleAircraftTailNumber);
        deleteCardWithText("NEXFC1 - original");
        assertHasAircraftCount(0);
    }

    @Test
    public void shouldCreateAnAircraftFullOfChecklistsManually() {

        String emptyAircraftListMessage =
                "It looks like you don't have any aircraft yet." +
                        "\n" +
                        "Tap 'Add Aircraft' in the options menu to get started.";
        assertTextIsDisplayed(emptyAircraftListMessage);

        openOptionsMenu();
        tapAddAircraftText();
        tapAddAircraftManuallyItemInDialog();

        String tailNumber = "N86869";
        fillInTailNumber(tailNumber);
        tapSaveButton();

        assertTextIsNotDisplayed(emptyAircraftListMessage);

        tapCardWithText(tailNumber);

        assertHasItemCount(0);
        scrollTabListToTheRight();
        assertHasItemCount(0);

        openOptionsMenu();
        tapAddChecklistText();
        String checklistName = "new checklist for " + tailNumber;
        fillInNameOfChecklist(checklistName);
        // Standard Checklist should be default in the Checklist Type Spinner
        tapSaveButton();

        assertHasItemCount(1);
        scrollTabListToTheRight();
        assertHasItemCount(0);

        openOptionsMenu();
        tapAddChecklistText();
        String emergencyChecklistName = "emergency checklist for " + tailNumber;
        fillInNameOfChecklist(emergencyChecklistName);
        selectChecklistCollectionType("Emergency Checklist");
        tapSaveButton();

        assertHasItemCount(1);
        scrollTabListToTheLeft();
        assertHasItemCount(1);

        tapCardWithText(checklistName);

        String emptyChecklistMessage =
                "It looks like you don't have any checklist items yet." +
                        "\n" +
                        "Tap 'Add Checklist Item' in the options menu to get started.";
        assertTextIsDisplayed(emptyChecklistMessage);

        openOptionsMenu();
        tapAddChecklistItemText();
        String checklistItemName = "a checklist item for " + tailNumber;
        String checklistItemAction = "a checklist item action for " + tailNumber;
        addChecklistItem(checklistItemName, checklistItemAction);
        tapSaveButton();

        playChecklist();
        markItemAsCompleted();
        tapYesToStartNextChecklist();

        assertCardAtPositionHasChecklistName(0, checklistName);

        pressBack();
        deleteCardWithText(tailNumber);
    }

    private static void tapPermissionButtonWithText(String permissionButtonText) {
        if (Build.VERSION.SDK_INT >= 23) {
            UiDevice device = UiDevice.getInstance(getInstrumentation());
            UiObject allowPermissionsDialog = findDialogButton(device, permissionButtonText);

            if (allowPermissionsDialog.exists()) {
                try {
                    allowPermissionsDialog.click();
                } catch (UiObjectNotFoundException e) {
                    throw new RuntimeException("There is no permissions dialog to interact with ");
                }
            } else {
                throw new AssertionError("Allow permissions dialog was not shown");
            }
        }
    }

    private static UiObject findDialogButton(UiDevice device, String clickableText) {

        UiObject uiObject;
        uiObject = device.findObject(new UiSelector().text(clickableText));

        if (!uiObject.exists()) {
            uiObject = device.findObject(new UiSelector().text(clickableText.toLowerCase()));
        }

        if (!uiObject.exists()) {
            uiObject = device.findObject(new UiSelector().text(clickableText.toUpperCase()));
        }

        return uiObject;
    }
}
