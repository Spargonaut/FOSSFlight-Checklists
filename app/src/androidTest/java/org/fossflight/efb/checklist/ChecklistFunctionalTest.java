package org.fossflight.efb.checklist;

import android.os.RemoteException;

import org.fossflight.efb.MainActivity;
import org.fossflight.efb.R;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import androidx.test.rule.ActivityTestRule;
import androidx.test.uiautomator.UiDevice;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static java.lang.Thread.sleep;
import static org.fossflight.efb.objectmodels.ChecklistModel.openOptionsMenu;
import static org.fossflight.efb.objectmodels.FossFlightActivityModel.tapSaveButton;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.deleteCardWithText;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.editCardWithText;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.numberOfCardsInTheRecyclerView;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.onTheListView;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.scrollRecyclerViewToPosition;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.tapCardWithText;
import static org.fossflight.efb.objectmodels.action.AddAircraftDialogAction.tapImportExampleAircraftItemInDialog;
import static org.fossflight.efb.objectmodels.action.AircraftListAction.tapAddAircraftText;
import static org.fossflight.efb.objectmodels.action.ChecklistAction.markItemAsCompleted;
import static org.fossflight.efb.objectmodels.action.ChecklistAction.playChecklist;
import static org.fossflight.efb.objectmodels.action.ChecklistAction.skipItem;
import static org.fossflight.efb.objectmodels.action.ChecklistAction.tapGoToPreviouslySkippedButton;
import static org.fossflight.efb.objectmodels.action.ChecklistAction.tapNextChecklistButton;
import static org.fossflight.efb.objectmodels.action.ChecklistAction.tapNoToDismissDialog;
import static org.fossflight.efb.objectmodels.action.ChecklistAction.tapResetChecklistText;
import static org.fossflight.efb.objectmodels.action.ChecklistAction.tapYesToReplaySkippedItems;
import static org.fossflight.efb.objectmodels.action.ChecklistAction.tapYesToStartNextChecklist;
import static org.fossflight.efb.objectmodels.action.ChecklistCollectionAction.scrollViewPagerToFirstPage;
import static org.fossflight.efb.objectmodels.action.ChecklistCollectionAction.tapChecklistWithText;
import static org.fossflight.efb.objectmodels.assertion.ChecklistModelAssert.assertCardAtPositionIsHighlighted;
import static org.fossflight.efb.objectmodels.assertion.ChecklistModelAssert.assertCardAtPositionIsPrechecked;
import static org.fossflight.efb.objectmodels.assertion.ChecklistModelAssert.assertCardAtPositionIsSectionHeaderWithText;
import static org.fossflight.efb.objectmodels.assertion.ChecklistModelAssert.assertCardAtPositionIsShownAsCompleted;
import static org.fossflight.efb.objectmodels.assertion.ChecklistModelAssert.assertCardAtPositionIsShownAsSkipped;
import static org.fossflight.efb.objectmodels.assertion.ChecklistModelAssert.assertCheckItemIconIsShown;
import static org.fossflight.efb.objectmodels.assertion.ChecklistModelAssert.assertChecklistHasItemCount;
import static org.fossflight.efb.objectmodels.assertion.ChecklistModelAssert.assertDoneAllIconIsShown;
import static org.fossflight.efb.objectmodels.assertion.ChecklistModelAssert.assertGoToPreviouslySkippedButtonIsDisplayedAsDisabled;
import static org.fossflight.efb.objectmodels.assertion.ChecklistModelAssert.assertGoToPreviouslySkippedButtonIsDisplayedAsEnabled;
import static org.fossflight.efb.objectmodels.assertion.ChecklistModelAssert.assertPlayChecklistIconIsShown;
import static org.fossflight.efb.objectmodels.assertion.ChecklistModelAssert.assertRevisitIconIsShown;
import static org.fossflight.efb.objectmodels.assertion.ChecklistModelAssert.assertTitleBarShowsText;
import static org.fossflight.efb.objectmodels.assertion.RecyclerViewAssertions.cardAtPositionIsKindaCenteredInLandscapeOrientation;
import static org.fossflight.efb.objectmodels.assertion.RecyclerViewAssertions.cardAtPositionIsKindaCenteredInPortraitOrientation;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.core.Is.is;

public class ChecklistFunctionalTest {

    @Rule
    public ActivityTestRule<MainActivity> mainActivityTestRule =
            new ActivityTestRule<>(MainActivity.class);

    private final UiDevice device = UiDevice.getInstance(getInstrumentation());

    @Before
    public void setup() {

        while (numberOfCardsInTheRecyclerView() > 0) {
            deleteCardWithText("NEXFC1");
        }

        openOptionsMenu();
        tapAddAircraftText();
        tapImportExampleAircraftItemInDialog();
        tapCardWithText("NEXFC1");
        scrollViewPagerToFirstPage();
    }

    @After
    public void teardown() {
        pressBack();
        deleteCardWithText("NEXFC1");
    }

    @Test
    public void shouldPlayTwoChecklistsWithRotations() throws RemoteException, InterruptedException {
        tapChecklistWithText("Preflight");
        playChecklist();

        int cardNumber = 1;
        assertCardAtPositionIsHighlighted(cardNumber);

        int indexOfHighlightedItem = markNextItemsAsCompleted(cardNumber, 4);

        device.setOrientationLeft();
        sleep(1500);
        assertCardAtPositionIsHighlighted(indexOfHighlightedItem);

        indexOfHighlightedItem = markNextItemsAsCompletedAndAssertCenteredInLandscapeOrientation(indexOfHighlightedItem, 21);

        assertCardAtPositionIsSectionHeaderWithText(indexOfHighlightedItem++, "Exterior Preflight");

        device.setOrientationNatural();
        sleep(1500);
        assertCardAtPositionIsHighlighted(indexOfHighlightedItem);

        indexOfHighlightedItem = markNextItemsAsCompletedAndAssertCenteredInPortraitOrientation(indexOfHighlightedItem, 61);

        markNextItemsAsCompleted(indexOfHighlightedItem, 63);

        // mark last item as completed to complete the checklist
        markItemAsCompleted();

        tapYesToStartNextChecklist();

        assertTitleBarShowsText("Before Starting Engine");

        assertPlayChecklistIconIsShown();

        int numberOfCards = numberOfCardsInTheRecyclerView();
        for (int position = 0; position < numberOfCards; position++) {
            assertCardAtPositionIsPrechecked(position);
        }

        playChecklist();
        markItemAsCompleted();
        markItemAsCompleted();

        tapNoToDismissDialog();

        device.setOrientationLeft();
        sleep(1000);
        assertDoneAllIconIsShown();

        device.setOrientationNatural();
        sleep(1000);
        assertDoneAllIconIsShown();

        pressBack();
    }

    @Test
    public void shouldPlayAChecklistWithAHeaderProperly() {
        tapChecklistWithText("Climb Out");
        String checklistItemName = "Mixture";
        convertItemWithNameToType(checklistItemName, "SECTION HEADER");

        playChecklist();

        int cardNumber = 0;
        assertCardAtPositionIsHighlighted(cardNumber);

        markItemAsCompleted();
        assertCardAtPositionIsShownAsCompleted(cardNumber++);
        assertCardAtPositionIsHighlighted(cardNumber);

        markItemAsCompleted();
        assertCardAtPositionIsShownAsCompleted(cardNumber++);
        cardNumber++;       // the card at this position is a section header and should be skipped
        assertCardAtPositionIsHighlighted(cardNumber);

        markItemAsCompleted();
        assertCardAtPositionIsShownAsCompleted(cardNumber++);
        assertCardAtPositionIsHighlighted(cardNumber);

        markItemAsCompleted();
        assertCardAtPositionIsShownAsCompleted(cardNumber++);
        assertCardAtPositionIsHighlighted(cardNumber);

        markItemAsCompleted();

        tapNoToDismissDialog();

        openOptionsMenu();
        tapResetChecklistText();

        convertItemWithNameToType(checklistItemName, "NORMAL");

        pressBack();
    }

    @Test
    public void shouldRevisitSkippedItemsWhilePlayingAChecklistInPortraitOrientation() {
        tapChecklistWithText("Run Up");
        playChecklist();

        markItemAsCompleted();
        assertGoToPreviouslySkippedButtonIsDisplayedAsDisabled();

        skipItem();
        assertGoToPreviouslySkippedButtonIsDisplayedAsEnabled();

        assertCardAtPositionIsShownAsSkipped(1);

        markItemAsCompleted();
        skipItem();
        assertCardAtPositionIsShownAsSkipped(3);

        markItemAsCompleted();
        assertCardAtPositionIsHighlighted(5);

        tapGoToPreviouslySkippedButton();
        assertCardAtPositionIsHighlighted(3);
        onTheListView().check(cardAtPositionIsKindaCenteredInPortraitOrientation(3));
        assertCardAtPositionIsPrechecked(5);

        tapGoToPreviouslySkippedButton();
        assertCardAtPositionIsHighlighted(1);
        // this card is not asserted on being centered b/c its at the top of the checklist.
        // cards at the top and bottom don't get centered
        assertCardAtPositionIsShownAsSkipped(3);

        tapGoToPreviouslySkippedButton();
        assertCardAtPositionIsHighlighted(1);
        // this card is not asserted on being centered b/c its at the top of the checklist.

        tapGoToPreviouslySkippedButton();
        assertCardAtPositionIsHighlighted(1);
        // this card is not asserted on being centered b/c its at the top of the checklist.

        skipItem();
        assertCardAtPositionIsShownAsSkipped(1);

        markItemAsCompleted();
        assertCardAtPositionIsHighlighted(5);

        tapGoToPreviouslySkippedButton();
        markItemAsCompleted();
        assertCardAtPositionIsHighlighted(5);
        onTheListView().check(cardAtPositionIsKindaCenteredInPortraitOrientation(5));

        markItemAsCompleted();
        markItemAsCompleted();
        markItemAsCompleted();
        markItemAsCompleted();
        markItemAsCompleted();
        skipItem();
        markItemAsCompleted();
        markItemAsCompleted();

        tapNoToDismissDialog();

        assertDoneAllIconIsShown();
        assertRevisitIconIsShown();

        tapGoToPreviouslySkippedButton();

        assertCheckItemIconIsShown();

        markItemAsCompleted();
        tapNoToDismissDialog();

        assertDoneAllIconIsShown();

        pressBack();
    }

    @Test
    public void shouldRevisitSkippedItemsWhilePlayingAChecklistInLandscapeOrientation() throws RemoteException {
        UiDevice device = UiDevice.getInstance(getInstrumentation());
        device.setOrientationLeft();

        tapChecklistWithText("Run Up");
        playChecklist();

        markItemAsCompleted();
        assertGoToPreviouslySkippedButtonIsDisplayedAsDisabled();

        skipItem();
        assertGoToPreviouslySkippedButtonIsDisplayedAsEnabled();

        assertCardAtPositionIsShownAsSkipped(1);

        markItemAsCompleted();
        skipItem();
        assertCardAtPositionIsShownAsSkipped(3);

        markItemAsCompleted();
        assertCardAtPositionIsHighlighted(5);

        tapGoToPreviouslySkippedButton();
        assertCardAtPositionIsHighlighted(3);
        onTheListView().check(cardAtPositionIsKindaCenteredInLandscapeOrientation(3));
        assertCardAtPositionIsPrechecked(5);

        tapGoToPreviouslySkippedButton();
        assertCardAtPositionIsHighlighted(1);
        // this card is not asserted on being centered b/c its at the top of the checklist.
        // cards at the top and bottom don't get centered
        assertCardAtPositionIsShownAsSkipped(3);

        tapGoToPreviouslySkippedButton();
        assertCardAtPositionIsHighlighted(1);
        // this card is not asserted on being centered b/c its at the top of the checklist.

        tapGoToPreviouslySkippedButton();
        assertCardAtPositionIsHighlighted(1);
        // this card is not asserted on being centered b/c its at the top of the checklist.

        skipItem();
        assertCardAtPositionIsShownAsSkipped(1);

        markItemAsCompleted();
        assertCardAtPositionIsHighlighted(5);

        tapGoToPreviouslySkippedButton();
        markItemAsCompleted();
        assertCardAtPositionIsHighlighted(5);
        onTheListView().check(cardAtPositionIsKindaCenteredInLandscapeOrientation(5));

        markItemAsCompleted();
        markItemAsCompleted();
        markItemAsCompleted();
        markItemAsCompleted();
        skipItem();
        markItemAsCompleted();
        markItemAsCompleted();
        markItemAsCompleted();

        tapNoToDismissDialog();

        assertDoneAllIconIsShown();
        assertRevisitIconIsShown();

        tapGoToPreviouslySkippedButton();

        assertCheckItemIconIsShown();

        markItemAsCompleted();
        tapNoToDismissDialog();

        assertDoneAllIconIsShown();

        pressBack();
        device.setOrientationNatural();
    }

    @Test
    public void shouldReplaySkippedItemAfterTappingYesOnTheChecklistCompleteWithSkippedItemsDialog() {
        tapChecklistWithText("Climb Out");
        playChecklist();

        markItemAsCompleted();
        skipItem();
        assertCardAtPositionIsShownAsSkipped(1);

        markItemAsCompleted();
        skipItem();
        assertCardAtPositionIsShownAsSkipped(3);

        markItemAsCompleted();
        markItemAsCompleted();

        tapYesToReplaySkippedItems();

        assertCardAtPositionIsHighlighted(1);
        markItemAsCompleted();
        assertCardAtPositionIsHighlighted(3);
        markItemAsCompleted();

        tapNoToDismissDialog();
        pressBack();
    }

    @Test
    public void shouldPlayTheNextChecklistAfterTappingNoOnTheChecklistCompleteWithSkippedItemsDialogAndTappingTheNextChecklistButton() throws RemoteException, InterruptedException {
        tapChecklistWithText("Cruising");
        playChecklist();

        markItemAsCompleted();
        skipItem();
        assertCardAtPositionIsShownAsSkipped(1);

        markItemAsCompleted();
        skipItem();
        assertCardAtPositionIsShownAsSkipped(3);

        markItemAsCompleted();

        tapNoToDismissDialog();

        device.setOrientationLeft();
        sleep(1000);
        assertDoneAllIconIsShown();

        device.setOrientationNatural();
        sleep(1000);
        assertDoneAllIconIsShown();

        tapNextChecklistButton();

        assertTitleBarShowsText("Let Down");

        assertPlayChecklistIconIsShown();

        int numberOfCards = numberOfCardsInTheRecyclerView();
        for (int position = 0; position < numberOfCards; position++) {
            assertCardAtPositionIsPrechecked(position);
        }

        playChecklist();
        markItemAsCompleted();
        markItemAsCompleted();
        markItemAsCompleted();
        markItemAsCompleted();
        markItemAsCompleted();

        tapNoToDismissDialog();
        pressBack();
    }

    @Test
    public void shouldResetAChecklistUsingTheOptionsMenu() {
        tapChecklistWithText("Climb Out");
        playChecklist();

        markItemAsCompleted();
        markItemAsCompleted();
        markItemAsCompleted();

        openOptionsMenu();
        tapResetChecklistText();

        assertPlayChecklistIconIsShown();

        int itemCount = numberOfCardsInTheRecyclerView();
        for (int i = 0; i < itemCount; i++) {
            scrollRecyclerViewToPosition(i);
            assertCardAtPositionIsPrechecked(i);
        }
        pressBack();
    }

    @Test
    public void shouldDeleteAChecklistItem() {
        tapChecklistWithText("Climb Out");
        assertChecklistHasItemCount(6);
        deleteCardWithText("Trim");
        assertChecklistHasItemCount(5);
        pressBack();
    }

    @Test
    public void shouldContinuePlayingAChecklistAfterDeviceRotations() throws RemoteException {
        tapChecklistWithText("Run Up");

        playChecklist();

        int cardNumber = 0;
        assertCardAtPositionIsHighlighted(cardNumber);

        markItemAsCompleted();
        assertCardAtPositionIsShownAsCompleted(cardNumber++);
        assertCardAtPositionIsHighlighted(cardNumber);

        markItemAsCompleted();
        assertCardAtPositionIsShownAsCompleted(cardNumber++);
        assertCardAtPositionIsHighlighted(cardNumber);

        UiDevice device = UiDevice.getInstance(getInstrumentation());
        device.setOrientationLeft();
        assertCardAtPositionIsHighlighted(cardNumber);

        markItemAsCompleted();
        assertCardAtPositionIsShownAsCompleted(cardNumber++);
        assertCardAtPositionIsHighlighted(cardNumber);
        onTheListView().check(cardAtPositionIsKindaCenteredInLandscapeOrientation(cardNumber));

        markItemAsCompleted();
        assertCardAtPositionIsShownAsCompleted(cardNumber++);
        assertCardAtPositionIsHighlighted(cardNumber);
        onTheListView().check(cardAtPositionIsKindaCenteredInLandscapeOrientation(cardNumber));

        markItemAsCompleted();
        assertCardAtPositionIsShownAsCompleted(cardNumber++);
        assertCardAtPositionIsHighlighted(cardNumber);
        onTheListView().check(cardAtPositionIsKindaCenteredInLandscapeOrientation(cardNumber));

        markItemAsCompleted();
        assertCardAtPositionIsShownAsCompleted(cardNumber++);
        assertCardAtPositionIsHighlighted(cardNumber);
        onTheListView().check(cardAtPositionIsKindaCenteredInLandscapeOrientation(cardNumber));

        device.setOrientationNatural();
        assertCardAtPositionIsHighlighted(cardNumber);

        markItemAsCompleted();
        assertCardAtPositionIsShownAsCompleted(cardNumber++);
        assertCardAtPositionIsHighlighted(cardNumber);

        markItemAsCompleted();
        markItemAsCompleted();
        markItemAsCompleted();
        markItemAsCompleted();
        markItemAsCompleted();
        markItemAsCompleted();

        tapNoToDismissDialog();
        pressBack();
    }

    @Test
    public void shouldRecenterHighlightedItemWhenItIsScrolledOffScreenAndDeviceIsRotated() throws RemoteException, InterruptedException {

        tapChecklistWithText("Run Up");
        playChecklist();

        markItemAsCompleted();
        markItemAsCompleted();
        markItemAsCompleted();
        markItemAsCompleted();
        markItemAsCompleted();

        scrollRecyclerViewToPosition(11);

        device.setOrientationLeft();
        sleep(1500);
        assertCardAtPositionIsHighlighted(5);

        markItemAsCompleted();
        markItemAsCompleted();
        markItemAsCompleted();
        markItemAsCompleted();

        scrollRecyclerViewToPosition(2);

        device.setOrientationNatural();
        sleep(1500);
        assertCardAtPositionIsHighlighted(9);

        markItemAsCompleted();
        markItemAsCompleted();
        markItemAsCompleted();
        markItemAsCompleted();

        tapNoToDismissDialog();
        pressBack();
    }

    private void convertItemWithNameToType(String checklistItemName, String sectionHeaderType) {
        editCardWithText(checklistItemName);
        onView(withId(R.id.checklist_item_type_spinner))
                .perform(click());
        onData(allOf(is(instanceOf(String.class)), is(sectionHeaderType)))
                .perform(click());
        tapSaveButton();
    }

    private int markNextItemsAsCompleted(int startIndex, int itemsToMarkAsCompleted) {
        int currentIndex = startIndex;
        while (currentIndex < itemsToMarkAsCompleted) {
            assertCardAtPositionIsHighlighted(currentIndex);
            markItemAsCompleted();
            assertCardAtPositionIsShownAsCompleted(currentIndex++);
            assertCheckItemIconIsShown();
        }
        return currentIndex;
    }

    private int markNextItemsAsCompletedAndAssertCenteredInPortraitOrientation(int startIndex, int itemsToMarkAsCompleted) {
        int currentIndex = startIndex;
        while (currentIndex < itemsToMarkAsCompleted) {
            assertCardAtPositionIsHighlighted(currentIndex);
            onTheListView().check(cardAtPositionIsKindaCenteredInPortraitOrientation(currentIndex));
            markItemAsCompleted();
            assertCardAtPositionIsShownAsCompleted(currentIndex++);
        }
        return currentIndex;
    }

    private int markNextItemsAsCompletedAndAssertCenteredInLandscapeOrientation(int startIndex, int itemsToMarkAsCompleted) {
        int currentIndex = startIndex;
        while (currentIndex < itemsToMarkAsCompleted) {
            assertCardAtPositionIsHighlighted(currentIndex);
            onTheListView().check(cardAtPositionIsKindaCenteredInLandscapeOrientation(currentIndex));
            markItemAsCompleted();
            assertCardAtPositionIsShownAsCompleted(currentIndex++);
        }
        return currentIndex;
    }
}
