package org.fossflight.efb.checklist;

import org.fossflight.efb.MainActivity;
import org.fossflight.efb.R;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import androidx.test.rule.ActivityTestRule;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.swipeRight;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.ViewPagerActions.scrollLeft;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static java.lang.Thread.sleep;
import static org.fossflight.efb.objectmodels.ChecklistModel.openOptionsMenu;
import static org.fossflight.efb.objectmodels.FossFlightActivityModel.tapCancelButton;
import static org.fossflight.efb.objectmodels.FossFlightActivityModel.tapSaveButton;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.deleteCardWithText;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.editCardWithText;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.numberOfCardsInTheRecyclerView;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.scrollRecyclerViewToPosition;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.tapCardWithText;
import static org.fossflight.efb.objectmodels.ViewPagerActions.scrollTabListToTheRight;
import static org.fossflight.efb.objectmodels.action.AddAircraftDialogAction.tapImportExampleAircraftItemInDialog;
import static org.fossflight.efb.objectmodels.action.AircraftListAction.tapAddAircraftText;
import static org.fossflight.efb.objectmodels.action.ChecklistAction.tapAddChecklistItemText;
import static org.fossflight.efb.objectmodels.action.ChecklistCollectionAction.scrollViewPagerToFirstPage;
import static org.fossflight.efb.objectmodels.action.ChecklistCollectionAction.tapChecklistWithText;
import static org.fossflight.efb.objectmodels.action.EditChecklistAction.fillInNameOfChecklist;
import static org.fossflight.efb.objectmodels.action.EditChecklistAction.tapAddChecklistText;
import static org.fossflight.efb.objectmodels.action.EditChecklistAction.updateChecklistNameTo;
import static org.fossflight.efb.objectmodels.action.EditChecklistItemAction.addChecklistItem;
import static org.fossflight.efb.objectmodels.assertion.ChecklistCollectionAssert.assertCardAtPositionHasChecklistName;
import static org.fossflight.efb.objectmodels.assertion.ChecklistCollectionAssert.assertPlayChecklistButtonIsDisabled;
import static org.fossflight.efb.objectmodels.assertion.ChecklistCollectionAssert.assertPlayChecklistButtonIsEnabled;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.core.Is.is;

public class EditChecklistFunctionalTest {

    @Rule
    public ActivityTestRule<MainActivity> mainActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void setup() {

        while (numberOfCardsInTheRecyclerView() > 0) {
            deleteCardWithText("NEXFC1");
        }

        openOptionsMenu();
        tapAddAircraftText();
        tapImportExampleAircraftItemInDialog();
        tapCardWithText("NEXFC1");
    }

    @After
    public void teardown() {
        pressBack();
        deleteCardWithText("NEXFC1");
    }

    @Test
    public void shouldAddAndEditAChecklist() throws InterruptedException {
        int itemCountBeforeAdding = numberOfCardsInTheRecyclerView();

        openOptionsMenu();
        tapAddChecklistText();

        onView(withText("Add Checklist")).check(matches(isDisplayed()));

        String newChecklistName = "a new checklist";
        fillInNameOfChecklist(newChecklistName);
        tapSaveButton();

        int itemCountAfterAdding = numberOfCardsInTheRecyclerView();

        assertThat(itemCountAfterAdding - itemCountBeforeAdding, is(1));

        scrollViewPagerToFirstPage();

        int positionOfNewChecklistOne = itemCountAfterAdding - 1;
        scrollRecyclerViewToPosition(positionOfNewChecklistOne);

        tapChecklistWithText(newChecklistName);
        assertThat(numberOfCardsInTheRecyclerView(), is(0));
        assertPlayChecklistButtonIsDisabled();

        openOptionsMenu();
        tapAddChecklistItemText();

        String checklistItemName = "a new checklist item";
        String checklistItemAction = "a new checklist item action";
        addChecklistItem(checklistItemName, checklistItemAction);
        tapSaveButton();

        assertThat(numberOfCardsInTheRecyclerView(), is(1));
        assertPlayChecklistButtonIsEnabled();
        pressBack();

        sleep(200);

        editCardWithText(newChecklistName);

        onView(withText("Edit Checklist")).check(matches(isDisplayed()));

        String updatedChecklistName = "the edited checklist";
        updateChecklistNameTo(updatedChecklistName);
        tapSaveButton();

        scrollRecyclerViewToPosition(positionOfNewChecklistOne);
        assertCardAtPositionHasChecklistName(positionOfNewChecklistOne, updatedChecklistName);

        tapChecklistWithText(updatedChecklistName);

        assertThat(numberOfCardsInTheRecyclerView(), is(1));
        deleteCardWithText(checklistItemName);
        onView(withId(com.google.android.material.R.id.snackbar_text)).perform(swipeRight());

        assertPlayChecklistButtonIsDisabled();
        pressBack();
    }

    @Test
    public void shouldCancelAddingAChecklist() {
        int itemCountBeforeAdding = numberOfCardsInTheRecyclerView();

        openOptionsMenu();
        tapAddChecklistText();

        fillInNameOfChecklist("a new checklist");

        tapCancelButton();

        int itemCountAfterAdding = numberOfCardsInTheRecyclerView();

        assertThat(itemCountAfterAdding - itemCountBeforeAdding, is(0));
    }

    @Test
    public void shouldMoveAChecklistFromOneCollectionToAnother() {
        int checklistCountInStandardChecklistCollectionBeforeEdit = numberOfCardsInTheRecyclerView();

        scrollTabListToTheRight();
        scrollTabListToTheRight();
        scrollTabListToTheRight();
        int checklistCountInEmergencyChecklistCollectionBeforeEdit = numberOfCardsInTheRecyclerView();

        onView(withId(R.id.pager_fragment)).perform(scrollLeft());
        onView(withId(R.id.pager_fragment)).perform(scrollLeft());
        onView(withId(R.id.pager_fragment)).perform(scrollLeft());

        String checklistToMove = "Preflight";
        editCardWithText(checklistToMove);

        onView(withId(R.id.checklist_type_spinner)).perform(click());
        onData(allOf(is(instanceOf(String.class)), is("Emergency Checklist"))).perform(click());

        tapSaveButton();

        int checklistCountInStandardChecklistCollectionAfterEdit = numberOfCardsInTheRecyclerView();
        assertThat(
                checklistCountInStandardChecklistCollectionAfterEdit,
                is(checklistCountInStandardChecklistCollectionBeforeEdit - 1)
        );

        scrollTabListToTheRight();
        scrollTabListToTheRight();
        scrollTabListToTheRight();
        int checklistCountInEmergencyChecklistCollectionAfterEdit = numberOfCardsInTheRecyclerView();
        assertThat(
                checklistCountInEmergencyChecklistCollectionAfterEdit,
                is(checklistCountInEmergencyChecklistCollectionBeforeEdit + 1)
        );

        assertCardAtPositionHasChecklistName(4, checklistToMove);

        tapChecklistWithText(checklistToMove);
        assertThat(numberOfCardsInTheRecyclerView(), is(64));
        pressBack();
    }
}
