package org.fossflight.efb.checklist;

import org.fossflight.efb.MainActivity;
import org.fossflight.efb.R;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import androidx.test.rule.ActivityTestRule;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.fossflight.efb.matchers.CustomTestMatchers.hasTextInputLayoutHintText;
import static org.fossflight.efb.objectmodels.ChecklistModel.openOptionsMenu;
import static org.fossflight.efb.objectmodels.FossFlightActivityModel.tapCancelButton;
import static org.fossflight.efb.objectmodels.FossFlightActivityModel.tapSaveButton;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.deleteCardWithText;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.editCardWithText;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.numberOfCardsInTheRecyclerView;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.tapCardWithText;
import static org.fossflight.efb.objectmodels.action.AddAircraftDialogAction.tapImportExampleAircraftItemInDialog;
import static org.fossflight.efb.objectmodels.action.AircraftListAction.tapAddAircraftText;
import static org.fossflight.efb.objectmodels.action.ChecklistAction.tapAddChecklistItemText;
import static org.fossflight.efb.objectmodels.action.ChecklistCollectionAction.tapChecklistWithText;
import static org.fossflight.efb.objectmodels.action.EditChecklistItemAction.addChecklistItem;
import static org.fossflight.efb.objectmodels.action.EditChecklistItemAction.setChecklistItemTypeToSectionHeader;
import static org.fossflight.efb.objectmodels.action.EditChecklistItemAction.updateChecklistItemActionTo;
import static org.fossflight.efb.objectmodels.action.EditChecklistItemAction.updateChecklistItemNameTo;
import static org.fossflight.efb.objectmodels.assertion.ChecklistModelAssert.assertCardAtPositionIsSectionHeaderWithText;
import static org.fossflight.efb.objectmodels.assertion.EditChecklistItemModelAssert.assertCardAtPositionIsChecklistItem;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class EditChecklistItemFunctionalTest {
    @Rule
    public ActivityTestRule<MainActivity> mainActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void setup() {
        openOptionsMenu();
        tapAddAircraftText();
        tapImportExampleAircraftItemInDialog();
        tapCardWithText("NEXFC1");
        tapChecklistWithText("Climb Out");
    }

    @After
    public void teardown(){
        pressBack();
        pressBack();
        deleteCardWithText("NEXFC1");
    }

    @Test
    public void shouldAddAndEditAChecklistItem() throws InterruptedException {

        int itemCountBeforeAdding = numberOfCardsInTheRecyclerView();

        openOptionsMenu();
        tapAddChecklistItemText();

        onView(withText("Add Checklist Item")).check(matches(isDisplayed()));

        tapSaveButton();

        onView(withId(R.id.input_layout_name_one))
                .check(matches(hasTextInputLayoutHintText("The Checklist Item Name is Required")));

        String checklistItemName = "a new checklist item";
        String checklistItemAction = "a new checklist item action";
        addChecklistItem(checklistItemName, checklistItemAction);

        tapSaveButton();

        int itemCountAfterAdding = numberOfCardsInTheRecyclerView();
        assertThat(itemCountAfterAdding - itemCountBeforeAdding, is(1));
        assertCardAtPositionIsChecklistItem(itemCountAfterAdding - 1, checklistItemName, checklistItemAction);

        editCardWithText(checklistItemName);

        onView(withText("Edit Checklist Item")).check(matches(isDisplayed()));

        String newChecklistItemName = "the new item name";
        String newChecklistItemAction = "the new action";

        updateChecklistItemNameTo(newChecklistItemName);
        updateChecklistItemActionTo(newChecklistItemAction);
        tapSaveButton();

        assertCardAtPositionIsChecklistItem(itemCountAfterAdding - 1, newChecklistItemName, newChecklistItemAction);
    }

    @Test
    public void shouldSaveAChecklistItemAsASectionHeaderWhenEditingAChecklistItem() {
        editCardWithText("Mixture");
        setChecklistItemTypeToSectionHeader();
        tapSaveButton();

        assertCardAtPositionIsSectionHeaderWithText(2, "Mixture");
    }

    @Test
    public void shouldCancelAddingAChecklistItem() {

        int itemCountBeforeAdding = numberOfCardsInTheRecyclerView();

        openOptionsMenu();
        tapAddChecklistItemText();

        String checklistItemName = "a new checklist item";
        addChecklistItem(checklistItemName, "");

        tapCancelButton();

        int itemCountAfterAdding = numberOfCardsInTheRecyclerView();

        assertThat(itemCountAfterAdding - itemCountBeforeAdding, is(0));
    }
}
