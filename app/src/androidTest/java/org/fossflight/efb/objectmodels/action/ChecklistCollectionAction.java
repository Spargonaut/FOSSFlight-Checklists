package org.fossflight.efb.objectmodels.action;

import org.fossflight.efb.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItem;
import static androidx.test.espresso.contrib.ViewPagerActions.scrollToFirst;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.onTheListView;

public class ChecklistCollectionAction {

    public static void tapChecklistWithText(String text) {
        onTheListView().perform(actionOnItem(hasDescendant(withText(text)), click()));
    }

    public static void scrollViewPagerToFirstPage() {
        onView(withId(R.id.pager_fragment)).perform(scrollToFirst());
    }
}
