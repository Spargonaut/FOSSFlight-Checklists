package org.fossflight.efb.objectmodels.action;

import static androidx.test.espresso.action.ViewActions.clearText;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static org.fossflight.efb.objectmodels.EditChecklistItemModel.onChecklistItemActionInput;
import static org.fossflight.efb.objectmodels.EditChecklistItemModel.onChecklistItemNameInput;
import static org.fossflight.efb.objectmodels.EditChecklistItemModel.onChecklistItemTypeSectionHeader;
import static org.fossflight.efb.objectmodels.EditChecklistItemModel.onChecklistItemTypeSpinner;

public class EditChecklistItemAction {
    public static void addChecklistItem(String itemName, String itemAction) {
        onChecklistItemNameInput().perform(typeText(itemName), closeSoftKeyboard());
        onChecklistItemActionInput().perform(typeText(itemAction), closeSoftKeyboard());
    }

    public static void updateChecklistItemNameTo(String name) {
        onChecklistItemNameInput().perform(clearText(), typeText(name), closeSoftKeyboard());
    }

    public static void updateChecklistItemActionTo(String action) {
        onChecklistItemActionInput().perform(clearText(), typeText(action), closeSoftKeyboard());
    }

    public static void setChecklistItemTypeToSectionHeader() {
        onChecklistItemTypeSpinner().perform(click());
        onChecklistItemTypeSectionHeader().perform(click());
    }
}
