package org.fossflight.efb.objectmodels;

import org.fossflight.efb.R;

import androidx.test.espresso.ViewInteraction;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

public class EditChecklistModel {

    public static ViewInteraction onChecklistNameInput() {
        return onView(withId(R.id.checklist_name_edit));
    }

    public static ViewInteraction onAddChecklistText() {
        return onView(withText(R.string.add_checklist));
    }
}
