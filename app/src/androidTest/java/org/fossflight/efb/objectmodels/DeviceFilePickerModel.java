package org.fossflight.efb.objectmodels;


import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiObjectNotFoundException;
import androidx.test.uiautomator.UiScrollable;
import androidx.test.uiautomator.UiSelector;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;


public class DeviceFilePickerModel {

    public static void importFileFromInternalStorage(String dirName,
            String filename) {
        UiDevice device = UiDevice.getInstance(getInstrumentation());
        showInternalStorage(device);
        selectInternalStorage(device);
        openDirWithName(dirName);
        selectFileWithName(filename);
    }

    public static void cancelFileSelection() {
        UiDevice device = UiDevice.getInstance(getInstrumentation());
        UiObject titleBar = device.findObject(new UiSelector().textMatches("FOSSFlight - Checklists"));

        while(!titleBar.exists()) {
            device.pressBack();
        }
    }

    private static void showInternalStorage(UiDevice device) {
        UiObject menuButton = device.findObject(
                new UiSelector().descriptionContains("More options"));
        try {
            menuButton.click();
        } catch (UiObjectNotFoundException e) {
            throw new RuntimeException("Unable to click menu button", e);
        }
        UiObject showInternalStorageOption = device.findObject(
                new UiSelector().text("Show internal storage"));
        UiObject hideInternalStorageOption = device.findObject(
                new UiSelector().text("Hide internal storage"));

        try {
            // The first menu button click may only have dismissed the storage options pane
            if (!(showInternalStorageOption.exists() || hideInternalStorageOption.exists())) {
                menuButton.click();
            }
            if (hideInternalStorageOption.exists()) {
                device.pressBack();
            } else {
                showInternalStorageOption.click();
            }
        } catch (UiObjectNotFoundException e) {
            throw new RuntimeException("Unable to show internal storage", e);
        }
    }

    private static void selectInternalStorage(UiDevice device) {
        UiObject selectStorageButton = device.findObject(
                new UiSelector().description("Show roots"));

        try {
            selectStorageButton.click();
        } catch (UiObjectNotFoundException e) {
            throw new RuntimeException("Unable to show storage roots", e);
        }

        UiObject internalStorage = device.findObject(
                new UiSelector().resourceId("com.android.documentsui:id/roots_list")
                                .childSelector(new UiSelector().index(3)));

        try {
            internalStorage.click();
        } catch (UiObjectNotFoundException e) {
            throw new RuntimeException("Unable to select internal storage", e);
        }
    }

    private static void openDirWithName(String dirName) {
        UiScrollable directoryScrollable = new UiScrollable(
                new UiSelector().resourceId("com.android.documentsui:id/dir_list"));

        UiObject dirToSelect;
        try {
            dirToSelect = directoryScrollable.getChildByText(
                    new UiSelector().className("android.widget.LinearLayout"), dirName);
        } catch (UiObjectNotFoundException e) {
            throw new RuntimeException("Unable to find directory " + dirName, e);
        }

        try {
            dirToSelect.click();
        } catch (NullPointerException | UiObjectNotFoundException e) {
            throw new RuntimeException("Unable to open directory " + dirName, e);
        }
    }

    private static void selectFileWithName(String filename) {
        UiScrollable directoryScrollable = new UiScrollable(
                new UiSelector().resourceId("com.android.documentsui:id/dir_list"));

        UiObject file;
        try {
            file = directoryScrollable.getChildByText(
                    new UiSelector().className("android.widget.TextView"), filename);
        } catch (UiObjectNotFoundException e) {
            throw new RuntimeException("Unable to find file " + filename, e);
        }

        try {
            file.click();
        } catch (NullPointerException | UiObjectNotFoundException e) {
            throw new RuntimeException("Unable to select file " + filename, e);
        }
    }
}
