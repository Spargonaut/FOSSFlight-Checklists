package org.fossflight.efb.objectmodels;

import android.view.View;

import org.fossflight.efb.R;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.ViewInteraction;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.longClick;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItem;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.allOf;

public class RecyclerViewActions {
    public static ViewInteraction onTheListView() {
        return onView(allOf(withId(R.id.list), isCompletelyDisplayed()));
    }

    public static void tapCardWithText(String text) {
        onTheListView().perform(actionOnItem(hasDescendant(withText(text)), click()));
    }

    public static void longClickCardWithText(String text) {
        onTheListView().perform(actionOnItem(hasDescendant(withText(text)), longClick()));
    }

    public static void deleteCardWithText(String text) {
        onTheListView().perform(actionOnItem(hasDescendant(withText(text)), longClick()));
        onView(withText(R.string.delete)).perform(click());
    }

    public static void editCardWithText(String text) {
        onTheListView().perform(actionOnItem(hasDescendant(withText(text)), longClick()));
        onView(withText(R.string.edit)).perform(click());
    }

    public static void scrollRecyclerViewToPosition(int position) {
        onTheListView().perform(androidx.test.espresso.contrib.RecyclerViewActions.scrollToPosition(position));
    }

    public static int numberOfCardsInTheRecyclerView() {
        final int[] COUNT = {0};
        Matcher<View> matcher = new TypeSafeMatcher<View>() {
            @Override
            protected boolean matchesSafely(View item) {
                COUNT[0] = ((RecyclerView) item).getAdapter().getItemCount();
                return true;
            }

            @Override
            public void describeTo(Description description) {
            }
        };
        onTheListView().check(matches(matcher));
        return COUNT[0];
    }
}
