package org.fossflight.efb.objectmodels.action;

import static androidx.test.espresso.action.ViewActions.click;
import static org.fossflight.efb.objectmodels.ImportChecklistTypeDialogModel.onFltPlanGoChecklistText;
import static org.fossflight.efb.objectmodels.ImportChecklistTypeDialogModel.onFossFLightChecklistText;

public class ImportChecklistTypeDialogAction {
    public static void tapFltPlanGoChecklistTextInDialog() {
        onFltPlanGoChecklistText().perform(click());
    }

    public static void tapFossFlightChecklistTextInDialog() {
        onFossFLightChecklistText().perform(click());
    }
}
