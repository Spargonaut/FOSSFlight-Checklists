package org.fossflight.efb.objectmodels;

import org.fossflight.efb.R;

import androidx.test.espresso.ViewInteraction;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

public class AddAircraftDialogModel {
    public static ViewInteraction onImportAircraftText() {
        return onView(withText(R.string.import_aircraft));
    }

    public static ViewInteraction onAddAircraftManuallyText() {
        return onView(withText(R.string.add_aircraft_manually));
    }

    public static ViewInteraction onImportExampleAircraftText() {
        return onView(withText(R.string.import_example_aircraft));
    }
}
