package org.fossflight.efb.objectmodels;

import org.fossflight.efb.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.contrib.ViewPagerActions.scrollLeft;
import static androidx.test.espresso.contrib.ViewPagerActions.scrollRight;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

public class ViewPagerActions {

    public static void scrollTabListToTheRight() {
        onView(withId(R.id.pager_fragment)).perform(scrollRight());
    }

    public static void scrollTabListToTheLeft() {
        onView(withId(R.id.pager_fragment)).perform(scrollLeft());
    }
}
