package org.fossflight.efb.objectmodels.action;

import org.fossflight.efb.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

public class AircraftListAction {
    public static void tapAddAircraftText() {
        onView(withText(R.string.add_aircraft)).perform(click());
    }

    public static void tapOK() {
        onView(withText("OK")).perform(click());
    }

    public static void tapExport() {
        onView(withText(R.string.export)).perform(click());
    }

    public static void tapExportAircraft() {
        onView(withText(R.string.export_aircraft)).perform(click());
    }
}
