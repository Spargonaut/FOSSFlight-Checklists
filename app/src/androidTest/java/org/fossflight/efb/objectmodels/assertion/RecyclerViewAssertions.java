package org.fossflight.efb.objectmodels.assertion;

import android.view.View;
import android.widget.TextView;

import org.fossflight.efb.R;
import org.junit.Assert;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.NoMatchingViewException;
import androidx.test.espresso.ViewAssertion;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


public class RecyclerViewAssertions {

    public static void assertTextIsDisplayed(String text) {
        onView(withText(text)).check(matches(isDisplayed()));
    }

    public static void assertTextIsNotDisplayed(String text) {
        onView(withText(text)).check(matches(not(isDisplayed())));
    }

    public static ViewAssertion hasItemCount(final int count) {
        return new ViewAssertion() {
            @Override
            public void check(View view, NoMatchingViewException noViewFoundException) {
                if (!(view instanceof RecyclerView)) {
                    throw noViewFoundException;
                }
                RecyclerView rv = (RecyclerView) view;
                assertThat(rv.getAdapter().getItemCount(), is(count));
            }
        };
    }

    public static ViewAssertion cardAtPositionHasBackgroundColor(final int index,
                                                                 final int backgroundColorId) {
        return new ViewAssertion() {
            @Override public void check(View view, NoMatchingViewException e) {
                if (!(view instanceof RecyclerView)) {
                    throw e;
                }
                RecyclerView rv = (RecyclerView) view;
                RecyclerView.ViewHolder viewHolder = rv.findViewHolderForAdapterPosition(index);
                int backgroundColor = ((CardView) viewHolder.itemView).getCardBackgroundColor().getDefaultColor();
                int expectedColor = ContextCompat.getColor(view.getContext(), backgroundColorId);
                Assert.assertThat(backgroundColor, is(expectedColor));
            }
        };

    }

    public static ViewAssertion cardAtPositionHasContentText(final int index, final String expectedText) {
        return new ViewAssertion() {
            @Override public void check(View view, NoMatchingViewException e) {
                if (!(view instanceof RecyclerView)) {
                    throw e;
                }
                RecyclerView.ViewHolder viewHolder =
                        ((RecyclerView) view).findViewHolderForAdapterPosition(index);
                String actualText =
                        ((TextView) viewHolder.itemView.findViewById(R.id.content)).getText().toString();
                Assert.assertThat(actualText, is(expectedText));
            }
        };
    }

    public static ViewAssertion cardAtPositionHasShortDescriptionText(final int index, final String expectedText) {
        return new ViewAssertion() {
            @Override public void check(View view, NoMatchingViewException e) {
                if (!(view instanceof RecyclerView)) {
                    throw e;
                }
                RecyclerView.ViewHolder viewHolder =
                        ((RecyclerView) view).findViewHolderForAdapterPosition(index);
                String actualText =
                        ((TextView) viewHolder.itemView.findViewById(R.id.short_description))
                                .getText().toString();
                Assert.assertThat(actualText, is(expectedText));
            }
        };
    }

    public static ViewAssertion cardAtPositionIsSectionHeaderWithText(final int index, final String expectedText) {
        return new ViewAssertion() {
            @Override public void check(View view, NoMatchingViewException e) {
                if (!(view instanceof RecyclerView)) {
                    throw e;
                }
                RecyclerView.ViewHolder viewHolder =
                        ((RecyclerView) view).findViewHolderForAdapterPosition(index);

                int backgroundColor =
                        ((CardView) viewHolder.itemView).getCardBackgroundColor().getDefaultColor();
                int sectionHeaderBackgroundColor =
                        view.getContext().getColor(R.color.section_header_background);
                assertThat(backgroundColor, is(sectionHeaderBackgroundColor));

                String actualText =
                        ((TextView) viewHolder.itemView.findViewById(R.id.content)).getText().toString();
                Assert.assertThat(actualText, is(expectedText));
            }
        };
    }

    public static ViewAssertion cardAtPositionIsKindaCenteredInPortraitOrientation(final int index) {
        return new ViewAssertion() {
            @Override public void check(View view, NoMatchingViewException e) {
                if (!(view instanceof RecyclerView)) {
                    throw e;
                }

                RecyclerView rv = (RecyclerView) view;
                RecyclerView.ViewHolder viewHolder = rv.findViewHolderForAdapterPosition(index);
                int[] itemViewOutLocation = new int[2];
                viewHolder.itemView.getLocationOnScreen(itemViewOutLocation);

                assertTrue(itemViewOutLocation[1] > 700);
                assertTrue(itemViewOutLocation[1] < 760);

            }
        };
    }

    public static ViewAssertion cardAtPositionIsKindaCenteredInLandscapeOrientation(final int index) {
        return new ViewAssertion() {
            @Override public void check(View view, NoMatchingViewException e) {
                if (!(view instanceof RecyclerView)) {
                    throw e;
                }

                RecyclerView rv = (RecyclerView) view;
                RecyclerView.ViewHolder viewHolder = rv.findViewHolderForAdapterPosition(index);
                int[] itemViewOutLocation = new int[2];
                viewHolder.itemView.getLocationOnScreen(itemViewOutLocation);

                assertTrue(itemViewOutLocation[1] > 400);
                assertTrue(itemViewOutLocation[1] < 450);

            }
        };
    }
}
