package org.fossflight.efb.objectmodels.assertion;

import static androidx.test.espresso.contrib.RecyclerViewActions.scrollToPosition;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.onTheListView;
import static org.fossflight.efb.objectmodels.assertion.RecyclerViewAssertions.cardAtPositionHasContentText;
import static org.fossflight.efb.objectmodels.assertion.RecyclerViewAssertions.cardAtPositionHasShortDescriptionText;

public class EditChecklistItemModelAssert {

    public static void assertCardAtPositionIsChecklistItem(int position, String checklistItemName, String checklistItemAction) {
        onTheListView().perform(scrollToPosition(position));
        onTheListView().check(cardAtPositionHasContentText(position, checklistItemName));
        onTheListView().check(cardAtPositionHasShortDescriptionText(position, checklistItemAction));
    }
}
