package org.fossflight.efb.objectmodels.assertion;

import static org.fossflight.efb.objectmodels.RecyclerViewActions.onTheListView;
import static org.fossflight.efb.objectmodels.assertion.RecyclerViewAssertions.cardAtPositionHasContentText;
import static org.fossflight.efb.objectmodels.assertion.RecyclerViewAssertions.cardAtPositionHasShortDescriptionText;

public class EditAircraftModelAssert {
    public static void assertCardAtPositionHasTailNumber(int position, String tailNumber) {
        onTheListView().check(cardAtPositionHasContentText(position, tailNumber));
    }

    public static void assertCardAtPositionHasContentDescription(int position, String description) {
        onTheListView().check(cardAtPositionHasShortDescriptionText(position, description));
    }
}
