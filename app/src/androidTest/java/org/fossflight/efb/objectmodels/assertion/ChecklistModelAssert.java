package org.fossflight.efb.objectmodels.assertion;

import org.fossflight.efb.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.fossflight.efb.matchers.CustomTestMatchers.withSVGDrawable;
import static org.fossflight.efb.matchers.CustomTestMatchers.withToolbarTitle;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.onTheListView;
import static org.fossflight.efb.objectmodels.assertion.RecyclerViewAssertions.cardAtPositionHasBackgroundColor;
import static org.fossflight.efb.objectmodels.assertion.RecyclerViewAssertions.cardAtPositionIsSectionHeaderWithText;

public class ChecklistModelAssert {
    public static void assertHasItemCount(int expectedCount) {
        onTheListView().check(RecyclerViewAssertions.hasItemCount(expectedCount));
    }

    public static void assertCardAtPositionIsHighlighted(int position) {
        onTheListView().check(cardAtPositionHasBackgroundColor(position, R.color.lightBlue));
    }

    public static void assertCardAtPositionIsShownAsCompleted(int position) {
        onTheListView().check(cardAtPositionHasBackgroundColor(position, R.color.lightGrey));
    }

    public static void assertCardAtPositionIsShownAsSkipped(int position) {
        onTheListView().check(cardAtPositionHasBackgroundColor(position, R.color.lightYellow));
    }

    public static void assertCardAtPositionIsPrechecked(int position) {
        onTheListView().check(cardAtPositionHasBackgroundColor(position, android.R.color.white));
    }

    public static void assertPlayChecklistIconIsShown() {
        onView(withId(R.id.play_button)).check(matches(withSVGDrawable(R.drawable.ic_play_arrow)));
    }

    public static void assertCheckItemIconIsShown() {
        onView(withId(R.id.play_button)).check(matches(withSVGDrawable(R.drawable.ic_done)));
    }

    public static void assertDoneAllIconIsShown() {
        onView(withId(R.id.play_button)).check(matches(withSVGDrawable(R.drawable.ic_done_all)));
    }

    public static void assertRevisitIconIsShown() {
        onView(withId(R.id.revisit_previously_skipped_button)).check(matches(withSVGDrawable(R.drawable.ic_skip_previous)));
    }

    public static void assertChecklistHasItemCount(int expectedCount) {
        onTheListView().check(RecyclerViewAssertions.hasItemCount(expectedCount));
    }

    public static void assertTitleBarShowsText(String text) {
        onView(withId(R.id.toolbar)).check(matches(withToolbarTitle(text)));
    }

    public static void assertGoToPreviouslySkippedButtonIsDisplayedAsDisabled() {
        onView(withId(R.id.revisit_previously_skipped_button)).check(matches(withSVGDrawable(R.drawable.ic_skip_previous)));
    }

    public static void assertGoToPreviouslySkippedButtonIsDisplayedAsEnabled() {
        onView(withId(R.id.revisit_previously_skipped_button)).check(matches(withSVGDrawable(R.drawable.ic_skip_previous)));
    }

    public static void assertCardAtPositionIsSectionHeaderWithText(int position, String sectionHeadline) {
        onTheListView().check(cardAtPositionIsSectionHeaderWithText(position, sectionHeadline));
    }
}
