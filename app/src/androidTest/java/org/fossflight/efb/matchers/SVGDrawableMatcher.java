package org.fossflight.efb.matchers;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

public class SVGDrawableMatcher extends TypeSafeMatcher<View> {

    private final int expectedId;
    private String expectedResourceName;

    SVGDrawableMatcher(int resourceId) {
        super(View.class);
        this.expectedId = resourceId;
    }

    @Override
    protected boolean matchesSafely(View target) {
        expectedResourceName = target.getContext().getResources().getResourceEntryName(expectedId);

        if (!(target instanceof ImageView)){
            return false;
        }
        ImageView actualImageView = (ImageView) target;

        if (expectedId < 0){
            return actualImageView.getDrawable() == null;
        }

        Drawable expectedDrawable = ContextCompat.getDrawable(target.getContext(), expectedId);
        if (expectedDrawable == null) {
            return false;
        }

        Bitmap expectedBitmap = createBitmapFromDrawable(expectedDrawable);
        Bitmap actualBitmap = createBitmapFromDrawable(actualImageView.getDrawable());

        return expectedBitmap.sameAs(actualBitmap);
    }

    @NonNull
    private Bitmap createBitmapFromDrawable(Drawable expectedDrawable) {
        Bitmap expectedBitmap = Bitmap.createBitmap(expectedDrawable.getIntrinsicWidth(), expectedDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(expectedBitmap);
        expectedDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        expectedDrawable.draw(canvas);
        return expectedBitmap;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("with drawable from resource id: ");
        description.appendValue(expectedId);
        if (expectedResourceName != null) {
            description.appendText("[");
            description.appendText(expectedResourceName);
            description.appendText("]");
        }
    }
}