package org.fossflight.efb.matchers;

import android.view.View;

import org.hamcrest.Matcher;

import static org.hamcrest.core.Is.is;

public class CustomTestMatchers {
    public static Matcher<View> withDrawable(final int resourceId) {
        return new BitmapDrawableMatcher(resourceId);
    }

    public static Matcher<View> withSVGDrawable(final int resourceId) {
        return new SVGDrawableMatcher(resourceId);
    }

    public static Matcher<View> noDrawable() {
        return new BitmapDrawableMatcher(-1);
    }

    public static Matcher<View> hasTextInputLayoutHintText(final String expectedErrorText) {
        return new TextInputLayoutErrorMatcher(expectedErrorText);
    }

    public static Matcher<View> withToolbarTitle(CharSequence title) {
        return new ToolbarTitleMatcher(is(title));
    }
}