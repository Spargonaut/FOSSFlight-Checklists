package org.fossflight.efb.matchers;

import android.view.View;

import org.hamcrest.Description;
import org.hamcrest.Matcher;

import androidx.appcompat.widget.Toolbar;
import androidx.test.espresso.matcher.BoundedMatcher;

public class ToolbarTitleMatcher extends BoundedMatcher<View, Toolbar> {

    private Matcher<CharSequence> textMatcher;

    ToolbarTitleMatcher(Matcher<CharSequence> textMatcher) {
        super(Toolbar.class);
        this.textMatcher = textMatcher;
    }

    @Override
    public boolean matchesSafely(Toolbar toolbar) {
        return textMatcher.matches(toolbar.getTitle());
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("with app_toolbar title: ");
        textMatcher.describeTo(description);
    }
}
